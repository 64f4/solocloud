#
# Makefile
#
# (C) 2012 Emanuele Altieri <ea@paradoxity.com>
#
# Usage:
#
#    make [OPTIONS] [target]
#
# Options:
#
#    BUILD={std|dbg|opt}
#
#        Set build type:
#        Standard (std)  / -O1 -g     [default]
#        Debug (dbg)     / -O0 -g
#        Optimized (opt) / -O3
#
#    SOLO_SERVER=name
#
#        Set SoloCloud switchboard server (default: localhost)
#
#    DEMO=1
#
#        Build using demo-specific parameters.
#

ifneq (cscope,$(MAKECMDGOALS))
include Makefile.in
endif

.PHONY: cscope
cscope:
	find * -name '*.c' -or -name '*.h' > allsource
	cscope -bkR

.PHONY: clean
clean:
	find * -name '*.o' -or -name '*.a' -or -name '*.dep' \
		-or -name '*.bin' | xargs rm -fv
	rm -vf $(CBIN)

.PHONY: distclean
distclean: clean
	rm -vf allsource cscope.out

.PHONY: info
info:
	@echo "CC: $(CC)"
	@echo "CC version: $(shell $(CC) --version)"
	@echo "CFLAGS: $(CFLAGS)"
