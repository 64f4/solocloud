/*
 * boxd-serve-rmdir.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* System headers */
#include <sys/stat.h>
#include <sys/types.h>

/* Private header */
#include "boxd.h"

int
boxd_serve_rmdir(
	struct sobx_ses          * const ses,
	struct fus_m_rmdir const * const req,
	size_t                     const reqsize
	)
{
	log_t        * const log  = ses->bxs_log;
	solo_conn_t  * const conn = ses->bxs_conn;
	int            err;

	log_entering(log);

	/* verify basic request structure */
	if (reqsize < sizeof(struct fus_m_rmdir)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}
	size_t const expected_reqsize =
		  sizeof(struct fus_m_rmdir)  // request structure size
		+ req->fus_q_rmdir_pathlenz;  // path length with nil char
	if (reqsize != expected_reqsize) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	/* file path */
	char const * const path = _boxdpath(req->fus_q_rmdir_path,
					    req->fus_q_rmdir_pathlenz);
	if (path == NULL) {
		log_unexpected(log);
		err = EINVAL;
		goto fail;
	}

	/* do not remove our root! */
	if (!strcmp(path, ".")) {
		log_printf(log, LOG_ERR, "cannot rmdir() root");
		err = EPERM;
		goto fail;
	}

	log_printf(log, LOG_DEBUG, "RMDIR \"%s\"", path); // FIXME Unsafe

	/* execute rmdir() system call */
	if ((err = rmdir(path) ? errno : 0)) {
		log_perror(log, LOG_ERR, "rmdir()", err);
		goto fail;
	}

	/* Submit RMDIR success response.
	 */
	struct fus_m_rmdir_r const rsp = {
		.fus_r_rmdir_magic = FUS_MAGIC_RMDIR,
	};
	if ((err = solo_send(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		goto fail;
	}

	log_leaving(log);
	return 0;

	/* Submit RMDIR error response.
	 */
fail:	log_assert(log, err);
	if (err != EPIPE) {
		struct fus_m_rmdir_r const badrsp = {
			.fus_r_rmdir_magic = FUS_MAGIC_RMDIR,
			.fus_r_rmdir_err   = fus_errno_os2fus(err),
		};
		(void) solo_send(conn, &badrsp, sizeof(badrsp));
	}
	log_leaving(log);
	return err;
}
