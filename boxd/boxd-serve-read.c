/*
 * boxd-serve-read.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <fcntl.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>

/* Private header */
#include "boxd.h"

int
boxd_serve_read(
	struct sobx_ses         * const ses,
	struct fus_m_read const * const req,
	size_t                    const reqsize
	)
{
	log_t       * const log  = ses->bxs_log;
	solo_conn_t * const conn = ses->bxs_conn;
	struct stat   stbuf;
	int           dummy;
	int           err;

	log_entering(log);

	/* verify request size */
	if (reqsize != sizeof(struct fus_m_read)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	/* check file descriptor */
	int const fd = req->fus_q_read_fd;
	if ((err = _lookupfd(ses, fd))) {
		log_printf(log, LOG_ERR, "bad file descriptor");
		goto fail_inval;
	}

	off_t const offs = req->fus_q_read_offset;
	size_t const count = req->fus_q_read_count;

	log_printf(log, LOG_DEBUG, "READ [%d] %lluB @%llu", fd,
		   (unsigned long long) count,
		   (unsigned long long) offs);

	//
	// XXX
	//	Notice that before we perform the read operation (done by
	//	solo_sendfile()) we issue a dummy read() to verify that the
	//	file descriptor can be read. This is necessary because,
	//	unfortunately, we cannot afford to have an error in
	//	solo_sendfile(), where we cannot recover from it.
	//
	//	While solo_sendfile() is very efficient compared to reading
	//	the file into a temporary buffer using pread(), I really
	//	dislike the fact that we cannot recover from solo_sendfile()
	//	errors, as the reply header is sent to the client before we
	//	actually read from the file. On error, the connection has to
	//	be dropped. I wonder if this trouble is worth the performance
	//	improvements.
	//

	/* make sure we have permissions to read from the descriptor */
	if ((err = read(fd, &dummy, 0) < 0 ? errno : 0)) {
		log_perror(log, LOG_ERR, "read()", err);
		goto fail_perm;
	}

	/* lock file for reading */
	if ((err = flock(fd, LOCK_SH) ? errno : 0)) {
		log_perror(log, LOG_ERR, "flock()", err);
		goto fail_lock;
	}

	/* determine number of bytes to read from file */
	if ((err = fstat(fd, &stbuf) ? errno : 0)) {
		log_perror(log, LOG_ERR, "stat()", err);
		goto fail_stat;
	}
	off_t const max = stbuf.st_size;
	off_t const avail = offs < max ? max - offs : 0;
	off_t const numread = GCC_MIN((off_t) count, avail);

	/* Submit READ success response.
	 */
	struct fus_m_read_r const rsp = {
		.fus_r_read_magic = FUS_MAGIC_READ,
		.fus_r_read_count = numread,
	};
	struct iovec const iov[] = {
		{ (void *) &rsp, sizeof(rsp), },
	};
	size_t const ioc = GCC_DIM(iov);
	if ((err = solo_sendfile(conn, iov, ioc, fd, offs, numread))) {
		log_perror(log, LOG_TRACE, "solo_sendfile()", err);
		err = EPIPE; // the connection has been compromised
		goto fail_send;
	}

	/* unlock file descriptor */
	flock(fd, LOCK_UN);
	log_leaving(log);
	return 0;

fail_send:
fail_stat:
	flock(fd, LOCK_UN);
fail_lock:
fail_perm:
fail_inval:
	/* Submit READ error response.
	 */
	log_assert(log, err);
	if (err != EPIPE) {
		struct fus_m_read_r const badrsp = {
			.fus_r_read_magic = FUS_MAGIC_READ,
			.fus_r_read_err   = fus_errno_os2fus(err),
		};
		(void) solo_send(conn, &badrsp, sizeof(badrsp));
	}
	log_leaving(log);
	return err;
}
