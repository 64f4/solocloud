/*
 * boxd-listen.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include "boxd.h"        // private header

int
boxd_listen(struct sobx * const bx)
{
	log_t       * const log  = bx->bx_log;
	solo_t      * const solo = bx->bx_solo;
	solo_conn_t * conn;
	int           err;

	/* session numbers */
	static unsigned sesno = 0;

	log_entering(log);

	// TODO
	//	solo_listen() needs to return a server object (solo_srv_t).
	//	This object should then be passed to solo_accept().
	//

	char const * const boxname = bx->bx_cf.bxcf_name;
	if ((err = solo_listen(solo, boxname, SOLO_PROTO_FUS))) {
		log_perror(log, LOG_TRACE, "solo_listen()", err);
		goto fail;
	}

	while ( !(err = solo_accept(solo, &conn)) )
	{
		struct sobx_ses * ses;
		pthread_t         id;

		/* allocate session for the new connection */
		if ((err = mm_malloc_t(log, &ses, struct sobx_ses)))
			continue;
		*ses = (struct sobx_ses) {
			.bxs_no   = __sync_add_and_fetch(&sesno, 1),
			.bxs_bx   = bx,
			.bxs_log  = log,
			.bxs_conn = conn,
		};

		/* add to session list */
		Pthread_mutex_lock(&bx->bx_sesmux);
		list_add_tail(&ses->bxs_node, &bx->bx_seslist);
		Pthread_mutex_unlock(&bx->bx_sesmux);

		/* fork a thread to deal with this connection */
		void * (* const thread)(void *) = boxd_serve__thread;
		if ((err = pthread_create(&id, NULL, thread, ses))) {
			log_perror(log, LOG_ERR, "pthread_create()", err);
			/* remove from session list */
			Pthread_mutex_lock(&bx->bx_sesmux);
			list_del(&ses->bxs_node);
			Pthread_mutex_unlock(&bx->bx_sesmux);
			solo_close(conn);
			mm_free(log, ses);
			continue;
		}
	}

	/* report solo_accept() error if applicable */
	if (err == ESHUTDOWN)
		err = 0;
	else if (err)
		log_perror(log, LOG_TRACE, "solo_accept()", err);

	/* drop all sessions */
	log_printf(log, LOG_INFO, "dropping sessions");
	Pthread_mutex_lock(&bx->bx_sesmux);
	list_t * it, * next;
	list_for_each_safe(it, next, &bx->bx_seslist) {
		struct sobx_ses * const ses =
			list_entry(it, struct sobx_ses, bxs_node);
		log_printf(log, LOG_DEBUG, "dropping SES%u", ses->bxs_no);
		solo_drop(ses->bxs_conn);
	}
	/* wait for the current sessions to quit */
	log_printf(log, LOG_DEBUG, "waiting for sessions");
	bx->bx_seswait = true;
	while (!list_empty(&bx->bx_seslist))
		Pthread_cond_wait(&bx->bx_sescond, &bx->bx_sesmux);
	bx->bx_seswait = false;
	Pthread_mutex_unlock(&bx->bx_sesmux);

fail:	log_leaving(log);
	return err;
}
