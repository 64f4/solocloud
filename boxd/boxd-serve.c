/*
 * boxd-serve.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include "boxd.h"  // private header

static int
boxd_serve(struct sobx_ses * const ses)
{
	log_t       * const log  = ses->bxs_log;
	solo_conn_t * const conn = ses->bxs_conn;
	void        * reqbuf;
	size_t        reqsize;
	int           err;

	log_entering(log);

	/* Receive request from client. A EPIPE error here means that the
	 * connection was dropped by the client; this is the way a session
	 * should normally end, so we don't report an error.
	 */
again:	if ((err = solo_recva(conn, &reqbuf, &reqsize))) {
		if (err == EPIPE)
			err = 0;
		else
			log_perror(log, LOG_TRACE, "solo_recva()", err);
		log_leaving(log);
		return err;
	}

	/* Extract request magic number.
	 */
	union fus_u const * const u = reqbuf;
	if (reqsize < sizeof(u->fus_u_magic)) {
		free(reqbuf);
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}
	uint32_t const magic = u->fus_u_magic;

	/* Authenticate the session first.
	 */
	if (!ses->bxs_auth) {
		switch (magic) {
		case FUS_MAGIC_LOGIN:
			err = boxd_serve_login(ses, &u->fus_u_login, reqsize);
			break;
		default:
			log_unexpected(log);
			err = EPROTO;
		}
		goto skip;
	}

	/* Call appropriate request handler based on magic number.
	 */
	switch (magic) {
	case FUS_MAGIC_CLOSE:
		err = boxd_serve_close(ses, &u->fus_u_close, reqsize);
		break;
	case FUS_MAGIC_LOGIN:
		err = boxd_serve_login(ses, &u->fus_u_login, reqsize);
		break;
	case FUS_MAGIC_STAT:
		err = boxd_serve_stat(ses, &u->fus_u_stat, reqsize);
		break;
	case FUS_MAGIC_STATFS:
		err = boxd_serve_statfs(ses, &u->fus_u_statfs, reqsize);
		break;
	case FUS_MAGIC_MKDIR:
		err = boxd_serve_mkdir(ses, &u->fus_u_mkdir, reqsize);
		break;
	case FUS_MAGIC_OPEN:
		err = boxd_serve_open(ses, &u->fus_u_open, reqsize);
		break;
	case FUS_MAGIC_READ:
		err = boxd_serve_read(ses, &u->fus_u_read, reqsize);
		break;
	case FUS_MAGIC_READDIR:
		err = boxd_serve_readdir(ses, &u->fus_u_readdir, reqsize);
		break;
	case FUS_MAGIC_RENAME:
		err = boxd_serve_rename(ses, &u->fus_u_rename, reqsize);
		break;
	case FUS_MAGIC_RMDIR:
		err = boxd_serve_rmdir(ses, &u->fus_u_rmdir, reqsize);
		break;
	case FUS_MAGIC_TRUNC:
		err = boxd_serve_trunc(ses, &u->fus_u_trunc, reqsize);
		break;
	case FUS_MAGIC_UNLINK:
		err = boxd_serve_unlink(ses, &u->fus_u_unlink, reqsize);
		break;
	case FUS_MAGIC_WRITE:
		err = boxd_serve_write(ses, &u->fus_u_write, reqsize);
		break;
	default:
		log_unexpected(log);
		err = EPROTO;
	}

skip:	free(reqbuf);

	/* Errors such as EPIPE, EPROTO, and ESHUTDOWN will cause the client
	 * to be dropped as we cannot recover from them. Any other error is
	 * ignored as it is assumed that an appropriate error message has
	 * already been sent to the client, and the session can resume as
	 * usual.
	 */
	if (err) {
		switch (err) {
		case EPIPE:
		case EPROTO:
		case ESHUTDOWN:
			log_leaving(log);
			return err;
		}
	}

	goto again;
}

static void
boxd_serve__init(struct sobx_ses * const ses)
{
	/* initialize open file table */
	memset(ses->bxs_fdv, -1, sizeof(ses->bxs_fdv));
}

static void
boxd_serve__atexit(struct sobx_ses * const ses)
{
	struct sobx * const bx = ses->bxs_bx;
	log_t * const log = ses->bxs_log;
	size_t i;

	/* close all FDs in the open file table */
	int const * const fdv = ses->bxs_fdv;
	size_t const fdm = GCC_DIM(ses->bxs_fdv);
	for (i = 0; i < fdm; i++)
		if (fdv[i] != -1)
			close(fdv[i]);

	Pthread_mutex_lock(&bx->bx_sesmux);
	/* remove from session list */
	list_del(&ses->bxs_node);
	/* clean up */
	solo_close(ses->bxs_conn);
	mm_free(log, ses);
	/* notify main thread if requested */
	if (list_empty(&bx->bx_seslist) && bx->bx_seswait)
		Pthread_cond_signal(&bx->bx_sescond);
	Pthread_mutex_unlock(&bx->bx_sesmux);
}

void *
boxd_serve__thread(void * const arg)
{
	struct sobx_ses * const ses = arg;
	Pthread_detach(pthread_self());
	boxd_serve__init(ses);
	boxd_serve(ses);
	boxd_serve__atexit(ses);
	pthread_exit(NULL);
}
