/*
 * boxd-serve-stat.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <sys/stat.h>

/* Private header */
#include "boxd.h"

int
boxd_serve_stat(
	struct sobx_ses         * const ses,
	struct fus_m_stat const * const req,
	size_t                    const reqsize )
{
	log_t       * const log  = ses->bxs_log;
	solo_conn_t * const conn = ses->bxs_conn;
	struct stat   stbuf;
	int           err;

	log_entering(log);

	/* verify basic request structure */
	if (reqsize < sizeof(struct fus_m_stat)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}
	size_t const expected_reqsize =
		  sizeof(struct fus_m_stat)  // request structure size
		+ req->fus_q_stat_pathlenz;  // path length with nil char
	if (reqsize != expected_reqsize) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	/* file path */
	char const * const path = _boxdpath(req->fus_q_stat_path,
					    req->fus_q_stat_pathlenz);
	if (path == NULL) {
		log_unexpected(log);
		err = EINVAL;
		goto fail;
	}

	log_printf(log, LOG_DEBUG, "STAT \"%s\"", path); // FIXME Unsafe

	/* execute stat() system call */
	if ((err = stat(path, &stbuf) ? errno : 0)) {
		log_perror(log, LOG_ERR, "stat()", err);
		goto fail;
	}

	/* clear group and others permissions, just in case */
	stbuf.st_mode &= ~(S_IRWXG | S_IRWXO);

	/* Submit STAT success response.
	 */
	struct fus_m_stat_r const rsp =
		FUS_MAKE_STRSP_FROM_STBUF(0, &stbuf);
	if ((err = solo_send(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		goto fail;
	}

	log_leaving(log);
	return 0;

	/* Submit STAT error response.
	 */
fail:	log_assert(log, err);
	if (err != EPIPE) {
		struct fus_m_stat_r const badrsp = {
			.fus_r_stat_magic = FUS_MAGIC_STAT,
			.fus_r_stat_err   = fus_errno_os2fus(err),
		};
		(void) solo_send(conn, &badrsp, sizeof(badrsp));
	}
	log_leaving(log);
	return err;
}
