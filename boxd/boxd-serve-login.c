/*
 * boxd-serve-login.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/stat.h>
#include <sys/types.h>

/* Private header */
#include "boxd.h"

int
boxd_serve_login(
	struct sobx_ses          * const ses,
	struct fus_m_login const * const req,
	size_t                     const reqsize
	)
{
	log_t                    * const log  = ses->bxs_log;
	solo_conn_t              * const conn = ses->bxs_conn;
	struct sobx const        * const box  = ses->bxs_bx;
	int                        err;
	int                        ioerr;

	log_entering(log);

	/* verify basic request structure */
	if (reqsize != sizeof(struct fus_m_login)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	log_printf(log, LOG_DEBUG, "LOGIN");

	/* check password */
	log_assert(log, sizeof(box->bx_passh) == sizeof(req->fus_q_login_h));
	if (memcmp(box->bx_passh,
		   req->fus_q_login_h,
		   sizeof(req->fus_q_login_h)))
	{
		log_printf(log, LOG_ERR, "invalid password");
		ses->bxs_auth = false;
		err = EPERM;
	}
	else
	{
		ses->bxs_auth = true;
		err = 0;
	}
	
	/* Submit LOGIN response.
	 */
	struct fus_m_login_r const rsp = {
		.fus_r_login_magic = FUS_MAGIC_LOGIN,
		.fus_r_login_err   = fus_errno_os2fus(err),
	};
	if ((ioerr = solo_send(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_send()", ioerr);
		log_leaving(log);
		return ioerr;
	}

	log_leaving(log);
	return err;
}
