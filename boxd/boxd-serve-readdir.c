/*
 * boxd-serve-readdir.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

/* Private header */
#include "boxd.h"

int
boxd_serve_readdir(
	struct sobx_ses            * const ses,
	struct fus_m_readdir const * const req,
	size_t                       const reqsize
	)
{
	log_t       * const log  = ses->bxs_log;
	solo_conn_t * const conn = ses->bxs_conn;
	int           err;

	log_entering(log);

	/* verify basic request structure */
	if (reqsize < sizeof(struct fus_m_readdir)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}
	size_t const expected_reqsize =
		  sizeof(struct fus_m_readdir)   // request structure size
		+ req->fus_q_readdir_pathlenz;   // path length with nil char
	if (reqsize != expected_reqsize) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	/* the path of the directory to be scanned */
	char const * const path = _boxdpath(req->fus_q_readdir_path,
					    req->fus_q_readdir_pathlenz);
	if (path == NULL) {
		log_unexpected(log);
		err = EINVAL;
		goto fail_inval;
	}

	/*
	 * Request has been validated. Proceed to execution.
	 */

	log_printf(log, LOG_DEBUG, "READDIR \"%s\"", path); // FIXME Unsafe

	/* Allocate initial READDIR reply buffer.
	 */
	size_t max = GCC_MAX(1024u, sizeof(struct fus_m_readdir_r) + 512);
	struct fus_m_readdir_r * rsp;
	if ((rsp = calloc(1, max)) == NULL) {
		err = errno;
		log_perror(log, LOG_ERR, "calloc()", err);
		goto fail_rsp;
	}
	*rsp = (struct fus_m_readdir_r) {
		.fus_r_readdir_magic = FUS_MAGIC_READDIR,
	};

	char * rsp_s = rsp->fus_r_readdir_entries;      // entry write head
	char const * rsp_e = (char const *) rsp + max;  // end of buffer
	log_assert(log, rsp_s <= rsp_e);

	/* Open requested directory.
	 */
	DIR * const dir = opendir(path);
	if (dir == NULL) {
		err = errno;
		log_perror(log, LOG_ERR, "opendir()", err);
		goto fail_opendir;
	}

	int const dir_fd = dirfd(dir);  // needed by fstatat() later

	/* Allocate buffer for readdir_r() as documented by the manual page
	 * for the system call.
	 */
	size_t const entrysize =
	      offsetof(struct dirent, d_name)
	    + pathconf(path, _PC_NAME_MAX) + 1;
	struct dirent * const entrybuf = malloc(entrysize);
	if (entrybuf == NULL) {
		err = errno;
		log_perror(log, LOG_ERR, "malloc()", err);
		goto fail_entrybuf;
	}

	/* Scan directory.
	 *
	 * For each entry in the directory, we send back to the client the
	 * name of the directory entry (nil terminated) immediately followed
	 * by its stat() values. The stat() values are not aligned.
	 */
	struct dirent * entry;
	unsigned numentries = 0;
	while (!(err = readdir_r(dir, entrybuf, &entry)) && entry)
	{
		struct stat stbuf;

		/* name, length of the directory entry */
		char const * const name = entry->d_name;
		size_t const namelenz = strlen(name) + 1;

		/* hide "." and ".." */
		if (!strcmp(name, ".") || !strcmp(name, ".."))
			continue;

		/* stat() file, or skip on error */
		if ((err = fstatat(dir_fd, name, &stbuf, 0) ? errno : 0)) {
			log_perror(log, LOG_ERR, "fstatat()", err);
			continue;
		}
		/* Fill structure to be transmitted to the client (this is the
		 * same as a STAT response message) .
		 */
		struct fus_m_stat_r const strsp =
			FUS_MAKE_STRSP_FROM_STBUF(0, &stbuf);

		/* calculate total size of the readdir() entry */
		size_t const entrysz = namelenz + sizeof(strsp);

		/* Make sure we have enough space in the response buffer for
		 * the entry name and its stat() values. If not, double the
		 * size of the response buffer.
		 */
		while ((size_t) (rsp_e - rsp_s) < entrysz) {
			max = max << 1;
			size_t const offs = rsp_s - (char *) rsp;
			void * const ptr = realloc(rsp, max);
			if (ptr == NULL) {
				err = errno;
				log_perror(log, LOG_ERR, "realloc()", err);
				goto fail;
			}
			/* adjust response pointers */
			rsp   = ptr;
			rsp_s = ptr + offs;
			rsp_e = ptr + max;
		}

		/* store entry name in reply buffer */
		memcpy(rsp_s, name, namelenz);
		rsp_s += namelenz;
		/* store entry stat() values in reply buffer */
		memcpy(rsp_s, &strsp, sizeof(strsp));
		rsp_s += sizeof(strsp);

		log_assert(log, rsp_s <= rsp_e);
		numentries++;
	}
	if (err) {
		log_perror(log, LOG_ERR, "readdir_r()", err);
		goto fail;
	}

	/* Submit READDIR success response.
	 */
	rsp->fus_r_readdir_numentries = numentries;
	size_t const rspsize = rsp_s - (char *) rsp;
	if ((err = solo_send(conn, rsp, rspsize))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		goto fail;
	}

	free(entrybuf);
	closedir(dir);
	free(rsp);
	log_leaving(log);
	return 0;

fail:
	free(entrybuf);
fail_entrybuf:
	closedir(dir);
fail_opendir:
	free(rsp);
fail_rsp:
fail_inval:
	/* Submit READDIR error response.
	 */
	log_assert(log, err);
	if (err != EPIPE) {
		struct fus_m_readdir_r const badrsp = {
			.fus_r_readdir_magic = FUS_MAGIC_READDIR,
			.fus_r_readdir_err   = fus_errno_os2fus(err),
		};
		(void) solo_send(conn, &badrsp, sizeof(badrsp));
	}
	log_leaving(log);
	return err;
}
