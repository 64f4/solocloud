/*
 * boxd-signal.c
 *
 *	Signal handling.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <signal.h>

#include "boxd.h"  // private header

/**
 * Notify main thread (boxd_main()) that the shutdown is intentional.
 */
static inline void
_notify_shutdown(struct sobx * const bx)
{
	Pthread_mutex_lock(&bx->bx_shutmux);
	bx->bx_shutdown = true;
	Pthread_cond_signal(&bx->bx_shutcond);
	Pthread_mutex_unlock(&bx->bx_shutmux);
	solo_shutdown(bx->bx_solo);
}

/**
 * Signal handling.
 */
static void
boxd_sigwait(struct sobx * const bx)
{
	log_t    * const log = bx->bx_log;
	sigset_t   sigset;
	int        signo;

	sigfillset(&sigset);
	while (!sigwait(&sigset, &signo))
	{
		char const * s = NULL;
		switch(signo) {
		case SIGINT:
			s = s ? : "SIGINT";
		case SIGQUIT:
			s = s ? : "SIGQUIT";
		case SIGTERM:
			s = s ? : "SIGTERM";
			log_printf(log, LOG_DEBUG, "received %s", s);
			log_printf(log, LOG_INFO, "shutting down");
			_notify_shutdown(bx);
			return;
		case SIGWINCH:
			break;
		default:
			log_printf(log, LOG_DEBUG, "ignoring SIG%d", signo);
		}
	}

	int const err = errno;
	log_perror(log, LOG_CRIT, "sigwait()", err);
	log_bug(log);
}

/**
 * Signal handling thread.
 */
void *
boxd_sigwait__thread(void * const arg)
{
	struct sobx * const bx = arg;
	boxd_sigwait(bx);
	pthread_exit(NULL);
}
