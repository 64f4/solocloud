/*
 * boxd-serve-unlink.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "boxd.h"

int
boxd_serve_unlink(
	struct sobx_ses           * const ses,
	struct fus_m_unlink const * const req,
	size_t                      const reqsize
	)
{
	log_t       * const log  = ses->bxs_log;
	solo_conn_t * const conn = ses->bxs_conn;
	int           err;

	log_entering(log);

	/* verify basic request structure */
	if (reqsize < sizeof(struct fus_m_unlink)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}
	size_t const expected_reqsize =
		  sizeof(struct fus_m_unlink)   // request structure size
		+ req->fus_q_unlink_pathlenz;   // path length with nil char
	if (reqsize != expected_reqsize) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	/* file path */
	char const * const path = _boxdpath(req->fus_q_unlink_path,
					    req->fus_q_unlink_pathlenz);
	if (path == NULL) {
		log_unexpected(log);
		err = EINVAL;
		goto fail;
	}

	log_printf(log, LOG_DEBUG, "UNLINK \"%s\"", path); // FIXME Unsafe

	/* execute unlink() system call */
	if ((err = unlink(path) ? errno : 0)) {
		log_perror(log, LOG_ERR, "unlink()", err);
		goto fail;
	}

	/* Submit UNLINK success response.
	 */
	struct fus_m_unlink_r const rsp = {
		.fus_r_unlink_magic = FUS_MAGIC_UNLINK,
	};
	if ((err = solo_send(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		goto fail;
	}

	log_leaving(log);
	return 0;

	/* Submit UNLINK error response.
	 */
fail:	log_assert(log, err);
	if (err != EPIPE) {
		struct fus_m_unlink_r const badrsp = {
			.fus_r_unlink_magic = FUS_MAGIC_UNLINK,
			.fus_r_unlink_err   = fus_errno_os2fus(err),
		};
		(void) solo_send(conn, &badrsp, sizeof(badrsp));
	}
	log_leaving(log);
	return err;
}
