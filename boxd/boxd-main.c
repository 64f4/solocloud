/*
 * boxd-main.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <ctype.h>
#include <signal.h>
#include <sys/time.h>
#include <sysexits.h>

/* Project headers */
#include "spooky.h"

/* Private header */
#include "boxd.h"

/* How long we wait before retrying to contact the switchboard after the
 * connection has been lost.
 */
#define BOXD_RETRY_INTERVAL 30  // sec

static int
boxd_help(void)
{
	fprintf(stderr, "\
Usage: " BOXD_PREFIX " BOXID [SWITCHBOARD]\n\
\n\
This is the SoloBox daemon, which serves remote filesystem requests. The\n\
directory in which the daemon is running will be used as the root for all the\n\
remote filesystem requests. The mandatory BOXID argument is used by the clients\n\
to locate this box.\n\
\n\
Mandatory arguments:\n\
  BOXID          box identifier\n\
\n\
Optional arguments:\n\
  SWITCHBOARD    switchboard server (developers only)\n\
\n\
Example: " BOXD_PREFIX " mrspock-box\n\
");
	return EINVAL;
}

/**
 * Parse command line arguments, configuration file if any.
 */
static int
boxd_config(
	int              const argc,
	char const     * const argv[],
	struct sobx_cf * const cf
	)
{
	char const     * s;
	char             c;
	int              i;

	memset(cf, 0, sizeof(*cf));

	if (argc < 2 || argc > 3)
		return boxd_help();
	for (i = 0; i < argc; i++)
		if (   !strcmp(argv[i], "-h")
		    || !strcmp(argv[i], "-help")
		    || !strcmp(argv[i], "--help"))
			return boxd_help();

	/* box name */
	s = argv[1];
	while ((c = *s) && isascii(c) && isprint(c) && !isspace(c))
		s++;
	if (*s) {
		boxd_fatal("invalid box name");
		return EINVAL;
	}
	cf->bxcf_name = argv[1];

	/* switchboard server (optional) */
	if (argv[2])
		cf->bxcf_server = argv[2];

	return 0;
}

/**
 * Configure server runtime environment.
 */
static int
boxd_init(
	struct sobx_cf const * const cf,
	struct sobx          * const bx )
{
	log_t                * log;
	solo_t               * solo;
	int                    err;

	memset(bx, 0, sizeof(*bx));

	/* global configuration */
	bx->bx_cf = *cf;

	/* open log file */
	if ((err = log_open_f(stderr, &log))) {
		boxd_fatal("cannot open log file");
		goto fail_log;
	}
	bx->bx_log = log;

	/* get SoloCloud library instance */
	struct solo_cf const solocf = {
		.socf_server = bx->bx_cf.bxcf_server,
	};
	if ((err = solo_init(log, &solocf, &solo))) {
		boxd_fatal("failed to instantiate SoloCloud library");
		goto fail_solo;
	}
	bx->bx_solo = solo;

	/* other */
	Pthread_mutex_init(&bx->bx_shutmux, NULL);
	Pthread_cond_init(&bx->bx_shutcond, NULL);
	Pthread_mutex_init(&bx->bx_sesmux, NULL);
	Pthread_cond_init(&bx->bx_sescond, NULL);
	list_init(&bx->bx_seslist);

	return 0;

fail_solo:
	log_close(log);
fail_log:
	memset(bx, 0, sizeof(*bx));
	return err;
}

/**
 * Destroy server runtime environment.
 */
static void
boxd_uninit(struct sobx * const bx)
{
	list_uninit(&bx->bx_seslist);
	Pthread_cond_destroy(&bx->bx_sescond);
	Pthread_mutex_destroy(&bx->bx_sesmux);
	Pthread_cond_destroy(&bx->bx_shutcond);
	Pthread_mutex_destroy(&bx->bx_shutmux);
	solo_uninit(bx->bx_solo);
	log_close(bx->bx_log);
	memset(bx, 0, sizeof(*bx));
}

static int
boxd_password(struct sobx * const bx)
{
	uint64_t h[2] = { 0, 0 };

	// FIXME
	//	This function is obviously temporary. First of all, getpass()
	//	is obsolete and we shouldn't be using it. Second, we shouldn't
	//	ask for a password every time we boot up. This is fine if we
	//	are launching the daemon directly on a machine we own, but
	//	what about the Solo Box?

	char * pass = getpass("Box password: ");
	size_t len = strlen(pass);

	/* remove terminating newline char */
	while (len && (   pass[len - 1] == '\n'
		       || pass[len - 1] == '\r'))
		len--;

	// FIXME
	//	We really shouldn't use SpookyHash for this.

	/* calculate hash of the password */
	spooky_hash128(pass, len, &h[0], &h[1]);

	memcpy(bx->bx_passh, h, sizeof(h));
	memset(h, -1, sizeof(h));

	memset(pass, 0, len);
	return 0;
}

/**
 * Main server body.
 */
static int
boxd_main(struct sobx * const bx)
{
	log_t     * const log = bx->bx_log;
	pthread_t   sigid;
	sigset_t    sigset;
	int         err;

	log_printf(log, LOG_INFO, "starting up");

	/* block all signals */
	sigfillset(&sigset);
	pthread_sigmask(SIG_BLOCK, &sigset, NULL);

	/* fork signal handling thread */
	void * (* const thread)(void *) = boxd_sigwait__thread;
	if ((err = pthread_create(&sigid, NULL, thread, bx))) {
		log_perror(log, LOG_ERR, "pthread_create()", err);
		goto fail_sigwait;
	}

	/* Unless the connection with the switchboard is lost intentionally
	 * (through a SIGINT, for instance), we keep retrying to contact the
	 * switchboard at regular intervals.
	 */
	pthread_mutex_t * const mux  = &bx->bx_shutmux;
	pthread_cond_t  * const cond = &bx->bx_shutcond;
	Pthread_mutex_lock(mux);
	while (!bx->bx_shutdown) {
		Pthread_mutex_unlock(mux);

		/* handle connections */
		if ((err = boxd_listen(bx))) {
			log_perror(log, LOG_TRACE, "siwtchd_listen()", err);
			/* (fall through) */
		}

		// FIXME
		//	Currently, when we loose the connection to the
		//	switchboard, all of the client sessions are dropped.
		//	This should not happen, as the client sessions may
		//	still be functional even if the main connection to the
		//	switchboard has been lost. On the other hand, this bug
		//	only affects Proxy connections, so it should not
		//	matter as much once P2P has been implemented.
		//

		/* wait a little before retrying a connection */
		Pthread_mutex_lock(mux);
		if (!bx->bx_shutdown) {
			struct timeval tv;
			gettimeofday(&tv, NULL);
			struct timespec const ts = {
				tv.tv_sec + BOXD_RETRY_INTERVAL, 0
			};
			/* sleep until timeout or a shutdown signal */
			log_printf(log, LOG_DEBUG, "retrying in %u sec",
				   BOXD_RETRY_INTERVAL);
			(void) Pthread_cond_timedwait(cond, mux, &ts);
		}
	}
	Pthread_mutex_unlock(mux);

	/* wait for the signal handling thread (if still running) */
	pthread_kill(sigid, SIGINT);
	pthread_join(sigid, NULL);

	log_printf(log, LOG_INFO, "exiting now");
	return 0;

fail_sigwait:
	log_printf(log, LOG_INFO, "exiting now (with errors)");
	log_assert(log, err);
	return err;
}

int
main(int const argc, char const * const argv[])
{
	struct sobx_cf cf;
	struct sobx    bx;
	int            err;

	/* parse command line arguments, configuration file */
	if ((err = boxd_config(argc, argv, &cf)))
		goto fail;

	/* prepare server runtime environment */
	if ((err = boxd_init(&cf, &bx)))
		goto fail;

	/* ask for the login password */
	if ((err = boxd_password(&bx)))
		goto fail;

	/* run server */
	err = boxd_main(&bx);

	/* destroy server runtime environment */
	boxd_uninit(&bx);

fail:	return err ? EX_SOFTWARE : EX_OK;
}
