/*
 * boxd-serve-write.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* System headers */
#include <sys/file.h>

/* Private header */
#include "boxd.h"

int
boxd_serve_write(
	struct sobx_ses          * const ses,
	struct fus_m_write const * const req,
	size_t                     const reqsize
	)
{
	log_t       * const log  = ses->bxs_log;
	solo_conn_t * const conn = ses->bxs_conn;
	int           err;

	log_entering(log);

	/* verify request size */
	if (reqsize < sizeof(struct fus_m_write)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}
	size_t const expected_reqsize =
		  sizeof(struct fus_m_write)   // request structure size
		+ req->fus_q_write_count;      // data size
	if (reqsize != expected_reqsize) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	/* check file descriptor validity */
	int const fd = req->fus_q_write_fd;
	if ((err = _lookupfd(ses, fd))) {
		log_printf(log, LOG_ERR, "bad file descriptor");
		goto fail_badf;
	}

	off_t const offs = req->fus_q_write_offset;
	size_t const count = req->fus_q_write_count;

	log_printf(log, LOG_DEBUG, "WRITE [%d] %lluB @%llu", fd,
		   (unsigned long long) count,
		   (unsigned long long) offs);

	/* lock file for writing */
	if ((err = flock(fd, LOCK_EX) ? errno : 0)) {
		log_perror(log, LOG_ERR, "flock()", err);
		goto fail_lock;
	}

	/* execute write() system call */
	void const * const buf = req->fus_q_write_buf;
	ssize_t const numwritten = pwrite(fd, buf, count, offs);
	if (numwritten < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "pwrite()", err);
		goto fail_write;
	}

	/* Submit WRITE success response.
	 */
	struct fus_m_write_r const rsp = {
		.fus_r_write_magic = FUS_MAGIC_WRITE,
		.fus_r_write_count = numwritten,
	};
	if ((err = solo_send(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		goto fail_send;
	}

	/* unlock file */
	flock(fd, LOCK_UN);
	log_leaving(log);
	return 0;

fail_send:
fail_write:
	flock(fd, LOCK_UN);
fail_lock:
fail_badf:
	/* Submit WRITE error response.
	 */
	log_assert(log, err);
	if (err != EPIPE) {
		struct fus_m_write_r const badrsp = {
			.fus_r_write_magic = FUS_MAGIC_WRITE,
			.fus_r_write_err   = fus_errno_os2fus(err),
		};
		(void) solo_send(conn, &badrsp, sizeof(badrsp));
	}
	log_leaving(log);
	return err;
}
