/*
 * boxd-serve-open.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

/* Private header */
#include "boxd.h"

static inline int
_open_flags(uint32_t const fl)
{
	int o = 0;

	/* read/write permissions */
	if (fl & FUS_O_READ && fl & FUS_O_WRITE)
		o |= O_RDWR;
	else if (fl & FUS_O_READ)
		o |= O_RDONLY;
	else if (fl & FUS_O_WRITE)
		o |= O_WRONLY;
	else
		o |= O_RDONLY;  // (default)

	/* other flags */
	if (fl & FUS_O_APPEND)
		o |= O_APPEND;
	if (fl & FUS_O_CREAT)
		o |= O_CREAT;
	if (fl & FUS_O_EXCL)
		o |= O_EXCL;
	if (fl & FUS_O_TRUNC)
		o |= O_TRUNC;

	return o;
}

static inline mode_t
_open_mode(uint32_t const fl)
{
	mode_t m = S_IRUSR | S_IWUSR;

	/* read/write permissions */
	if (fl & FUS_O_EXECUTE)
		m |= S_IXUSR;

	return m;
}

int
boxd_serve_open(
	struct sobx_ses         * const ses,
	struct fus_m_open const * const req,
	size_t                    const reqsize
	)
{
	log_t       * const log  = ses->bxs_log;
	solo_conn_t * const conn = ses->bxs_conn;
	int           fd;
	int           err;

	log_entering(log);

	/* verify basic request structure */
	if (reqsize < sizeof(struct fus_m_open)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}
	size_t const expected_reqsize =
		  sizeof(struct fus_m_open)  // request structure size
		+ req->fus_q_open_pathlenz;  // path length with nil char
	if (reqsize != expected_reqsize) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	/* file path */
	char const * const path = _boxdpath(req->fus_q_open_path,
					    req->fus_q_open_pathlenz);
	if (path == NULL) {
		log_unexpected(log);
		err = EINVAL;
		goto fail_inval;
	}

	// FIXME Unsafe.
	log_printf(log, LOG_DEBUG, "OPEN \"%s\"", path);

	// FIXME
	//	We should probaly avoid opening directories.

	/* execute open() system call */
	uint32_t const flags = req->fus_q_open_flags;
	int const o_flags = _open_flags(flags);
	mode_t const o_mode  = _open_mode(flags);
	if ((fd = open(path, o_flags, o_mode)) < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "open()", err);
		goto fail_open;
	}

	/* Store file descriptor in open file table, unless we ran out of
	 * slots, in which case the file descriptor is closed and an error
	 * (ENFILE) is reported back to the client.
	 */
	if ((err = _addfd(ses, fd))) {
		log_printf(log, LOG_ERR, "too many file descriptors");
		goto fail_toomany;
	}

	log_printf(log, LOG_DEBUG, "FD [%d]", fd);

	// XXX
	//	Revisit the security implications of passing an actual file
	//	descriptor to the client. While the client can only access the
	//	file descriptors it opens (thanks to the FD table), I'm not
	//	sure whether there are other more subtle security issues.
	//

	/* Submit OPEN success response.
	 */
	struct fus_m_open_r const rsp = {
		.fus_r_open_magic = FUS_MAGIC_OPEN,
		.fus_r_open_fd    = fd,
	};
	if ((err = solo_send(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		goto fail_send;
	}

	log_leaving(log);
	return 0;

fail_send:
	_delfd(ses, fd);
fail_toomany:
	close(fd);
fail_open:
fail_inval:
	/* Submit OPEN error response.
	 */
	log_assert(log, err);
	if (err != EPIPE) {
		struct fus_m_open_r const badrsp = {
			.fus_r_open_magic = FUS_MAGIC_OPEN,
			.fus_r_open_err   = fus_errno_os2fus(err),
			.fus_r_open_fd    = ~0,
		};
		(void) solo_send(conn, &badrsp, sizeof(badrsp));
	}
	log_leaving(log);
	return err;
}
