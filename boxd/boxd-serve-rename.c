/*
 * boxd-serve-rename.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* System headers */
#include <sys/stat.h>
#include <sys/types.h>

/* Private header */
#include "boxd.h"

int
boxd_serve_rename(
	struct sobx_ses           * const ses,
	struct fus_m_rename const * const req,
	size_t                      const reqsize
	)
{
	log_t        * const log  = ses->bxs_log;
	solo_conn_t  * const conn = ses->bxs_conn;
	int            err;

	log_entering(log);

	/* verify basic request structure */
	if (reqsize < sizeof(struct fus_m_rename)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}
	size_t const expected_reqsize =
		  sizeof(struct fus_m_rename)   // request structure size
		+ req->fus_q_rename_srclenz     // source path length
		+ req->fus_q_rename_dstlenz;    // source path length
	if (reqsize != expected_reqsize) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	/* source and destination paths */
	char const * const p = req->fus_q_rename_buf;
	size_t const srclenz = req->fus_q_rename_srclenz;
	size_t const dstlenz = req->fus_q_rename_dstlenz;
	char const * const src = _boxdpath(p, srclenz);
	char const * const dst = _boxdpath(p + srclenz, dstlenz);
	if (src == NULL || dst == NULL) {
		log_unexpected(log);
		err = EINVAL;
		goto fail;
	}

	/* for obvious reaons, we can't rename the root */
	if (!strcmp(src, ".") || !strcmp(dst, ".")) {
		log_printf(log, LOG_ERR, "cannot rename root");
		err = EINVAL;
		goto fail;
	}

	// FIXME Unsafe.
	log_printf(log, LOG_DEBUG, "RENAME \"%s\" --> \"%s\"", src, dst); 

	/* execute rename() system call */
	if ((err = rename(src, dst) ? errno : 0)) {
		log_perror(log, LOG_ERR, "rename()", err);
		goto fail;
	}

	/* Submit RENAME success response.
	 */
	struct fus_m_rename_r const rsp = {
		.fus_r_rename_magic = FUS_MAGIC_RENAME,
	};
	if ((err = solo_send(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		goto fail;
	}

	log_leaving(log);
	return 0;

fail:	/* Submit RENAME error response.
	 */
	log_assert(log, err);
	if (err != EPIPE) {
		struct fus_m_rename_r const badrsp = {
			.fus_r_rename_magic = FUS_MAGIC_RENAME,
			.fus_r_rename_err   = fus_errno_os2fus(err),
		};
		(void) solo_send(conn, &badrsp, sizeof(badrsp));
	}
	log_leaving(log);
	return err;
}
