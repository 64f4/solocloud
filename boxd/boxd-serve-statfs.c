/*
 * boxd-serve-statfs.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <sys/statvfs.h>

/* Private header */
#include "boxd.h"

int
boxd_serve_statfs(
	struct sobx_ses           * const ses,
	struct fus_m_statfs const * const req,
	size_t                      const reqsize )
{
	log_t                     * const log  = ses->bxs_log;
	solo_conn_t               * const conn = ses->bxs_conn;
	struct statvfs              stvfs;
	int                         err;

	log_entering(log);

	/* verify basic request structure */
	if (reqsize < sizeof(struct fus_m_statfs)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}
	size_t const expected_reqsize =
		  sizeof(struct fus_m_statfs)  // request structure size
		+ req->fus_q_statfs_pathlenz;  // path length with nil char
	if (reqsize != expected_reqsize) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	/* file path */
	char const * const path = _boxdpath(req->fus_q_statfs_path,
					    req->fus_q_statfs_pathlenz);
	if (path == NULL) {
		log_unexpected(log);
		err = EINVAL;
		goto fail;
	}

	log_printf(log, LOG_DEBUG, "STATFS \"%s\"", path); // FIXME Unsafe

	/* execute statvfs() system call */
	if ((err = statvfs(path, &stvfs) ? errno : 0)) {
		log_perror(log, LOG_ERR, "statvfs()", err);
		goto fail;
	}

	/* Submit STATFS success response.
	 */
	struct fus_m_statfs_r const rsp = {
		.fus_r_statfs_magic   = FUS_MAGIC_STATFS,
        	.fus_r_statfs_namemax = stvfs.f_namemax,
        	.fus_r_statfs_bsize   = stvfs.f_bsize,
        	.fus_r_statfs_blocks  = stvfs.f_blocks,
        	.fus_r_statfs_bfree   = stvfs.f_bfree,
        	.fus_r_statfs_bavail  = stvfs.f_bavail,
		.fus_r_statfs_files   = stvfs.f_files,
        	.fus_r_statfs_ffree   = stvfs.f_ffree,
	};
	if ((err = solo_send(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		goto fail;
	}

	log_leaving(log);
	return 0;

	/* Submit STATFS error response.
	 */
fail:	log_assert(log, err);
	if (err != EPIPE) {
		struct fus_m_statfs_r const badrsp = {
			.fus_r_statfs_magic = FUS_MAGIC_STATFS,
			.fus_r_statfs_err   = fus_errno_os2fus(err),
		};
		(void) solo_send(conn, &badrsp, sizeof(badrsp));
	}
	log_leaving(log);
	return err;
}
