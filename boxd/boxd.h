/*
 * boxd.h
 *
 *	Private header for the Solo Box daemon.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_BOXD_H
#define SOLOCLOUD_BOXD_H

/* System headers */
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

/* Project headers */
#include "fus-proto.h"    // SoloCloud FUSE protocol definition
#include "list.h"         // doubly-linked list
#include "log.h"          // logging facilities
#include "malloc.h"       // malloc() wrappers
#include "safepthread.h"  // safe pthread wrappers
#include "solo.h"         // SoloCloud library

/* Daemon configuration.
 */
struct sobx_cf
{
	char const      * bxcf_name;   // box name
	char const      * bxcf_server; // switchboard server
};

/* Daemon session.
 */
struct sobx_ses
{
	list_t            bxs_node;    // node in session list
	unsigned          bxs_no;      // session number
	struct sobx     * bxs_bx;      // daemon instance
	log_t           * bxs_log;     // log stream
	solo_conn_t     * bxs_conn;    // client connection
	bool              bxs_auth;    // authenticated session
	int               bxs_fdv[16]; // open file table (see NOTE)

	// NOTE
	//	The open file table (bxs_fdv) guarantees that a client cannot
	//	operate on random server-side file descriptors. Only the file
	//	descriptors listed in `bxs_fdv' can be accessed by the
	//	session.
};

/* Daemon instance.
 */
struct sobx
{
	struct sobx_cf    bx_cf;       // configuration
	log_t           * bx_log;      // log stream
	solo_t          * bx_solo;     // SoloCloud instance
	uint64_t          bx_passh[2]; // password hash
	pthread_mutex_t   bx_sesmux;   // session list mutex
	list_t            bx_seslist;  // session list (struct sobx_ses)
	bool              bx_seswait;  // set when waiting for condition below
	pthread_cond_t    bx_sescond;  // signals that all sessions have quit

	/* shutdown notification */
	pthread_mutex_t   bx_shutmux;
	bool volatile     bx_shutdown;
	pthread_cond_t    bx_shutcond;
};

/* Print messages to standard error during initialization.
 */
#define BOXD_PREFIX "solo-boxd"
#define boxd_fatal(fmt, ...) \
   fprintf(stderr, BOXD_PREFIX ": fatal: " fmt "\n", ## __VA_ARGS__)
#define boxd_warning(fmt, ...) \
   fprintf(stderr, BOXD_PREFIX ": warning: " fmt "\n", ## __VA_ARGS__)
#define boxd_info(fmt, ...) \
   fprintf(stderr, BOXD_PREFIX ": " fmt "\n", ## __VA_ARGS__)

/* Private functions.
 */
extern int    boxd_listen(struct sobx *);    /* boxd-listen.c */
extern void * boxd_serve__thread(void *);    /* boxd-serve.c  */
extern void * boxd_sigwait__thread(void *);  /* boxd-signal.c */

/* Protocol handlers.
 */
extern int    boxd_serve_close   (struct sobx_ses *, struct fus_m_close const *, size_t);
extern int    boxd_serve_login   (struct sobx_ses *, struct fus_m_login const *, size_t);
extern int    boxd_serve_mkdir   (struct sobx_ses *, struct fus_m_mkdir const *, size_t);
extern int    boxd_serve_open    (struct sobx_ses *, struct fus_m_open const *, size_t);
extern int    boxd_serve_read    (struct sobx_ses *, struct fus_m_read const *, size_t);
extern int    boxd_serve_readdir (struct sobx_ses *, struct fus_m_readdir const *, size_t);
extern int    boxd_serve_rename  (struct sobx_ses *, struct fus_m_rename const *, size_t);
extern int    boxd_serve_rmdir   (struct sobx_ses *, struct fus_m_rmdir const *, size_t);
extern int    boxd_serve_stat    (struct sobx_ses *, struct fus_m_stat const *, size_t);
extern int    boxd_serve_statfs  (struct sobx_ses *, struct fus_m_statfs const *, size_t);
extern int    boxd_serve_trunc   (struct sobx_ses *, struct fus_m_trunc const *, size_t);
extern int    boxd_serve_unlink  (struct sobx_ses *, struct fus_m_unlink const *, size_t);
extern int    boxd_serve_write   (struct sobx_ses *, struct fus_m_write const *, size_t);

/**
 * Perform basic checks on the path validity and strip leading / chatacters to
 * convert path to a relative path. Returns NULL if path is found to be
 * invalid.
 */
static inline char const *
_boxdpath(char const * path, size_t const lenz)
{
	char const * const s = path;
	char const * e;
	char c;

	if (lenz < 2)
		return NULL;
	if (path[lenz - 1])
		return NULL;
	while (*path == '/')
		path++;

	// FIXME
	//	The following restriction that the path may not contain a ".."
	//	substring is too restrictive.
	//
	if (strstr(path, "..") || strstr(path, "./"))
		return NULL;

	// FIXME
	//	The following restriction that the path must contain only
	//	printable ASCII characters is too restrictive.
	//
	for (e = path; (c = *e) && isascii(c) && isprint(c); e++)
		continue;
	if (*e)
		return NULL;
	if ((size_t) (e - s) != lenz - 1)
		return NULL;


	return *path ? path : ".";
}

/**
 * Store file descriptor in open file descriptor table.
 */
static inline int
_addfd(struct sobx_ses * const ses, int const fd)
{
	int * const v = ses->bxs_fdv;
	size_t const dim = GCC_DIM(ses->bxs_fdv);
	size_t i, h;

	if (fd < 0)
		return EBADF;

	for (i = 0, h = fd % dim;
	     i < dim;
	     i++, h = (h + 1) % dim)
	{
		if (v[h] == -1) {
			v[h] = fd;
			return 0;
		}
	}

	return ENFILE;
}

/**
 * Delete file descriptor from the open file table.
 */
static inline int
_delfd(struct sobx_ses * const ses, int const fd)
{
	int * const v = ses->bxs_fdv;
	size_t const dim = GCC_DIM(ses->bxs_fdv);
	size_t i, h;

	if (fd < 0)
		return EBADF;

	for (i = 0, h = fd % dim;
	     i < dim;
	     i++, h = (h + 1) % dim)
	{
		if (v[h] == fd) {
			v[h] = -1;
			return 0;
		}
	}

	return EBADF;
}

/**
 * Check existance of file descriptor in the open file table.
 */
static inline int
_lookupfd(struct sobx_ses const * const ses, int const fd)
{
	int const * const v = ses->bxs_fdv;
	size_t const dim = GCC_DIM(ses->bxs_fdv);
	size_t i, h;

	if (fd < 0)
		return EBADF;

	for (i = 0, h = fd % dim;
	     i < dim;
	     i++, h = (h + 1) % dim)
	{
		if (v[h] == fd)
			return 0;
	}

	return EBADF;
}

#endif
