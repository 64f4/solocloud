/*
 * boxd-serve-close.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/stat.h>
#include <sys/types.h>

/* Private header */
#include "boxd.h"

int
boxd_serve_close(
	struct sobx_ses          * const ses,
	struct fus_m_close const * const req,
	size_t                     const reqsize
	)
{
	log_t       * const log  = ses->bxs_log;
	solo_conn_t * const conn = ses->bxs_conn;
	int           err;

	log_entering(log);

	/* verify basic request structure */
	if (reqsize != sizeof(struct fus_m_close)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	/* file descriptor to be closed */
	int const fd = req->fus_q_close_fd;

	log_printf(log, LOG_DEBUG, "CLOSE [%d]", fd);

	/* make sure the file descriptor belongs to us */
	if ((err = _delfd(ses, fd))) {
		log_printf(log, LOG_ERR, "bad file descriptor");
		goto fail;
	}

	/* execute close() system call */
	if ((err = close(fd) ? errno : 0)) {
		log_perror(log, LOG_ERR, "close()", err);
		goto fail;
	}

	/* Submit CLOSE success response.
	 */
	struct fus_m_close_r const rsp = {
		.fus_r_close_magic = FUS_MAGIC_CLOSE,
	};
	if ((err = solo_send(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		goto fail;
	}

	log_leaving(log);
	return 0;

	/* Submit CLOSE error response.
	 */
fail:	log_assert(log, err);
	if (err != EPIPE) {
		struct fus_m_close_r const badrsp = {
			.fus_r_close_magic = FUS_MAGIC_CLOSE,
			.fus_r_close_err   = fus_errno_os2fus(err),
		};
		(void) solo_send(conn, &badrsp, sizeof(badrsp));
	}
	log_leaving(log);
	return err;
}
