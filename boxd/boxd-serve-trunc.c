/*
 * boxd-serve-trunc.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <fcntl.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>

/* Private header */
#include "boxd.h"

int
boxd_serve_trunc(
	struct sobx_ses          * const ses,
	struct fus_m_trunc const * const req,
	size_t                     const reqsize
	)
{
	log_t       * const log  = ses->bxs_log;
	solo_conn_t * const conn = ses->bxs_conn;
	int           err;

	log_entering(log);

	/* verify basic request structure */
	if (reqsize != sizeof(struct fus_m_trunc)) {
		log_unexpected(log);
		log_leaving(log);
		return EPROTO;
	}

	/* file descriptor and offset */
	int const fd = req->fus_q_trunc_fd;
	off_t const offs = req->fus_q_trunc_offs;

	/* check file descriptor */
	if ((err = _lookupfd(ses, fd))) {
		log_printf(log, LOG_ERR, "bad file descriptor");
		goto fail_badf;
	}

	log_printf(log, LOG_DEBUG, "TRUNC [%d] @%lld", fd, (long long) offs);

	/* lock file for writing */
	if ((err = flock(fd, LOCK_EX) ? errno : 0)) {
		log_perror(log, LOG_ERR, "flock()", err);
		goto fail_lock;
	}
	/* execute ftruncate() system call */
	if ((err = ftruncate(fd, offs) ? errno : 0)) {
		log_perror(log, LOG_ERR, "flock()", err);
		goto fail_trunc;
	}

	/* Submite TRUNC success response.
	 */
	struct fus_m_trunc_r const rsp = {
		.fus_r_trunc_magic = FUS_MAGIC_TRUNC,
	};
	if ((err = solo_send(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		goto fail_send;
	}

	/* unlock file */
	flock(fd, LOCK_UN);
	log_leaving(log);
	return 0;

fail_send:
fail_trunc:
	flock(fd, LOCK_UN);
fail_lock:
fail_badf:
	/* Submit TRUNC error response.
	 */
	if (err != EPIPE) {
		struct fus_m_trunc_r const badrsp = {
			.fus_r_trunc_magic = FUS_MAGIC_TRUNC,
			.fus_r_trunc_err   = fus_errno_os2fus(err),
		};
		(void) solo_send(conn, &badrsp, sizeof(badrsp));
	}
	log_leaving(log);
	return err;
}
