                         SOLOCLOUD DEMO README
                           September 13, 2012

SUPPORTED PLATFORMS:

    Max OS X 10.7 or later

REQUIREMENTS:

    This demo requires OSXFUSE (http://osxfuse.github.com). Please
    install OSXFUSE before running this demo.

HOW TO RUN THE DEMO:

    Double-click on the CloudBoxDemo application, then enter the
    password "test" when asked in the Terminal window. On success, a
    Finder window will pop up showing the remote CloudBox filesystem.
    When done, unmount the CloudBox filesystem by clicking on the
    "Eject" button in the Finder.

WHAT IT DOES:

    The demo demonstrates the client-side portion of SoloCloud by
    connecting to a preexisting test box in an undisclosed location (my
    house ;-) This is a proof-of-concept demo showing that no matter
    where you are in the world, you always have access to your (i.e.
    mine) CloudBox. The beauty of this idea is that anyone can install a
    CloudBox in their home, because it does not require any special
    technical skills. There is no need to change the firewall settings
    of your router, obtain a static IP address, or perform other tweaks
    normally needed to run a home server. Everything takes place
    transparently through a P2P connection in a way similar to how Skype
    establishes communications between callers [but see CAVEATS below].

    The server-side portion of SoloCloud is still quite unpolished so I
    do not yet feel comfortable making it available for download. A
    face-to-face demo can be still be scheduled by contacting me
    directly at <ea@paradoxity.com>.

CAVEATS:

    This demo is just a proof of concept and as such it has numerous
    caveats.

    For starters, the demo does not yet take advantage of P2P due to a
    lack of time on my part. Instead, a proxy server is being used to
    allow access to the test box from anywhere in the world, even if the
    box is behind a firewall.

    In part due to the use of a proxy, and in part due to the almost
    complete absence of caching, the demo is much slower than it should
    be. To make things worse, the Finder issues a myriad of needless
    operations on the filesystem (apparently that application was not
    designed with remote filesystems in mind), further slowing down the
    demo.

    I'm hoping to work out all of these issues in the weeks to come.

CONTACT INFO:

    Emanuele Altieri <ea@paradoxity.com>
