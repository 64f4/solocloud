#!/bin/bash
#
# (C) 2012 Emanuele Altieri
#
# YCombinator demo script.
#

readonly ME=$(basename $0)

# Box we will be connecting to
readonly BOXID="test-box"

# Directory where we will be mounting the file system (temporary)
readonly MOUNTDIR="/tmp/solo-fs.$(date +%s).$RANDOM$RANDOM"

# Switchboard server
readonly SWITCHBOARD="ec2-107-20-78-222.compute-1.amazonaws.com"

function quit_in()
{
	local -i i=$1
	while [ $i -gt 0 ]
	do
		printf "\r[ press CTRL^C to end or wait %2d sec ]" $i
		sleep 1
		i=$((i-1))
	done
	exit 1
}

function fail()
{
	echo "$*" >&2
	quit_in 60
}

function info()
{
	echo "$*" >&2
}

function onexit()
{
	# remove temporary directory
	rmdir $MOUNTDIR &>/dev/null
}

function init()
{
	# clear screen
	clear
	# check whether OSXFUSE is installed
	if [ ! -d /Library/Filesystems/osxfusefs.fs ]
	then
		info "Sorry, OSXFUSE does not seem to be installed on this system."
		info "Please download it from http://osxfuse.github.com"
		open http://osxfuse.github.com
		quit_in 60
	fi
}

function main()
{
	trap onexit EXIT

	# chdir() to script directory
	cd $(dirname "$0") 2>/dev/null || \
		fail "Cannot chdir()"
	
	# create temporary directory where to mount the file system
	mkdir -p $MOUNTDIR &>/dev/null || \
		fail "Cannot create temporary directory $MOUNTDIR"

	# mount FUSE filesystem
	info "Connecting to CloudBox \`$BOXID'"
	./solo-fs $MOUNTDIR $BOXID $SWITCHBOARD &>/dev/null
	case $? in
	0)
		;;
	# EX_NOHOST
	68)
		fail "Cannot locate box \"$BOXID\"" ;;
	# EX_NOPERM
	77)
		fail "Invalid password" ;;
	*)
		fail "Service is currently unavailable" ;;
	esac

	# wait for file system to be mounted
	info "Mounting file system..."
	while [ 1 ];
	do
		sleep 1
		mount | /usr/bin/grep $MOUNTDIR &>/dev/null && break
	done

	# open mount directory with the Finder
	open $MOUNTDIR

	# wait for file system to be unmounted
	while [ 1 ];
	do
		sleep 1
		mount | /usr/bin/grep $MOUNTDIR &>/dev/null || break
	done
	info "Unmounting file system"
}

init
main
