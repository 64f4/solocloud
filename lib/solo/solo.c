/*
 * solo.c
 *
 *	SoloCloud protocol implementation.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <string.h>

#include "solo-internals.h"

int
solo_init(
	log_t                 * const log,
	struct solo_cf const  * const cf,
	struct solo          ** const newsolo )
{
	struct solo           * solo;
	struct solo_cf const  * use_cf;
	int                     err;

	// TODO
	//	Assemble a new configuration structure from scratch using the
	//	given `cf' paramaters, replacing every NULL field with its
	//	default value. Otherwise, every time we use a configuration
	//	field we need to check whether it is NULL first and, if so,
	//	select its default value, which is quite inconvenient. This is
	//	exactly what we are doing for the time being.

	if (cf)
		use_cf = cf;
	else {
		static struct solo_cf const default_cf = {
			.socf_server = SOLO_DEFAULT_SWITCHBOARD,
			.socf_port   = SOLO_DEFAULT_PORT,
		};
		use_cf = &default_cf;
	}

	if ((err = mm_malloc_t(log, &solo, struct solo))) {
		*newsolo = NULL;
		return err;
	}

	*solo = (struct solo) {
		.so_cf  = *use_cf,
		.so_log = log,
	};

	*newsolo = solo;
	return 0;
}

void
solo_uninit(struct solo * const solo)
{
	log_t * const log = solo->so_log;
	/* server identifier */
	if (solo->so_id)
		mm_free(log, solo->so_id);
	/* terminate connection with switchboard server */
	if (solo->so_switchd)
		tcp_close(solo->so_switchd);
	/* delete library instance */
	mm_free(log, solo);
}

void
solo_shutdown(struct solo * const solo)
{
	/* drop connection with the Solo Switchboard */
	if (solo->so_switchd) {
		solo->so_shutdown = true;
		tcp_drop(solo->so_switchd);
	}
}

/* ------------------------------------------------------------------------- */
/*                          Accessory Functions                              */
/* ------------------------------------------------------------------------- */

log_t *
solo_get_log(struct solo const * const solo)
{
	return solo->so_log;
}

log_t *
solo_conn_get_log(struct solo_conn const * const conn)
{
	return conn->sc_log;
}
