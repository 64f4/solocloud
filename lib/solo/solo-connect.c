/*
 * solo-connect.c
 *
 *	SoloCloud protocol implementation.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include "solo-internals.h"

int
solo_connect(
	struct solo       * const solo,
	char const        * const id,
	enum solo_proto     const proto,
	struct solo_conn ** const new_soloconn
	)
{
	log_t             * const log = solo->so_log;
	tcp_conn_t        * conn;
	struct solo_conn  * soloconn;
	int                 err;

	// TODO Use server/port information in configuration.

	log_entering(log);

	/* check server identifier */
	size_t const idlenz = strlen(id) + 1;
	if (idlenz > UINT8_MAX)
		return EINVAL;

	/* allocate connection object */
	if ((err = mm_malloc(log, &soloconn, sizeof(*soloconn))))
		goto fail_malloc;
	*soloconn = (struct solo_conn) {
		.sc_solo = solo,
		.sc_log  = log,
	};

	/* connect to switchboard */
	unsigned const port =
		solo->so_cf.socf_port ? : SOLO_DEFAULT_PORT;
	char const * const host =
		solo->so_cf.socf_server ? : SOLO_DEFAULT_SWITCHBOARD;
	log_printf(log, LOG_INFO, "connecting to switchboard %s:%u",
		   host, port);
	if ((err = tcp_connect(log, host, port, &conn))) {
		log_perror(log, LOG_TRACE, "tcp_connect()", err);
		goto fail_connect;
	}

	/* submit client registration message */
	struct solo_m_register const req = {
		.solo_q_reg_magic  = SOLO_MAGIC_REGISTER,
		.solo_q_reg_proto  = proto,             // protocol
		.solo_q_reg_peer   = SOLO_PEER_CLIENT,  // client connection
		.solo_q_reg_idlenz = idlenz,            // server ID length
	};
	struct iovec const iov[] = {
		{ (void *) &req, sizeof(req) },
		{ (void *) id, idlenz }
	};
	if ((err = tcp_sendv(conn, iov, GCC_DIM(iov)))) {
		log_perror(log, LOG_TRACE, "tcp_sendv()", err);
		goto fail;
	}

	/* fetch response from switchboard server */
	struct solo_m_register_r rsp;
	if ((err = tcp_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "tcp_recvn()", err);
		goto fail;
	}
	if (rsp.solo_r_reg_magic != SOLO_MAGIC_REGISTER) {
		log_unexpected(log);
		err = EPROTO;
		goto fail;
	}
	if (rsp.solo_r_reg_err != SOLO_OK) {
		char const * const s = solo_err_tostring(rsp.solo_r_reg_err);
		log_printf(log, LOG_ERR, "server said: %s", s);
		switch (rsp.solo_r_reg_err) {
		case SOLO_ENOENT:
			err = ESRCH;
			break;
		default:
			err = EPERM;
		}
		goto fail;
	}

	log_printf(log, LOG_INFO, "connected to `%s'", id);

	/* make it official */
	soloconn->sc_conn = conn;
	*new_soloconn = soloconn;

	/* cleanup */
	log_leaving(log);
	return 0;

fail:
	tcp_close(conn);
fail_connect:
	mm_free(log, soloconn);
fail_malloc:
	*new_soloconn = NULL;
	log_assert(log, err);
	log_leaving(log);
	return err;
}
