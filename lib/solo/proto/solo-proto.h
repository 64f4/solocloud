/**
 * solo-proto.h
 *
 *	SoloCloud protocol internals.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_LIB_SOLO_PROTO_H
#define SOLOCLOUD_LIB_SOLO_PROTO_H

#include <stdint.h>

#include "solo.h"

/* Magic numbers */
#define SOLO_MAGIC_REGISTER 0x6919ABCB4FF78F6Aull
#define SOLO_MAGIC_ACCEPT   0x32a59a8dul

/** Error codes */
enum solo_err
{
	SOLO_OK,               /// success
	SOLO_EACCESS,          /// permission denied
	SOLO_EBUSY,            /// system is busy
	SOLO_EEXIST,           /// already registered
	SOLO_ENOENT,           /// no such entity
};

/* Error codes (as string) */
static inline char const *
solo_err_tostring(int const e)
{
	static char const * const strv[] = {
		[SOLO_OK]      = "OK",
		[SOLO_EACCESS] = "EACCESS",
		[SOLO_EBUSY]   = "EBUSY",
		[SOLO_EEXIST]  = "EEXIST",
		[SOLO_ENOENT]  = "ENOENT",
	};

	if (e < 0 || (unsigned) e >= sizeof(strv) / sizeof(strv[0]))
		return "#INVALID#";

	return strv[e] ? : "#UNKNOWN#";
}

// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: //
//                               REGISTER                                  //
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: //

/**
 * Peer connection type.
 */
enum solo_peer
{
	SOLO_PEER_SERVER,
	SOLO_PEER_CLIENT,
};

/**
 * REGISTER request.
 *
 * When sent by a server to the switchboard, it is used to register the
 * server's presence.
 *
 * When sent by a client to the switchboard, it is used to eastablish a
 * connection with a server.
 */
struct solo_m_register
{
	uint64_t solo_q_reg_magic;   /// magic number (SOLO_MAGIC_REGISTER)
	uint16_t solo_q_reg_flags;   /// miscellaneous flags (TBD)
	uint16_t solo_q_reg_proto;   /// peer protocol type (`enum solo_proto')
	uint8_t  solo_q_reg_peer;    /// peer connection type (`enum solo_peer')

	/* Peer identifier: a nil-terminated, case-insensitive ASCII string
	 * containing only printable characters and no spaces. The identifier
	 * is interpreted differently based on the connection type (see
	 * `solo_q_reg_peer' field):
	 *
	 * - For SOLO_PEER_SERVER connection types: `solo_q_reg_id' identifies
	 *   the name of server performing the actual registration.
	 *
	 * - For SOLO_PEER_CLIENT connection types: `solo_q_reg_id' identifies
	 *   the name of server to which the client wishes to connect.
	 */
	uint8_t  solo_q_reg_idlenz;  /// length of the peer identifier
	char     solo_q_reg_id[];    /// peer identifier

} GCC_PACKED;

/**
 * REGISTER response. Sent by the switchboard to a server indicating the
 * result of the registration.
 */
struct solo_m_register_r
{
	uint64_t solo_r_reg_magic;   /// magic number (SOLO_MAGIC_REGISTER)
	uint32_t solo_r_reg_err;

} GCC_PACKED;

// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: //
//                                ACCEPT                                   //
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: //

/* Flags for ACCEPT requests.
 */
#define SOLO_FLAG_PROXY 0x1          /// use Proxy

/**
 * ACCEPT request. Sent by a server to the switchboard to indicate that the
 * server is ready to accept a client connection. The call should block until
 * a connection is available.
 */
struct solo_m_accept
{
	uint32_t solo_q_acc_magic;   /// magic number (SOLO_MAGIC_ACCEPT)
	uint32_t solo_q_acc_flags;   /// flags (currently unused)

} GCC_PACKED;

/**
 * ACCEPT response. Sent by the switchboard to a server to indicate that a
 * client connection is now available.
 */
struct solo_m_accept_r
{
	uint32_t solo_r_acc_magic;   /// magic number (SOLO_MAGIC_ACCEPT)
	uint32_t solo_r_acc_err;     /// error code (enum solo_err)

	// TODO Proxy could be a different server than the switchboard.

	/* if no errors... */
	uint16_t solo_r_acc_port;    /// proxy port number

} GCC_PACKED;

#endif
