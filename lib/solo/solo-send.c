/*
 * solo-send.c
 *
 *	SoloCloud protocol implementation.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include "solo-internals.h"

int
solo_send(
	struct solo_conn * const soconn,
	void const       * const payload,
	size_t             const payload_size
	)
{
	return tcp_send(soconn->sc_conn, payload, payload_size);
}

int
solo_sendv(
	struct solo_conn   * soconn,
	struct iovec const * iov,
	int unsigned         iocnt
	)
{
	return tcp_sendv(soconn->sc_conn, iov, iocnt);
}

int
solo_sendfile(
	struct solo_conn   * soconn,
	struct iovec const * iov,
	size_t               iocnt,
	int                  fd,
	off_t                offs,
	size_t               count
	)
{
	return tcp_sendfile(soconn->sc_conn, iov, iocnt, fd, offs, count);
}
