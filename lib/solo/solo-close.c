/*
 * solo-close.c
 *
 *	SoloCloud protocol implementation.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include "solo-internals.h"

void
solo_drop(struct solo_conn * const soconn)
{
	tcp_drop(soconn->sc_conn);
}

void
solo_close(struct solo_conn * const soconn)
{
	log_t * const log = soconn->sc_log;
	tcp_close(soconn->sc_conn);
	mm_free(log, soconn);
}
