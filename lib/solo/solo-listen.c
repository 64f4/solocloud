/*
 * solo-listen.c
 *
 *	SoloCloud protocol implementation.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include "solo-internals.h"

int
solo_listen(
	struct solo     * const solo,
	char const      * const id,
	enum solo_proto   const proto )
{
	log_t           * const log = solo->so_log;
	tcp_conn_t      * conn;
	char            * dupid;
	int               err;

	/* check identifier */
	size_t const idlenz = strlen(id) + 1;
	if (idlenz > UINT8_MAX)
		return EINVAL;

	// FIXME !
	//	The code below is a hack. We need to clean up potential
	//	leftovers from a previous switchboard connection because of an
	//	API design flaw.  solo_listen() should really return a server
	//	handle, which can then be reclaimed by a yet-to-be-implemented
	//	solo_terminate() function.
	//
	// BEGIN HACK
	//
	/* cleanup previous connection */
	if (solo->so_switchd) {
		tcp_close(solo->so_switchd);
		solo->so_switchd = NULL;
	}
	/* cleanup previous server identifier */
	if (solo->so_id) {
		mm_free(log, solo->so_id);
		solo->so_id = NULL;
	}
	//
	// END HACK

	// FIXME
	//	Needs a way to interrupt tcp_connect()

	/* connect to switchboard server */
	unsigned const port =
		solo->so_cf.socf_port ? : SOLO_DEFAULT_PORT;
	char const * const host =
		solo->so_cf.socf_server ? : SOLO_DEFAULT_SWITCHBOARD;
	log_printf(log, LOG_INFO, "connecting to switchboard %s:%u", host, port);
	if ((err = tcp_connect(log, host, port, &conn))) {
		log_perror(log, LOG_TRACE, "tcp_connect()", err);
		goto fail_0;
	}

	/* register server with switchboard */
	struct solo_m_register const req = {
		.solo_q_reg_magic  = SOLO_MAGIC_REGISTER,
		.solo_q_reg_proto  = proto,             // protocol
		.solo_q_reg_peer   = SOLO_PEER_SERVER,  // server connection
		.solo_q_reg_idlenz = idlenz,            // server ID length
	};
	struct iovec const iov[] = {
		{ (void *) &req, sizeof(req) },
		{ (void *) id, idlenz }
	};
	if ((err = tcp_sendv(conn, iov, GCC_DIM(iov)))) {
		log_perror(log, LOG_TRACE, "tcp_sendv()", err);
		goto fail;
	}

	/* fetch response from switchboard server */
	struct solo_m_register_r rsp;
	if ((err = tcp_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "tcp_recvn()", err);
		goto fail;
	}
	if (rsp.solo_r_reg_magic != SOLO_MAGIC_REGISTER) {
		log_unexpected(log);
		err = EPROTO;
		goto fail;
	}
	if (rsp.solo_r_reg_err != SOLO_OK) {
		char const * const s = solo_err_tostring(rsp.solo_r_reg_err);
		log_printf(log, LOG_ERR, "server said: %s", s);
		err = EPERM;  // FIXME Needs more appropriate error code
		goto fail;
	}

	/* duplicate identifier string */
	if ((err = mm_dup(log, id, &dupid, idlenz)))
		goto fail;

	// FIXME
	//	I am current seeing a bizarre behavior regarding the TCP
	//	keepalive probes. After unplugging the box from the network
	//	(by turning off the wireless adapter), I still see the
	//	keepalive probes succeed. That is, using `tcpdump', I see
	//	both the box probe and the switchboard reply probe, as if the
	//	network never went down.  Why?
	//
	//	Should the SO_KEEPALIVE socket option fail, we will have to
	//	implement keepalive probes in our TCP layer.
	//

	// TODO
	//	Revisit the TCP keepalive parameters.

	/* Set TCP keepalive probes for the connection with the switchboard.
	 * It is important that we keep monitoring this connection, as it is
	 * the only way we get notified of new clients. If the connection
	 * drops, we need to try to re-establish it.
	 */
	tcp_keepalive(conn, 10,  /* start delay (sec)      */
		            30,  /* probes interval (sec)  */
			    1);  /* number of probes       */

	/* make it official */
	solo->so_switchd = conn;
	solo->so_id = dupid;

	log_printf(log, LOG_INFO, "registered as `%s'", id);
	return 0;

fail:	tcp_close(conn);
fail_0:	log_assert(log, err);
	return err;
}
