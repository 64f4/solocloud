/*
 * solo-recv.c
 *
 *	SoloCloud protocol implementation.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include "solo-internals.h"

int
solo_recv(
	struct solo_conn  * const soconn,
	void              * const buf,
	size_t              const buf_size,
	size_t            * const payload_size
	)
{
	return tcp_recv(soconn->sc_conn, buf, buf_size, payload_size);
}

int
solo_recvn(
	struct solo_conn  * const soconn,
	void              * const payload,
	size_t              const payload_size
	)
{
	return tcp_recvn(soconn->sc_conn, payload, payload_size);
}

int
solo_recva(
	struct solo_conn  * const soconn,
	void             ** const payload,      // [out]
	size_t            * const payload_size  // [out]
	)
{
	return tcp_recva(soconn->sc_conn, payload, payload_size);
}
