/**
 * solo-internals.h
 *
 *	SoloCloud internal header.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_LIB_SOLO_INTERNALS_H
#define SOLOCLOUD_LIB_SOLO_INTERNALS_H

#include <stdbool.h>
#include <string.h>
#include <unistd.h>

/* Project header files */
#include "malloc.h"      // memory management
#include "tcp.h"         // TCP communication library

/* SoloCloud header files */
#include "solo.h"        // our public API
#include "solo-proto.h"  // protocol definition

/* Default configuration parameters.
 */
#ifndef SOLO_DEFAULT_SWITCHBOARD
#define SOLO_DEFAULT_SWITCHBOARD "localhost" /// default switchboard server
#endif
#ifndef SOLO_DEFAULT_PORT
#define SOLO_DEFAULT_PORT        29909       /// default switchboard port
#endif

/**
 * Library instance.
 */
struct solo
{
	struct solo_cf    so_cf;        // configuration
	log_t           * so_log;       // log stream
	tcp_conn_t      * so_switchd;   // connection to the switchboard
	char            * so_id;        // identifier
	bool volatile     so_shutdown;  // shutting down
};

/**
 * Connection.
 */
struct solo_conn
{
	solo_t          * sc_solo;      // library instance
	log_t           * sc_log;       // log stream
	tcp_conn_t      * sc_conn;      // peer connection
};

#endif
