/*
 * solo-accept.c
 *
 *	SoloCloud protocol implementation.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include "solo-internals.h"

int
solo_accept(
	struct solo       * const solo,
	struct solo_conn ** const new_soloconn
	)
{
	log_t             * const log     = solo->so_log;
	tcp_conn_t        * const switchd = solo->so_switchd;
	tcp_conn_t        * conn;
	struct solo_conn  * soloconn;
	int                 err;

	// FIXME
	//	This entire function should be protected by a mutex. It is not
	//	possible to issue multiple ACCEPT requests to the server (we
	//	have to wait until an ACCEPT reply is received before issuing
	//	the next request). However, a server may still want to issue
	//	multiple solo_accept() calls in parallel from distinct
	//	threads.

	log_entering(log);

	/* allocate connection object */
	if ((err = mm_malloc(log, &soloconn, sizeof(*soloconn))))
		goto fail_malloc;
	*soloconn = (struct solo_conn) {
		.sc_solo = solo,
		.sc_log  = log,
	};

	/* send ACCEPT request to switchboard */
	struct solo_m_accept const req = {
		.solo_q_acc_magic = SOLO_MAGIC_ACCEPT,
		.solo_q_acc_flags = SOLO_FLAG_PROXY,
	};
	if ((err = tcp_send(switchd, &req, sizeof(req)))) {
		log_perror(log, LOG_TRACE, "tcp_send()", err);
		goto fail;
	}

	/* switchboard should block until a client connection becomes
	 * available...
	 */

	/* read ACCEPT response */
	struct solo_m_accept_r rsp;
	if ((err = tcp_recvn(switchd, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_ERR, "tcp_recvn()", err);
		goto fail;
	}
	if (rsp.solo_r_acc_magic != SOLO_MAGIC_ACCEPT) {
		log_unexpected(log);
		err = EPROTO;
		goto fail;
	}
	if (rsp.solo_r_acc_err) {
		char const * const s = solo_err_tostring(rsp.solo_r_acc_err);
		log_printf(log, LOG_ERR, "server error: %s", s);
		err = EPERM;
		goto fail;
	}
	/* we are expecting a dynamic port number */
	int unsigned const port = rsp.solo_r_acc_port;
	log_printf(log, LOG_DEBUG, "connecting to port %u", (unsigned) port);
	if (port > UINT16_MAX) {
		log_unexpected(log);
		err = EPROTO;
		goto fail;
	}

	// FIXME
	//	We need to be able to abort tcp_connect().

	/* connect to the proxy port number */
	char const * const host =
		solo->so_cf.socf_server ? : SOLO_DEFAULT_SWITCHBOARD;
	if ((err = tcp_connect(log, host, port, &conn))) {
		log_perror(log, LOG_TRACE, "tcp_connect()", err);
		err = ECANCELED; // most likely error
		goto fail;
	}
	soloconn->sc_conn = conn;

	/* success */
	*new_soloconn = soloconn;
	log_leaving(log);
	return 0;

fail:
	mm_free(log, soloconn);
fail_malloc:
	*new_soloconn = NULL;
	log_assert(log, err);
	log_leaving(log);
	return err == EPIPE && solo->so_shutdown ? ESHUTDOWN : err;
}
