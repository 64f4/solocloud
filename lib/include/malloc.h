/*
 * malloc.h
 *
 *	Wrappers for malloc functions.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_MALLOC_H
#define SOLOCLOUD_MALLOC_H

#include <errno.h>
#include <stdlib.h>

#include "log.h"    // logging

/**
 * Like malloc(), but returns an error code (0 on success) and logs on error.
 *
 * @param log
 *	Log stream.
 * @param [out] pptr
 *	The newly allocated memory, or NULL on error.
 * @param size
 *	Size of the memory to be allocated.
 * @return
 *	System error code (zero on succes, or likley ENOMEM)
 */
#define mm_malloc(log, pptr, size) \
({ \
	int __err__ = 0; \
	typeof (*(pptr)) const __ptr__ = malloc(size); \
	*(pptr) = __ptr__; \
	if (__ptr__ == NULL) { \
		__err__ = errno; \
		log_perror((log), LOG_ERR, "malloc()", __err__); \
	} \
	__err__; \
})

/**
 * Like mm_malloc(), but takes a type as one of the arguments, allowing
 * compile-time type checking on the `pptr' argument.
 */
#define mm_malloc_t(log, pptr, T) \
({ \
	int __err__ = 0; \
	T * const __ptr__ = malloc(sizeof(T)); \
	*(pptr) = __ptr__; \
	if (__ptr__ == NULL) { \
		__err__ = errno; \
		log_perror((log), LOG_ERR, "malloc()", __err__); \
	} \
	__err__; \
})

/**
 * A simple wrapper for free().
 */
#define mm_free(log, ptr) \
do { \
	typeof (ptr) const __ptr__ = (ptr); \
	log_assert(log, __ptr__ != NULL); \
	free(__ptr__); \
} while(0)

/**
 * Duplicate region of memory.
 */
#define mm_dup(log, src, pdst, size) \
({ \
	int __err__ = 0; \
	size_t const __size__ = (size); \
	typeof (*(pdst)) const __ptr__ = malloc(__size__); \
	*(pdst) = __ptr__; \
	if (__ptr__) \
		memcpy(__ptr__, (src), __size__); \
	else { \
		__err__ = errno; \
		log_perror((log), LOG_ERR, "malloc()", __err__); \
	} \
	__err__; \
})


#endif
