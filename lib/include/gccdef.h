/**
 * stddef.h
 *
 *	Miscellaneous macros.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_STDDEF_H
#define SOLOCLOUD_STDDEF_H

/** Unused declaration. To prevent accidental use, the name is modified. */
#define GCC_ATTRIBUTE_UNUSED(d) \
    d ## __unused __attribute__ ((unused))

/** A function which does not return */
#define GCC_ATTRIBUTE_NORETURN \
   __attribute__ ((noreturn))

/** A function that requires printf-like format arguments */
#define GCC_ATTRIBUTE_PRINTF(fmtix, argix) \
   __attribute__ ((format (printf, fmtix, argix)))

/** Warn when the result of a function is unchecked */
#define GCC_WARN_UNUSED_RESULT \
   __attribute__ ((warn_unused_result))

/** Predicts a true condition */
#define GCC_LIKELY(cond) \
   __builtin_expect(cond, 1)

/** Predicts a false condition */
#define GCC_UNLIKELY(cond) \
   __builtin_expect(cond, 0)

/** Prefetch value in memory */
#define GCC_PREFETCH(...) \
   __builtin_prefetch(__VA_ARGS__)

/** Return the offset of a member in a structure */
#define GCC_OFFSET_OF(type, member) \
   __builtin_offsetof(type, member)

/** Cast a member of a structure out to the containing structure */
#define GCC_CONTAINER_OF(ptr, type, member) ({ \
   const typeof( ((type *) 0)->member ) * _mptr = (ptr); \
   (type *)( (char *) _mptr - GCC_OFFSET_OF(type, member) ); })

/** Packed structure */
#define GCC_PACKED \
   __attribute__ ((packed))

/** Align structure or variable */
#define GCC_ALIGNED(n) \
    __attribute__ ((aligned(n)))

/** Return minimum of... */
#define GCC_MIN(a, b) \
({ typeof (a) const _a = (a); \
   typeof (b) const _b = (b); \
   _a < _b ? _a : _b;         })

/** Return maximum of... */
#define GCC_MAX(a, b) \
({ typeof (a) const _a = (a); \
   typeof (b) const _b = (b); \
   _a > _b ? _a : _b;         })

/** Return the dimension of an vector */
#define GCC_DIM(v) \
   (sizeof(v) / sizeof(v[0]))

#endif
