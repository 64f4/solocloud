/**
 * pthread.h
 *
 *	Safe wrappers for pthread calls.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_SAFEPTHREAD_H
#define SOLOCLOUD_SAFEPTHREAD_H

#include <assert.h>
#include <errno.h>
#include <pthread.h>

#include "gccdef.h"

/* Warn when any of these functions is used without checking its return value.
 */

GCC_WARN_UNUSED_RESULT int
	pthread_mutex_init(),
	pthread_mutex_destroy(),
	pthread_mutex_lock(),
	pthread_mutex_unlock(),
	pthread_cond_init(),
	pthread_cond_wait(),
	pthread_cond_timedwait(),
	pthread_cond_signal(),
	pthread_cond_broadcast(),
	pthread_detach();

/* Wrappers for functions that cannot fail unless there is a bug somewhere.
 * Notice that we are using macros (as opposed to, say, inline functions) so
 * that in the event of a core dump, the debugger will show the meaningful
 * origin of the crash.
 */

#define Pthread_create(...) \
	assert(!pthread_create(__VA_ARGS__))

#define Pthread_mutex_init(...) \
	assert(!pthread_mutex_init(__VA_ARGS__))

#define Pthread_mutex_lock(...) \
	assert(!pthread_mutex_lock(__VA_ARGS__))

#define Pthread_mutex_unlock(...) \
	assert(!pthread_mutex_unlock(__VA_ARGS__))

#define Pthread_mutex_destroy(...) \
	assert(!pthread_mutex_destroy(__VA_ARGS__))

#define Pthread_cond_destroy(...) \
	assert(!pthread_cond_destroy(__VA_ARGS__))

#define Pthread_cond_init(...) \
	assert(!pthread_cond_init(__VA_ARGS__))

#define Pthread_cond_signal(...) \
	assert(!pthread_cond_signal(__VA_ARGS__))

#define Pthread_cond_broadcast(...) \
	assert(!pthread_cond_broadcast(__VA_ARGS__))

#define Pthread_cond_wait(...) \
	assert(!pthread_cond_wait(__VA_ARGS__))

#define Pthread_cond_timedwait(...) ({ \
	int const __err = pthread_cond_timedwait(__VA_ARGS__); \
	assert(__err == 0 || __err == ETIMEDOUT); \
	__err; \
})

#define Pthread_detach(id) \
	assert(!pthread_detach(id))

#endif
