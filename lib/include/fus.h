/**
 * fus-proto.h
 *
 *	SoloCloud Filesystem in Userspace -- Public API.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_LIB_FUS_H
#define SOLOCLOUD_LIB_FUS_H

/* System headers */
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/types.h>

/* Project headers */
#include "solo.h"

/** File descriptor (server side) */
typedef int fus_fd_t;

/** Callback invoked by fus_readdir() */
typedef int (* fus_filler_f) (
	void              * arg,   // arbitrary data
	char const        * name,  // entry name
	char const        * path,  // entry full path
	struct stat const * stbuf  // entry stat() result
	);

/*
 * FILE-SYSTEM OPERATIONS.
 *
 * By convention, all operations return a system error code as defined in the
 * <errno.h> system header file. This forces some operations to return data
 * through one of the arguments (see fus_open(), fus_pread(), etc.) Aside from
 * this difference, all methods should appear identical to their corresponding
 * POSIX system calls.
 */

/**
 * close()
 */
extern int
fus_close(
	solo_conn_t  * conn,
	fus_fd_t       fd
	);

/**
 * ftruncate()
 */
extern int
fus_ftruncate(
	solo_conn_t  * conn,
	fus_fd_t       fd,
	off_t          length
	);

/**
 * login()
 */
extern int
fus_login(
	solo_conn_t  * conn,
	char const   * pass
	);

/**
 * mkdir()
 */
extern int
fus_mkdir(
	solo_conn_t  * conn,
	char const   * path,
	mode_t         mode     // [unused]
	);

/**
 * open()
 */
extern int
fus_open(
	solo_conn_t  * conn,
	char const   * path,
	int            flags,
	mode_t         mode,    // [unused]
	fus_fd_t     * fd       // [out]
	);

/**
 * pread()
 */
extern int
fus_pread(
	solo_conn_t  * conn,
	fus_fd_t       fd,
	void         * buf,     // [out]
	size_t       * count,   // [in/out]
	off_t          offset
	);

/**
 * pwrite()
 */
extern int
fus_pwrite(
	solo_conn_t  * conn,
	fus_fd_t       fd,
	char const   * buf,
	size_t       * count,   // [in/out]
	off_t          offset
	);

/**
 * readdir()
 */
extern int
fus_readdir(
	solo_conn_t  * conn,
	char const   * path,
	void         * arg,
	fus_filler_f   filler
	);

/**
 * rename()
 */
extern int
fus_rename(
	solo_conn_t  * conn,
	char const   * src,
	char const   * dst
	);

/**
 * rmdir()
 */
extern int
fus_rmdir(
	solo_conn_t  * conn,
	char const   * path
	);

/**
 * stat()
 */
extern int
fus_stat(
	solo_conn_t  * conn,
	char const   * path,
	struct stat  * stbuf    // [out]
	);

/**
 * statfs()
 */
extern int
fus_statvfs(
	solo_conn_t    * conn,
	char const     * path,
	struct statvfs * stbuf  // [out]
	);

/**
 * unlink()
 */
extern int
fus_unlink(
	solo_conn_t  * conn,
	char const   * path
	);

#endif
