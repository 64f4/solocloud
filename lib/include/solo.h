/**
 * solo.h
 *
 *	SoloCloud public interface.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_LIB_SOLO_H
#define SOLOCLOUD_LIB_SOLO_H

#include <errno.h>    // error codes returned by functions
#include <sys/uio.h>  // for `struct iovec'

#include "log.h"    // logging facilities

/** SoloCloud library instance */
typedef struct solo solo_t;

/** SoloCloud client connection */
typedef struct solo_conn solo_conn_t;

/** Configuration parameters to be passed to solo_init() */
struct solo_cf {
	char const     * socf_server;  // switchboard server name
	short unsigned   socf_port;    // switchboard server port number
};

/**
 * Protocol types. Used by solo_listen().
 */
enum solo_proto
{
	/** Generic, raw protocol */
	SOLO_PROTO_RAW = 0,
	/** File system in user space (FUSE) protocol */
	SOLO_PROTO_FUS = 1,
};

/**
 * Instantiate a server or client library object.
 *
 * @param log
 *	Log stream.
 *
 * @param cf
 *	Optional configuration parameters. May be NULL.
 *
 * @param [out] newsolo
 *	New server or client library instance is stored in `*newsolo'
 *
 * @return
 *	Zero on success, otherwise a system error code.
 */
extern int solo_init(log_t * log, struct solo_cf const * cf, solo_t ** newsolo);

/**
 * Destroy library instance.
 */
extern void solo_uninit(solo_t * solo);

/**
 * Shutdown all library activities. This is a thread safe function which can
 * be called from within a signal-handling thread.
 */
extern void solo_shutdown(solo_t * solo);

/**
 * Start listening for connections.
 *
 * This function sets the SoloCloud descriptor to server mode.
 *
 * @param solo
 *	Library instance.
 *
 * @param id
 *	Server identifier.
 *
 * @param proto
 *	Protocol type.
 *
 * @return
 *	Zero on success, otherwise a system error code.
 *
 * FIXME
 *	solo_listen() needs to return a server descriptor (solo_srv_t). This
 *	allows a server to listen on different protocol numbers from within
 *	the same process.
 */
extern int solo_listen(solo_t * solo, char const * id, enum solo_proto proto);

/**
 * Accept a connection from a client.
 *
 * @param solo
 *	Library instance.
 *
 * @param [out] newconn
 *	New client connection.
 *
 * @return
 *	Zero on success, otherwise a system error code.
 */
extern int solo_accept(solo_t * solo, solo_conn_t ** newconn);

/**
 * Connect to server.
 *
 * @param solo
 *	Library instance.
 *
 * @param id
 *	Box identifier.
 *
 * @param proto
 *	Communication protocol.
 *
 * @param [out] conn
 *	Newly allocated connection, or NULL on error.
 *
 * @return
 *	Zero on success, otherwise a system error code. In particular, ESRCH
 *	is returned if a connection with the specified box ID could not be
 *	established.
 */
extern int solo_connect(
	solo_t           * solo,
	char const       * id,
	enum solo_proto    proto,
	solo_conn_t     ** newconn
	);

/**
 * Close connection.
 *
 * @param conn
 *	Connection created with solo_accept() or solo_connect().
 */
extern void solo_close(solo_conn_t * conn);

/**
 * Drop connection. This interrupts all communication on the given connection,
 * but it does not free the resources associated with the connection, so it is
 * safe to call this function from within a signal handling thread. A call to
 * solo_close() is still mandatory.
 */
extern void solo_drop(solo_conn_t * conn);

/**
 * Send message to peer.
 *
 * @param conn
 *	Connection created with solo_accept() or solo_connect().
 *
 * @param payload
 *	The payload of the message.
 *
 * @param payload_size
 *	Size of the payload.
 *
 * @return
 *	Zero on success, otherwise a system error code.
 */
extern int solo_send(
	solo_conn_t * conn,
	void const  * payload,
	size_t        payload_size
	);

/**
 * Send message to peer. This is the same as solo_sendv(), except that the
 * message is to be composed from multiple buffers.
 */
extern int solo_sendv(
	solo_conn_t        * conn,
	struct iovec const * iov,
	int unsigned         iocnt
	);

/**
 * See tcp_sendfile()
 */
extern int
solo_sendfile(
	solo_conn_t         * conn,
	struct iovec const  * iov,   // (may be NULL)
	size_t                iocnt,
	int                   fd,
	off_t                 offs,
	size_t                count
	);

/**
 * Receive message from peer.
 *
 * The message payload is stored in `buf', unless the payload size is larger
 * than `buf_size' bytes, in which case nothing is written to the buffer and
 * the function returns ENOSPC. Both on success and on ENOSPC the actual size
 * of the payload is stored in `payload_size'. In any other case the function
 * returns an appropriate error code and `payload_size' is set to zero.
 *
 * Notice that if `buf_size' is zero (and `buf' is NULL) the function simply
 * returns the size of the message payload through the `payload_size'
 * argument. This allows the caller to find out how much buffer space is
 * needed before retrieving a message.
 */
extern int
solo_recv(
	solo_conn_t         * conn,
	void                * buf,
	size_t                buf_size,
	size_t              * payload_size  // [out]
	);

/**
 * Receive message from peer.
 *
 * Unlike solo_recv(), this function expects a message of an exact size as
 * given by `payload_size'. If the received message does not match this size,
 * then EPROTO is returned.
 */
extern int
solo_recvn(
	solo_conn_t         * conn,
	void                * payload,
	size_t                payload_size
	);

/**
 * Receive message from peer, allocate receiving buffer.
 *
 * @param conn
 *	Connection created with solo_accept() or solo_connect().
 *
 * @param [out] payload
 *	The received message payload, as a buffer allocated with malloc().
 *
 * @param [out] payload_size
 *	The received message payload size.
 *
 * @return
 *	Zero on success, otherwise a system error code.
 */
extern int solo_recva(
	solo_conn_t  * conn,
	void        ** payload,      // [out]
	size_t       * payload_size  // [out]
	);

/* ------------------------------------------------------------------------- */
/*                          Accessory Functions                              */
/* ------------------------------------------------------------------------- */

/**
 * Get log stream associated with a SoloCloud instance.
 */
extern log_t *
solo_get_log(
	solo_t const * solo
	);

/**
 * Get log stream associated with a SoloCloud connection.
 */
extern log_t *
solo_conn_get_log(
	solo_conn_t const * conn
	);

#endif
