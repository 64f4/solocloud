/**
 * list.h
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_LIST_H
#define SOLOCLOUD_LIST_H

#include <unistd.h>

#include "stddef.h"

typedef struct list_struct list_t;

struct list_struct
{
	struct list_struct * li_next;
	struct list_struct * li_prev;
};

static inline void
__list_add(
	struct list_struct * elm,
	struct list_struct * prev,
	struct list_struct * next
	)
{
	next->li_prev = elm;
	elm->li_next  = next;
	elm->li_prev  = prev;
	prev->li_next = elm;
}

static inline void
__list_del(
	struct list_struct * prev,
	struct list_struct * next
	)
{
	next->li_prev = prev;
	prev->li_next = next;
}

static inline void
__list_splice(
	struct list_struct const * list,
	struct list_struct       * prev,
	struct list_struct       * next
	)
{
	struct list_struct * first = list->li_next;
	struct list_struct * last  = list->li_prev;
	first->li_prev = prev;
	prev->li_next  = first;
	last->li_next  = next;
	next->li_prev  = last;
}

/**
 * Initialize list.
 */
static inline void list_init(struct list_struct * li)
{
	li->li_next = li;
	li->li_prev = li;
}

/**
 * Uninitialize list
 */
static inline void list_uninit(struct list_struct * li)
{
	li->li_next = NULL;
	li->li_prev = NULL;
}

/**
 * Add entry to a list after the given element.
 */
static inline void list_add(struct list_struct * elm,
			    struct list_struct * where)
{
	__list_add(elm, where, where->li_next);
}

/**
 * Add entry to a list before the given element.
 */
static inline void list_add_tail(
	struct list_struct * elm,
	struct list_struct * where
	)
{
	__list_add(elm, where->li_prev, where);
}

/**
 * Delete element from a list.
 */
static inline void list_del(struct list_struct * elm)
{
	__list_del(elm->li_prev, elm->li_next);
	elm->li_next = NULL;
	elm->li_prev = NULL;
}

/**
 * Move entry after the given element, possibly on a different list.
 */
static inline void list_move(struct list_struct * elm,
			     struct list_struct * where)
{
	__list_del(elm->li_prev, elm->li_next);
	list_add(elm, where);
}

/**
 * Move entry before the given element, possibly on a different list.
 */
static inline void list_move_tail(
	struct list_struct * elm,
	struct list_struct * where
	)
{
	__list_del(elm->li_prev, elm->li_next);
	list_add_tail(elm, where);
}

/**
 * Tests whether a list is empty.
 */
static inline int list_empty(const struct list_struct * li)
{
	return li->li_next == li;
}

/**
 * Test where list is ununitialized.
 */
static inline int list_nil(const struct list_struct * li)
{
	return li->li_next == NULL && li->li_prev == NULL;
}

/**
 * Join two lists and reinitialize the emptied list.
 *
 * `src'
 *	Source list, which will be reinitialized.
 * `dst'
 *	Destination list.
 */
static inline void list_splice_init(
	struct list_struct * src,
	struct list_struct * dst
	)
{
	if (!list_empty(src))
	{
		__list_splice(src, dst, dst->li_next);
		list_init(src);
	}
}

/**
 * Get the given element's enclosing structure.
 */
#define list_entry(elm, type, member) \
	GCC_CONTAINER_OF(elm, type, member)

/**
 * Get the first element's enclosing structure.
 */
#define list_first_entry(li, type, member) \
	list_entry((li)->li_next, type, member)

/**
 * Iterate over a list.
 */
#define list_for_each(pos, li) \
	for (pos = (li)->li_next; GCC_PREFETCH(pos->li_next), pos != (li); \
	     pos = pos->li_next)

/**
 * Iterate over a list, backwards.
 */
#define list_for_each_prev(pos, li) \
	for (pos = (li)->li_prev; GCC_PREFETCH(pos->li_prev), pos != (li); \
	     pos = pos->li_prev)

/**
 * Iterate over a list in a way that is safe against the removal of the
 * current entry.
 */
#define list_for_each_safe(pos, next, li) \
	for (pos = (li)->li_next, next = pos->li_next; pos != (li); \
	     pos = next, next = pos->li_next)

/**
 * Iterate over a list backwards in a way that is safe against the removal
 * of the current entry.
 */
#define list_for_each_prev_safe(pos, next, li) \
	for (pos = (li)->li_prev, next = pos->li_prev; \
	     GCC_PREFETCH(pos->li_prev), pos != (li); \
	     pos = next, next = pos->li_prev)

/**
 * Iterate over a list and obtain each element's enclosing structure.
 */
#define list_for_each_entry(pos, li, type, member) \
	for (pos = list_entry((li)->li_next, type, member); \
	     GCC_PREFETCH(pos->member.li_next), &pos->member != (li); \
	     pos = list_entry(pos->member.li_next, type, member))

/**
 * Iterate over a list in reverse order and obtain each element's
 * enclosing structure.
 */
#define list_for_each_entry_reverse(pos, li, type, member) \
	for (pos = list_entry((li)->li_prev, type, member); \
	     GCC_PREFETCH(pos->member.li_prev), &pos->member != (li); \
	     pos = list_entry(pos->member.li_prev, type, member))

/**
 * Iterate over a list and obtain each element's enclosing structure. It
 * is safe the delete the current element while iterating.
 */
#define list_for_each_entry_safe(pos, next, li, type, member) \
	for (pos = list_entry((li)->li_next, type, member), \
	     	next = list_entry(pos->member.li_next, type, member); \
	     &pos->member != (li); \
	     pos = next, next = list_entry(next->member.li_next, type, member))

#endif
