/**
 * tcp.h
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_LIB_TCP_H
#define SOLOCLOUD_LIB_TCP_H

#include <stdint.h>
#include <sys/uio.h>  // (struct iovec)

#include "log.h"

/** Listening handle */
typedef struct tcp_server tcp_server_t;

/** Connection handle */
typedef struct tcp_conn tcp_conn_t;

/**
 * Open listening TCP/IP socket.
 *
 * The server decriptor needs to be deallocated tcp_release().
 */
extern int
tcp_listen(
	log_t               * log,
	short unsigned        portno,
	int                   backlog,
	tcp_server_t       ** newsrv
	);

/**
 * Release listening TCP/IP socket.
 *
 * No further operation is possible on the socket once the socket has been
 * released.
 */
extern void
tcp_release(
	tcp_server_t        * srv
	);

/**
 * Shutdown listening TCP/IP socket.
 *
 * This has the effect of interrupting tcp_accept() with an error. No further
 * connections can be accepted after tcp_shutdown(). It is safe to invoke this
 * function from within a signal handling thread.
 *
 * It is still necessary to call tcp_release() on the server descriptor.
 */
extern void
tcp_shutdown(
	tcp_server_t        * srv
	);

/**
 * Accept a connection from a client.
 */
extern int
tcp_accept(
	tcp_server_t        * srv,
	tcp_conn_t         ** newconn
	);

/**
 * Connect to server.
 */
extern int
tcp_connect(
	log_t               * log,
	char const          * hostname,
	int unsigned          portno,
	tcp_conn_t         ** newconn
	);

/**
 * Drop client connection.
 *
 * Causes all I/O on the given connection to fail. This function may be called
 * from within a signal handling thread.
 *
 * Notice that the client connection still needs to be closed with
 * tcp_close().
 */
extern void
tcp_drop(
	tcp_conn_t          * conn
	);

/**
 * Close a connection.
 */
extern void
tcp_close(
	tcp_conn_t          * conn
	);

/**
 * Send message to peer.
 */
extern int
tcp_send(
	tcp_conn_t          * conn,
	void const          * payload,
	size_t                payload_size
	);

/**
 * Send message to peer, combining the given I/O buffers.
 */
extern int
tcp_sendv(
	tcp_conn_t          * conn,
	struct iovec const  * iov,
	int unsigned          iocnt
	);

/**
 * Send message to peer, combining an optional header with the contents of a
 * file.
 *
 * @param conn
 *	Established connection.
 *
 * @param iov
 *	An optional header to be transmitted before the file contents. May be
 *	NULL if none.
 *
 * @param iocnt
 *	Number of entries in `iov'.
 *
 * @param fd
 *	File descriptor to read from.
 *
 * @param offs
 *	File descriptor read offset.
 *
 * @param count
 *	Number of bytes to read from file descriptor. Notice that the function
 *	will fail with an EIO error if it cannot read this exact number of
 *	bytes from `fd'. This is due to the fact that messages sent through
 *	this function must have a predetermined size marker.
 *
 * @returns
 *	A system error code, or zero on success.
 */
extern int
tcp_sendfile(
	tcp_conn_t          * conn,
	struct iovec const  * iov,
	size_t                iocnt,
	int                   fd,
	off_t                 offs,
	size_t                count
	);

/**
 * Receive message from peer.
 *
 * The message payload is stored in `buf', unless the payload size is larger
 * than `buf_size' bytes, in which case nothing is written to the buffer and
 * the function returns ENOSPC. Both on success and on ENOSPC the actual size
 * of the payload is stored in `payload_size'. In any other case the function
 * returns an appropriate error code and `payload_size' is set to zero.
 *
 * Notice that if `buf_size' is zero (and `buf' is NULL) the function simply
 * returns the size of the message payload through the `payload_size'
 * argument. This allows the caller to find out how much buffer space is
 * needed before retrieving a message.
 */
extern int
tcp_recv(
	tcp_conn_t          * conn,
	void                * buf,
	size_t                buf_size,
	size_t              * payload_size  // [out]
	);

/**
 * Receive message from peer.
 *
 * Unlike tcp_recv(), this function expects a message of an exact size as
 * given by `payload_size'. If the received message does not match this size,
 * then EPROTO is returned.
 */
extern int
tcp_recvn(
	tcp_conn_t          * conn,
	void                * payload,
	size_t                payload_size
	);

/**
 * Receive message from peer. Allocate receiving buffer.
 */
extern int
tcp_recva(
	tcp_conn_t          * conn,
	void               ** payload,      // [out]
	size_t              * payload_size  // [out]
	);

/* ========================================================================= */
/*                            Tuning and Control                             */
/* ========================================================================= */

/**
 * Enable keepalive probes on a TCP connection.
 *
 * @param conn
 *	TCP connection, obtained via tcp_accept() or tcp_connect().
 *
 * @param start
 *	The time (in seconds) the connection needs to remain idle before TCP
 *	starts sending keepalive probes.
 *
 * @param interval
 *	The time (in seconds) between individual keepalive probes.
 *
 * @param count
 *	The maximum number of keepalive probes TCP should send before dropping
 *	the connection.
 */
extern void
tcp_keepalive(
	tcp_conn_t          * conn,
	unsigned              start,
	unsigned              interval,
	unsigned              count
	);

/* ========================================================================= */
/*                           Accessory Functions                             */
/* ========================================================================= */

/**
 * Retrieve the file decriptor associated with a connection. Useful for
 * poll(), epoll(), etc.
 */
extern int
tcp_fileno(
	tcp_conn_t          * conn
	);

/**
 * Retrieve the file descriptor associated with a listening socket. Useful for
 * poll(), epoll(), etc.
 */
extern int
tcp_fileno_server(
	tcp_server_t        * srv
	);

/**
 * Return the IP address and TCP port number of the given server socket.
 */
int
tcp_serverinfo(
	tcp_server_t       * const srv,
	uint32_t           * const addr,
	uint16_t           * const port );

#endif
