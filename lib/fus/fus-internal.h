/**
 * fus-internal.h
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_LIB_FUS_INTERNAL_H
#define SOLOCLOUD_LIB_FUS_INTERNAL_H

/* System headers */
#include <stdbool.h>
#include <stdlib.h>

/* Our headers */
#include "fus-proto.h"
#include "fus.h"

#endif
