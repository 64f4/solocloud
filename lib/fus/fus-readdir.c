/**
 * fus-readdir.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* System headers */
#include <ctype.h>

/* Private header */
#include "fus-internal.h"

int
fus_readdir(
	solo_conn_t  * const conn,
	char const   * const path,
	void         * const arg,
	fus_filler_f   const filler
	)
{
	log_t        * const log = solo_conn_get_log(conn);
	int            err;

	/* check arguments */
	if (path == NULL || filler == NULL) {
		log_unexpected(log);
		return EFAULT;
	}
		
	/* are we scanning the root path? */
	bool const isroot = !strcmp(path, "/");

	/* Submit READDIR request.
	 */
	size_t const lenz = strlen(path) + 1;
	struct fus_m_readdir const req = {
		.fus_q_readdir_magic    = FUS_MAGIC_READDIR,
		.fus_q_readdir_pathlenz = lenz,
	};
	struct iovec const iov[] = {
		{ (void *) &req, sizeof(req) },
		{ (void *) path, lenz },
	};
	if ((err = solo_sendv(conn, iov, GCC_DIM(iov)))) {
		log_perror(log, LOG_TRACE, "solo_sendv()", err);
		goto fail_io;
	}

	/* Fetch READDIR response.
	 */
	void * rspbuf;
	size_t rspsize;
	if ((err = solo_recva(conn, &rspbuf, &rspsize))) {
		log_perror(log, LOG_TRACE, "solo_recva()", err);
		goto fail_io;
	}
	if (rspsize < sizeof(struct fus_m_readdir_r)) {
		log_unexpected(log);
		err = EPROTO;
		goto fail;
	}
	struct fus_m_readdir_r const * const rsp = rspbuf;
	if (rsp->fus_r_readdir_magic != FUS_MAGIC_READDIR) {
		log_unexpected(log);
		err = EPROTO;
		goto fail;
	}

	/* any server-side errors? */
	if ((err = fus_errno_fus2os(rsp->fus_r_readdir_err)))
		goto fail;

	/* Scan directory entries in the reply. Each entry ends with a single
	 * nil character, followed by a corresponding STAT response structure.
	 */
	unsigned numentries = 0;
	char const * s = rsp->fus_r_readdir_entries;
	char const * const e = (char const *) rsp + rspsize;
	while (s < e)
	{
		char const * p;
		char buf[512];  // XXX Too small? Use malloc().

		/* find the terminating nil char of this entry */
		for (p = s; p < e && *p; p++)
			continue;
		if (p == e) {
			log_unexpected(log);
			err = EPROTO;
			goto fail;
		}

		/* name must be followed by stat() values */
		if ((size_t) (e - (++p)) < sizeof(struct fus_m_stat_r)) {
			log_unexpected(log);
			err = EPROTO;
			goto fail;
		}

		/* Read STAT response following the entry name (notice that we
		 * have to duplicate the structure from the response buffer
		 * because it may be unaligned).
		 */
		struct fus_m_stat_r const strsp = *((struct fus_m_stat_r *) p);
		if (strsp.fus_r_stat_magic != FUS_MAGIC_STAT) {
			log_unexpected(log);
			err = EPROTO;
			goto fail;
		}

		/* convert STAT response to a stat() structure */
		struct stat const stbuf = FUS_MAKE_STBUF_FROM_STRSP(&strsp);

		/* Determine full entry path.
		 */
		ssize_t const n =
			isroot ? snprintf(buf, sizeof(buf), "/%s", s)
			       : snprintf(buf, sizeof(buf), "%s/%s", path, s);
		if (n < 0 || (size_t) n >= sizeof(buf)) {
			log_unexpected(log);
			err = ENAMETOOLONG;
			goto fail;
		}

		/* Invoke callback with gathered entry information.
		 */
		if ((err = filler(arg, s, buf, &stbuf))) {
			log_perror(log, LOG_ERR, "filler()", err);
			goto fail;
		}

		/* next entry */
		s = p + sizeof(struct fus_m_stat_r);
		log_assert(log, s <= e);
		numentries++;
	}

	/* verify number of entries */
	if (rsp->fus_r_readdir_numentries != numentries) {
		log_unexpected(log);
		err = EPROTO;
		goto fail;
	}

	free(rspbuf);
	return 0;

fail:
	free(rspbuf);
fail_io:
	log_assert(log, err);
	return err;
}
