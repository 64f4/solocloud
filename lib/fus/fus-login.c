/**
 * fus-login.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Project headers */
#include "spooky.h"

/* Private header */
#include "fus-internal.h"

int
fus_login(
	solo_conn_t  * const conn,
	char const   * const pass
	)
{
	log_t        * const log  = solo_conn_get_log(conn);
	uint64_t       h[2] = { 0, 0 };
	int            err;

	/* verify arguments */
	if (!conn) {
		log_unexpected(log);
		return EFAULT;
	}

	/* remove terminating newline char */
	size_t len = strlen(pass);
	while (len && (   pass[len - 1] == '\n'
		       || pass[len - 1] == '\r'))
		len--;

	/* hash password */
	spooky_hash128(pass, len, &h[0], &h[1]);

	/* Submit LOGIN request.
	 */
	struct fus_m_login const req = {
		.fus_q_login_magic = FUS_MAGIC_LOGIN,
		.fus_q_login_h     = { h[0], h[1] },
	};
	if ((err = solo_send(conn, &req, sizeof(req)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		return err;
	}

	/* Fetch LOGIN response.
	 */
	struct fus_m_login_r rsp;
	if ((err = solo_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_recvn()", err);
		return err;
	}
	if (rsp.fus_r_login_magic != FUS_MAGIC_LOGIN) {
		log_unexpected(log);
		return EPROTO;
	}

	return fus_errno_fus2os(rsp.fus_r_login_err);
}
