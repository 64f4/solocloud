/**
 * fus-pwrite.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fus-internal.h"

int
fus_pwrite(
	solo_conn_t  * const conn,
	fus_fd_t       const fd,
	char const   * const buf,
	size_t       * const count,   // [in/out]
	off_t          const offset
	)
{
	log_t        * const log = solo_conn_get_log(conn);
	int            err;

	/* verify arguments */
	if (fd < 0 || offset < 0) {
		log_unexpected(log);
		return EINVAL;
	}
	if (!conn || !buf || !count) {
		log_unexpected(log);
		return EFAULT;
	}

	/* Submit WRITE request.
	 */
	struct fus_m_write const req = {
		.fus_q_write_magic  = FUS_MAGIC_WRITE,
		.fus_q_write_count  = *count,
		.fus_q_write_offset = offset,
		.fus_q_write_fd     = fd,
	};
	struct iovec const iov[] = {
		{ (void *) &req, sizeof(req) },
		{ (void *) buf, *count },
	};
	if ((err = solo_sendv(conn, iov, GCC_DIM(iov)))) {
		log_perror(log, LOG_TRACE, "solo_sendv()", err);
		return err;
	}

	/* Fetch WRITE response.
	 */
	struct fus_m_write_r rsp;
	if ((err = solo_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_recvn()", err);
		return err;
	}
	if (rsp.fus_r_write_magic != FUS_MAGIC_WRITE) {
		log_unexpected(log);
		return EPROTO;
	}

	/* check for server-side errors */
	if ((err = fus_errno_fus2os(rsp.fus_r_write_err)))
		return err;

	/* report number of bytes written */
	*count = rsp.fus_r_write_count;
	return 0;
}
