/**
 * fus-rmdir.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fus-internal.h"

int
fus_rmdir(
	solo_conn_t  * const conn,
	char const   * const path
	)
{
	log_t        * const log = solo_conn_get_log(conn);
	int            err;

	/* verify arguments */
	if (conn == NULL || path == NULL) {
		log_unexpected(log);
		return EFAULT;
	}

	/* Submit RMDIR request.
	 */
	size_t const lenz = strlen(path) + 1;
	struct fus_m_rmdir const req = {
		.fus_q_rmdir_magic    = FUS_MAGIC_RMDIR,
		.fus_q_rmdir_pathlenz = lenz,
	};
	struct iovec const iov[] = {
		{ (void *) &req, sizeof(req) },
		{ (void *) path, lenz },
	};
	if ((err = solo_sendv(conn, iov, GCC_DIM(iov)))) {
		log_perror(log, LOG_TRACE, "solo_sendv()", err);
		return err;
	}

	/* Fetch RMDIR response.
	 */
	struct fus_m_rmdir_r rsp;
	if ((err = solo_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_recvn()", err);
		return err;
	}
	if (rsp.fus_r_rmdir_magic != FUS_MAGIC_RMDIR) {
		log_unexpected(log);
		return EPROTO;
	}

	/* error code */
	return fus_errno_fus2os(rsp.fus_r_rmdir_err);
}
