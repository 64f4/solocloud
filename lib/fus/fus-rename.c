/**
 * fus-rename.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fus-internal.h"

int
fus_rename(
	solo_conn_t * const conn,
	char const  * const src,
	char const  * const dst
	)
{
	log_t       * const log = solo_conn_get_log(conn);
	int           err;

	/* check arguments */
	if (src == NULL || dst == NULL) {
		log_unexpected(log);
		return EFAULT;
	}

	/* Submit RENAME request.
	 */
	size_t const srclenz = strlen(src) + 1;
	size_t const dstlenz = strlen(dst) + 1;
	struct fus_m_rename const req = {
		.fus_q_rename_magic   = FUS_MAGIC_RENAME,
		.fus_q_rename_srclenz = srclenz,
		.fus_q_rename_dstlenz = dstlenz,
	};
	struct iovec const iov[] = {
		{ (void *) &req, sizeof(req) },
		{ (void *) src, srclenz },
		{ (void *) dst, dstlenz },
	};
	if ((err = solo_sendv(conn, iov, GCC_DIM(iov)))) {
		log_perror(log, LOG_TRACE, "solo_sendv()", err);
		return err;
	}

	/* Fetch RENAME response.
	 */
	struct fus_m_rename_r rsp;
	if ((err = solo_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_recvn()", err);
		return err;
	}
	if (rsp.fus_r_rename_magic != FUS_MAGIC_RENAME) {
		log_unexpected(log);
		return EPROTO;
	}

	/* error code */
	return fus_errno_fus2os(rsp.fus_r_rename_err);
}
