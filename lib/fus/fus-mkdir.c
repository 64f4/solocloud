/**
 * fus-mkdir.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fus-internal.h"

extern int
fus_mkdir(
	solo_conn_t  * const conn,
	char const   * const path,
	mode_t         const GCC_ATTRIBUTE_UNUSED(mode)
	)
{
	log_t        * const log = solo_conn_get_log(conn);
	int            err;

	/* check arguments */
	if (path == NULL) {
		log_unexpected(log);
		return EFAULT;
	}

	/* Submit MKDIR request.
	 */
	size_t const lenz = strlen(path) + 1;
	struct fus_m_mkdir const req = {
		.fus_q_mkdir_magic    = FUS_MAGIC_MKDIR,
		.fus_q_mkdir_pathlenz = lenz,
	};
	struct iovec const iov[] = {
		{ (void *) &req, sizeof(req) },
		{ (void *) path, lenz },
	};
	if ((err = solo_sendv(conn, iov, GCC_DIM(iov)))) {
		log_perror(log, LOG_TRACE, "solo_sendv()", err);
		return err;
	}

	/* Fetch MKDIR response.
	 */
	struct fus_m_mkdir_r rsp;
	if ((err = solo_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_recvn()", err);
		return err;
	}
	if (rsp.fus_r_mkdir_magic != FUS_MAGIC_MKDIR) {
		log_unexpected(log);
		return EPROTO;
	}

	/* report error code */
	return fus_errno_fus2os(rsp.fus_r_mkdir_err);
}
