/**
 * fus-unlink.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fus-internal.h"

int
fus_unlink(
	solo_conn_t  * const conn,
	char const   * const path
	)
{
	log_t        * const log = solo_conn_get_log(conn);
	int            err;

	/* verify arguments */
	if (conn == NULL || path == NULL) {
		log_unexpected(log);
		return EFAULT;
	}

	/* Submit UNLINK request.
	 */
	size_t const lenz = strlen(path) + 1;
	struct fus_m_unlink const req = {
		.fus_q_unlink_magic    = FUS_MAGIC_UNLINK,
		.fus_q_unlink_pathlenz = lenz,
	};
	struct iovec const iov[] = {
		{ (void *) &req, sizeof(req) },
		{ (void *) path, lenz },
	};
	if ((err = solo_sendv(conn, iov, GCC_DIM(iov)))) {
		log_perror(log, LOG_TRACE, "solo_sendv()", err);
		return err;
	}

	/* Fetch UNLINK response.
	 */
	struct fus_m_unlink_r rsp;
	if ((err = solo_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_recvn()", err);
		return err;
	}
	if (rsp.fus_r_unlink_magic != FUS_MAGIC_UNLINK) {
		log_unexpected(log);
		return EPROTO;
	}

	return fus_errno_fus2os(rsp.fus_r_unlink_err);
}
