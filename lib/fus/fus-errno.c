/**
 * fus-errno.c
 *
 *	SoloCloud FUSE protocol error codes.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <errno.h>

#include "gccdef.h"

#include "fus-errno.h"

/* Maps a system error code to a FUSED error code */
static enum fus_errno const os2fus[] = {
	[0           ] = FUS_OK,
        [EPERM       ] = FUS_EPERM,
        [EBADF       ] = FUS_EBADF,
        [EBUSY       ] = FUS_EBUSY,
        [EEXIST      ] = FUS_EEXIST,
        [EFBIG       ] = FUS_EFBIG,
        [EINVAL      ] = FUS_EINVAL,
        [EIO         ] = FUS_EIO,
        [EISDIR      ] = FUS_EISDIR,
        [EMFILE      ] = FUS_EMFILE,
        [ENAMETOOLONG] = FUS_ENAMETOOLONG,
        [ENFILE      ] = FUS_ENFILE,
        [ENODATA     ] = FUS_ENODATA,
        [ENODEV      ] = FUS_ENODEV,
        [ENOENT      ] = FUS_ENOENT,
        [ENOMEM      ] = FUS_ENOMEM,
        [ENOSPC      ] = FUS_ENOSPC,
        [ENOSYS      ] = FUS_ENOSYS,
        [ENOTDIR     ] = FUS_ENOTDIR,
        [ENOTEMPTY   ] = FUS_ENOTEMPTY,
        [EOVERFLOW   ] = FUS_EOVERFLOW,
        [EPROTO      ] = FUS_EPROTO,
        [EROFS       ] = FUS_EROFS,
        [ESHUTDOWN   ] = FUS_ESHUTDOWN,
        [ESPIPE      ] = FUS_ESPIPE,
        [ETIMEDOUT   ] = FUS_ETIMEDOUT,
};

/* Maps a FUSED error code to a system error code */
static int const fus2os[] = {
	[FUS_OK            ] = 0,
        [FUS_EPERM       ] = EPERM,
        [FUS_EBADF       ] = EBADF,
        [FUS_EBUSY       ] = EBUSY,
        [FUS_EEXIST      ] = EEXIST,
        [FUS_EFBIG       ] = EFBIG,
        [FUS_EINVAL      ] = EINVAL,
        [FUS_EIO         ] = EIO,
        [FUS_EISDIR      ] = EISDIR,
        [FUS_EMFILE      ] = EMFILE,
        [FUS_ENAMETOOLONG] = ENAMETOOLONG,
        [FUS_ENFILE      ] = ENFILE,
        [FUS_ENODATA     ] = ENODATA,
        [FUS_ENODEV      ] = ENODEV,
        [FUS_ENOENT      ] = ENOENT,
        [FUS_ENOMEM      ] = ENOMEM,
        [FUS_ENOSPC      ] = ENOSPC,
        [FUS_ENOSYS      ] = ENOSYS,
        [FUS_ENOTDIR     ] = ENOTDIR,
        [FUS_ENOTEMPTY   ] = ENOTEMPTY,
        [FUS_EOVERFLOW   ] = EOVERFLOW,
        [FUS_EPROTO      ] = EPROTO,
        [FUS_EROFS       ] = EROFS,
        [FUS_ESHUTDOWN   ] = ESHUTDOWN,
        [FUS_ESPIPE      ] = ESPIPE,
        [FUS_ETIMEDOUT   ] = ETIMEDOUT,
};

enum fus_errno
fus_errno_os2fus(int const err)
{
	if (err == 0)
		return 0;
	if (err < 0 || (unsigned) err >= GCC_DIM(os2fus))
		return EPERM;
	
	return os2fus[err] ? : FUS_EPERM;
}

int
fus_errno_fus2os(enum fus_errno const err)
{
	if (err == 0)
		return 0;
	if (err >= GCC_DIM(fus2os))
		return EPERM;
	
	return fus2os[err] ? : EPERM;
}
