/**
 * fus-statfs.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fus-internal.h"

int
fus_statvfs(
	solo_conn_t    * const conn,
	char const     * const path,
	struct statvfs * const stvfs )
{
	log_t          * const log  = solo_conn_get_log(conn);
	int              err;

	/* check arguments */
	if (!conn || !path || !stvfs) {
		log_unexpected(log);
		return EFAULT;
	}

	/* Submit STATFS request.
	 */
	size_t const lenz = strlen(path) + 1;
	struct fus_m_statfs const req = {
		.fus_q_statfs_magic    = FUS_MAGIC_STATFS,
		.fus_q_statfs_pathlenz = lenz,
	};
	struct iovec const iov[] = {
		{ (void *) &req, sizeof(req), },
		{ (void *) path, lenz, },
	};
	if ((err = solo_sendv(conn, iov, GCC_DIM(iov)))) {
		log_perror(log, LOG_TRACE, "solo_sendv()", err);
		return err;
	}

	/* Fetch STATFS response.
	 */
	struct fus_m_statfs_r rsp;
	if ((err = solo_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_recvn()", err);
		return err;
	}
	if (rsp.fus_r_statfs_magic != FUS_MAGIC_STATFS) {
		log_unexpected(log);
		return EPROTO;
	}

	/* return error back to caller */
	if ((err = fus_errno_fus2os(rsp.fus_r_statfs_err)))
		return err;

	/* return statvfs() information back to caller */
	*stvfs = (struct statvfs) {
        	.f_namemax = rsp.fus_r_statfs_namemax, 
        	.f_bsize   = rsp.fus_r_statfs_bsize,   
        	.f_blocks  = rsp.fus_r_statfs_blocks,  
        	.f_bfree   = rsp.fus_r_statfs_bfree,   
        	.f_bavail  = rsp.fus_r_statfs_bavail,  
		.f_files   = rsp.fus_r_statfs_files,   
        	.f_ffree   = rsp.fus_r_statfs_ffree,   
	};

	return 0;
}
