/**
 * fus-ftruncate.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fus-internal.h"

int
fus_open(
	solo_conn_t  * const conn,
	char const   * const path,
	int            const flags,
	mode_t         const GCC_ATTRIBUTE_UNUSED(omode),
	fus_fd_t     * const fd
	)
{
	log_t        * const log = solo_conn_get_log(conn);
	int            err;

	/* check arguments */
	if (path == NULL || fd == NULL) {
		log_unexpected(log);
		return EFAULT;
	}

	/* Submit OPEN request.
	 */
	size_t const lenz = strlen(path) + 1;
	struct fus_m_open const req = {
		.fus_q_open_magic    = FUS_MAGIC_OPEN,
		.fus_q_open_flags    = fus_OPEN_flags(flags),
		.fus_q_open_pathlenz = lenz,
	};
	struct iovec const iov[] = {
		{ (void *) &req, sizeof(req) },
		{ (void *) path, lenz },
	};
	if ((err = solo_sendv(conn, iov, GCC_DIM(iov)))) {
		log_perror(log, LOG_TRACE, "solo_sendv()", err);
		return err;
	}

	/* Fetch OPEN response.
	 */
	struct fus_m_open_r rsp;
	if ((err = solo_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_recvn()", err);
		return err;
	}
	if (rsp.fus_r_open_magic != FUS_MAGIC_OPEN) {
		log_unexpected(log);
		return EPROTO;
	}

	/* report error and file descriptor */
	*fd = rsp.fus_r_open_fd;
	return fus_errno_fus2os(rsp.fus_r_open_err);
}
