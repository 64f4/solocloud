/**
 * fus-close.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fus-internal.h"

int
fus_close(
	solo_conn_t  * const conn,
	fus_fd_t       const fd
	)
{
	log_t        * const log = solo_conn_get_log(conn);
	int            err;

	/* verify arguments */
	if (fd < 0) {
		log_unexpected(log);
		return EINVAL;
	}
	if (!conn) {
		log_unexpected(log);
		return EFAULT;
	}

	/* Submit CLOSE request.
	 */
	struct fus_m_close const req = {
		.fus_q_close_magic = FUS_MAGIC_CLOSE,
		.fus_q_close_fd    = fd,
	};
	if ((err = solo_send(conn, &req, sizeof(req)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		return err;
	}

	/* Fetch CLOSE response.
	 */
	struct fus_m_close_r rsp;
	if ((err = solo_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_recvn()", err);
		return err;
	}
	if (rsp.fus_r_close_magic != FUS_MAGIC_CLOSE) {
		log_unexpected(log);
		return EPROTO;
	}

	return fus_errno_fus2os(rsp.fus_r_close_err);
}
