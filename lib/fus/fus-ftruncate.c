/**
 * fus-ftruncate.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fus-internal.h"

int
fus_ftruncate(
	solo_conn_t  * const conn,
	fus_fd_t       const fd,
	off_t          const offs
	)
{
	log_t        * const log = solo_conn_get_log(conn);
	int            err;

	if (fd < 0 || offs < 0) {
		log_unexpected(log);
		return EINVAL;
	}

	/* Submit TRUNC request.
	 */
	struct fus_m_trunc const req = {
		.fus_q_trunc_magic = FUS_MAGIC_TRUNC,
		.fus_q_trunc_fd    = fd,
		.fus_q_trunc_offs  = offs,
	};
	if ((err = solo_send(conn, &req, sizeof(req)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		return err;
	}

	/* Fetch TRUNC response.
	 */
	struct fus_m_trunc_r rsp;
	if ((err = solo_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_recvn()", err);
		return err;
	}
	if (rsp.fus_r_trunc_magic != FUS_MAGIC_TRUNC) {
		log_unexpected(log);
		return EPROTO;
	}

	/* report error code */
	return fus_errno_fus2os(rsp.fus_r_trunc_err);
}
