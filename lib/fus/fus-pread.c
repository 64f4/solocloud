/**
 * fus-pread.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fus-internal.h"

int
fus_pread(
	solo_conn_t  * const conn,
	fus_fd_t       const fd,
	void         * const buf,
	size_t       * const count,
	off_t          const offset
	)
{
	log_t        * const log = solo_conn_get_log(conn);
	int            err;

	/* verify arguments */
	if (fd < 0 || offset < 0) {
		log_unexpected(log);
		return EINVAL;
	}
	if (!conn || !buf || !count) {
		log_unexpected(log);
		return EFAULT;
	}

	/* Submit READ request.
	 */
	size_t const bufsize = *count;
	struct fus_m_read const req = {
		.fus_q_read_magic  = FUS_MAGIC_READ,
		.fus_q_read_count  = bufsize,
		.fus_q_read_offset = offset,
		.fus_q_read_fd     = fd,
	};
	if ((err = solo_send(conn, &req, sizeof(req)))) {
		log_perror(log, LOG_TRACE, "solo_send()", err);
		return err;
	}

	// FIXME !
	//	This is criminally inefficient: we allocate the response
	//	buffer on the heap and then copy the data we received to the
	//	output buffer. We need to be able to do it in one step,
	//	without copying any data, but this will require new APIs in
	//	"solo.h" and "tcp.h".
	//

	/* Fetch READ response.
	 */
	void * rspbuf;
	size_t rspsize;
	if ((err = solo_recva(conn, &rspbuf, &rspsize))) {
		log_perror(log, LOG_TRACE, "solo_recva()", err);
		return err;
	}
	/* check minimum size of the response */
	if (rspsize < sizeof(struct fus_m_read_r)) {
		log_unexpected(log);
		free(rspbuf);
		return EPROTO;
	}
	/* check magic number */
	struct fus_m_read_r const * const rsp = rspbuf;
	if (rsp->fus_r_read_magic != FUS_MAGIC_READ) {
		log_unexpected(log);
		free(rspbuf);
		return EPROTO;
	}
	/* check expected size of the response */
	size_t const numread = rsp->fus_r_read_count;
	if (rspsize != sizeof(*rsp) + numread || numread > bufsize) {
		log_unexpected(log);
		free(rspbuf);
		return EPROTO;
	}

	/* check for server-side errors */
	if ((err = fus_errno_fus2os(rsp->fus_r_read_err))) {
		free(rspbuf);
		return err;
	}

	memcpy(buf, rsp->fus_r_read_buf, numread);
	free(rspbuf);
	*count = numread;
	return 0;
}
