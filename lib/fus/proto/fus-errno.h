/**
 * fus-errno.h
 *
 *	SoloCloud FUSE protocol error codes.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_LIB_FUS_ERRNO_H
#define SOLOCLOUD_LIB_FUS_ERRNO_H

enum fus_errno
{
	FUS_OK = 0,
        FUS_EPERM,          /* Operation not permitted */
        FUS_EBADF,          /* Bad file number */
        FUS_EBUSY,          /* Device or resource busy */
        FUS_EEXIST,         /* File exists */
        FUS_EFBIG,          /* File too large */
        FUS_EINVAL,         /* Invalid argument */
        FUS_EIO,            /* I/O error */
        FUS_EISDIR,         /* Is a directory */
        FUS_EMFILE,         /* Too many open files */
        FUS_ENAMETOOLONG,   /* File name too long */
        FUS_ENFILE,         /* File table overflow */
        FUS_ENODATA,        /* No data available */
        FUS_ENODEV,         /* No such device */
        FUS_ENOENT,         /* No such file or directory */
        FUS_ENOMEM,         /* Out of memory */
        FUS_ENOSPC,         /* No space left on device */
        FUS_ENOSYS,         /* Function not implemented */
        FUS_ENOTDIR,        /* Not a directory */
        FUS_ENOTEMPTY,      /* Directory not empty */
        FUS_EOVERFLOW,      /* Value too large for defined data type */
        FUS_EPROTO,         /* Protocol error */
        FUS_EROFS,          /* Read-only file system */
        FUS_ESHUTDOWN,      /* Shutdown in progress */
        FUS_ESPIPE,         /* Illegal seek */
        FUS_ETIMEDOUT,      /* Connection timed out */
};

/** Maps a system error code to a FUSED error code */
extern int fus_errno_fus2os(enum fus_errno);

/** Maps a FUSED error code to a system error code */
extern enum fus_errno fus_errno_os2fus(int);

#endif
