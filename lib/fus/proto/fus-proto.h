/**
 * fus-proto.h
 *
 *	SoloCloud FUSE protocol definition.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_LIB_FUS_PROTO_H
#define SOLOCLOUD_LIB_FUS_PROTO_H

/* System headers */
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>

/* Public headers */
#include "gccdef.h"

/* Error definitions */
#include "fus-errno.h"

/* Magic values.
 */
#define FUS_MAGIC_CLOSE   0x3e4bb797ul
#define FUS_MAGIC_LOGIN   0xf6db859bul
#define FUS_MAGIC_MKDIR   0xad2abaceul
#define FUS_MAGIC_OPEN    0x331dd57aul
#define FUS_MAGIC_READ    0x648dca34ul
#define FUS_MAGIC_READDIR 0x770b0723ul
#define FUS_MAGIC_RENAME  0x906249feul
#define FUS_MAGIC_RMDIR   0xbb35bffdul
#define FUS_MAGIC_STAT    0x250edaf1ul
#define FUS_MAGIC_STATFS  0x134b66d9ul
#define FUS_MAGIC_TRUNC   0xdfcf26c5ul
#define FUS_MAGIC_UNLINK  0xe565e058ul
#define FUS_MAGIC_WRITE   0xbd41f480ul

/* ************************************************************************* */
/*                                  CLOSE                                    */
/* ************************************************************************* */

/**
 * CLOSE request.
 */
struct fus_m_close
{
	uint32_t  fus_q_close_magic;        // FUS_MAGIC_CLOSE
	uint32_t  fus_q_close_fd;           // file descriptor

} GCC_PACKED;

/**
 * CLOSE response.
 */
struct fus_m_close_r
{
	uint32_t  fus_r_close_magic;        // FUS_MAGIC_CLOSE
	uint16_t  fus_r_close_err;          // system error code

} GCC_PACKED;

/* ************************************************************************* */
/*                                  LOGIN                                    */
/* ************************************************************************* */

/**
 * LOGIN request.
 */
struct fus_m_login
{
	uint32_t  fus_q_login_magic;        // FUS_MAGIC_LOGIN
	uint64_t  fus_q_login_h[2];         // password hash

} GCC_PACKED;

/**
 * LOGIN response.
 */
struct fus_m_login_r
{
	uint32_t  fus_r_login_magic;        // FUS_MAGIC_LOGIN
	uint16_t  fus_r_login_err;          // system error code

} GCC_PACKED;

/* ************************************************************************* */
/*                                   MKDIR                                    */
/* ************************************************************************* */

/**
 * MKDIR request.
 */
struct fus_m_mkdir
{
	uint32_t  fus_q_mkdir_magic;        // FUS_MAGIC_MKDIR
	uint16_t  fus_q_mkdir_pathlenz;     // path length plus nil char
	char      fus_q_mkdir_path[];       // path string

} GCC_PACKED;

/**
 * MKDIR response.
 */
struct fus_m_mkdir_r
{
	uint32_t  fus_r_mkdir_magic;        // FUS_MAGIC_MKDIR
	uint16_t  fus_r_mkdir_err;          // system error code

} GCC_PACKED;

/* ************************************************************************* */
/*                                   OPEN                                    */
/* ************************************************************************* */

/* OPEN flags */
#define FUS_O_READ     0x00000001u
#define FUS_O_WRITE    0x00000002u
#define FUS_O_APPEND   0x00000004u
#define FUS_O_CREAT    0x00000008u
#define FUS_O_EXCL     0x00000010u
#define FUS_O_TRUNC    0x00000020u
/* OPEN modes */
#define FUS_O_EXECUTE  0x00010000u       // file should be executable

/**
 * Convert open() system call flags to OPEN request flags.
 */
static inline uint32_t
fus_OPEN_flags(int const o)
{
	uint32_t f = 0;

	/* read/write permissions */
	if (o & O_RDONLY)
		f |= FUS_O_READ;
	else if (o & O_WRONLY)
		f |= FUS_O_WRITE;
	else if (o & O_RDWR)
		f |= FUS_O_READ | FUS_O_WRITE;
	else
		f |= FUS_O_READ;

	/* other flags */
	if (o & O_APPEND)
		f |= FUS_O_WRITE | FUS_O_APPEND;
	if (o & O_CREAT)
		f |= FUS_O_WRITE | FUS_O_CREAT;
	if (o & O_TRUNC)
		f |= FUS_O_WRITE | FUS_O_TRUNC;
	if (o & O_EXCL)
		f |= FUS_O_WRITE | FUS_O_CREAT | FUS_O_EXCL;

	return f;
}

/**
 * OPEN request.
 */
struct fus_m_open
{
	uint32_t  fus_q_open_magic;         // FUS_MAGIC_OPEN
	uint32_t  fus_q_open_flags;         // FUS_O_READ, etc.
	uint16_t  fus_q_open_pathlenz;      // path length plus nil char
	char      fus_q_open_path[];        // path string

} GCC_PACKED;

/**
 * OPEN response.
 */
struct fus_m_open_r
{
	uint32_t  fus_r_open_magic;         // FUS_MAGIC_OPEN
	uint32_t  fus_r_open_fd;            // file descriptor
	uint16_t  fus_r_open_err;           // system error code

} GCC_PACKED;

/* ************************************************************************* */
/*                                   READ                                    */
/* ************************************************************************* */

/**
 * READ request.
 */
struct fus_m_read
{
	uint32_t  fus_q_read_magic;         // FUS_MAGIC_READ
	uint32_t  fus_q_read_fd;            // file descriptor
	uint64_t  fus_q_read_offset;        // start offset
	uint32_t  fus_q_read_count;         // number of bytes to read

} GCC_PACKED;

/**
 * READ response.
 */
struct fus_m_read_r
{
	uint32_t  fus_r_read_magic;         // FUS_MAGIC_READ
	uint16_t  fus_r_read_err;           // system error code
	uint16_t  fus_r_read_;              // (reserved)
	uint32_t  fus_r_read_count;         // number of bytes read
	uint8_t   fus_r_read_buf[];         // data read

} GCC_PACKED;

/* ************************************************************************* */
/*                                 READDIR                                   */
/* ************************************************************************* */

/**
 * READDIR request.
 */
struct fus_m_readdir
{
	uint32_t  fus_q_readdir_magic;      // FUS_MAGIC_READDIR
	uint16_t  fus_q_readdir_pathlenz;   // path length plus nil char
	char      fus_q_readdir_path[];     // path string

} GCC_PACKED;

/**
 * READDIR response.
 *
 * This is a variable-length response. The size of the response depends on the
 * number of file entries listed under the directory path specified in the
 * READDIR request.
 *
 * The file listing starts at the address pointed by `fus_r_readdir_entries'.
 * Entries are listed sequentially, without any gaps. Each file entry consists
 * of a nil-terminated string (the file's base name) immediately followed by a
 * STAT response structure (see STAT response structure definition later in
 * this header). The STAT response structure is not aligned, so it may be
 * necessary to copy it to an aligned address before parsing it.
 *
 * By embedding STAT response structures in the output of READDIR we save a
 * lot of subsequent individual STAT requests. Client FUSE implementations
 * should cache these STAT structures returned by READDIR and make them
 * available to subsequent getattr() operations.
 */
struct fus_m_readdir_r
{
	uint32_t  fus_r_readdir_magic;      // FUS_MAGIC_READDIR
	uint16_t  fus_r_readdir_err;        // system error code
	uint16_t  fus_r_readdir_numentries; // number of entries
	char      fus_r_readdir_entries[];  // nil-terminated entries

} GCC_PACKED;

/* ************************************************************************* */
/*                                RENAME                                     */
/* ************************************************************************* */

/**
 * RENAME request.
 */
struct fus_m_rename
{
	uint32_t  fus_q_rename_magic;       // FUS_MAGIC_RENAME
	uint16_t  fus_q_rename_srclenz;     // path length plus nil char
	uint16_t  fus_q_rename_dstlenz;     // path length plus nil char
	char      fus_q_rename_buf[];       // source, destination paths
	                                    // (nil terminated)
} GCC_PACKED;

/**
 * RENAME response.
 */
struct fus_m_rename_r
{
	uint32_t  fus_r_rename_magic;       // FUS_MAGIC_RENAME
	uint16_t  fus_r_rename_err;         // system error code

} GCC_PACKED;

/* ************************************************************************* */
/*                                   RMDIR                                    */
/* ************************************************************************* */

/**
 * RMDIR request.
 */
struct fus_m_rmdir
{
	uint32_t  fus_q_rmdir_magic;        // FUS_MAGIC_RMDIR
	uint16_t  fus_q_rmdir_pathlenz;     // path length plus nil char
	char      fus_q_rmdir_path[];       // path string

} GCC_PACKED;

/**
 * RMDIR response.
 */
struct fus_m_rmdir_r
{
	uint32_t  fus_r_rmdir_magic;        // FUS_MAGIC_RMDIR
	uint16_t  fus_r_rmdir_err;          // system error code

} GCC_PACKED;

/* ************************************************************************* */
/*                                   STAT                                    */
/* ************************************************************************* */

/**
 * STAT request.
 */
struct fus_m_stat
{
	uint32_t  fus_q_stat_magic;         // FUS_MAGIC_STAT
	uint16_t  fus_q_stat_pathlenz;      // path length plus nil char
	char      fus_q_stat_path[];        // path string

} GCC_PACKED;

/**
 * STAT response.
 *
 * This structure is also part of a READDIR response (each entry returned by
 * READDIR is accompanied by its own STAT values, saving us a lot of
 * subsequent individual STAT transactions).
 *
 * Given its popularity, there are shortcuts to deal with this structure. See
 * the FUS_MAKE_STRSP_FROM_STBUF() and FUS_MAKE_STBUF_FROM_STRSP() macros
 * defined below.
 *
 * FIXME
 *	This response structure is too specific to Unix operating systems, and
 *	even so I'm not completely sure that the mode flags have the same
 *	values across the different Unix flavors. This structure should be
 *	operating system independent.
 */
struct fus_m_stat_r
{
	uint32_t  fus_r_stat_magic;         // FUS_MAGIC_STAT
	uint16_t  fus_r_stat_err;           // system error code
	uint16_t  fus_r_stat_nlink;         // number of hard links
	uint32_t  fus_r_stat_mode;          // file mode
	uint32_t  fus_r_stat_size;          // file size in bytes
	uint64_t  fus_r_stat_atime;         // time of last access
	uint64_t  fus_r_stat_mtime;         // time of last modification
	uint64_t  fus_r_stat_ctime;         // time of last status change

} GCC_PACKED;

/** Shortcut to make a `fus_m_stat_r' structure from a `stat' structure */
#define FUS_MAKE_STRSP_FROM_STBUF(err, stbuf) \
({ \
	struct stat const * const __stbuf = (stbuf); \
	typeof (err) const __err = (err); \
	__err ? (struct fus_m_stat_r) { \
			.fus_r_stat_magic = FUS_MAGIC_STAT, \
			.fus_r_stat_err   = fus_errno_os2fus(__err), \
		} \
	      : (struct fus_m_stat_r) { \
			.fus_r_stat_magic = FUS_MAGIC_STAT, \
			.fus_r_stat_nlink = __stbuf->st_nlink,   \
			.fus_r_stat_mode  = __stbuf->st_mode,    \
			.fus_r_stat_size  = __stbuf->st_size,    \
			.fus_r_stat_atime = __stbuf->st_atime,   \
			.fus_r_stat_mtime = __stbuf->st_mtime,   \
			.fus_r_stat_ctime = __stbuf->st_ctime,   \
		}; \
})

/** Shortcut to make a `stat' structure from a `fus_m_stat_r' structure */
#define FUS_MAKE_STBUF_FROM_STRSP(rsp) \
({ \
	struct fus_m_stat_r const * const __rsp = (rsp); \
	struct stat __stbuf; \
	memset(&__stbuf, 0, sizeof(__stbuf)); \
	if (!__rsp->fus_r_stat_err) { \
		__stbuf.st_mode  = __rsp->fus_r_stat_mode;  \
		__stbuf.st_nlink = __rsp->fus_r_stat_nlink; \
		__stbuf.st_size  = __rsp->fus_r_stat_size;  \
		__stbuf.st_atime = __rsp->fus_r_stat_atime; \
		__stbuf.st_mtime = __rsp->fus_r_stat_mtime; \
		__stbuf.st_ctime = __rsp->fus_r_stat_ctime; \
		__stbuf.st_uid   = getuid(); \
		__stbuf.st_gid   = getgid(); \
	}; \
	__stbuf; \
})

/* ************************************************************************* */
/*                                   STATFS                                    */
/* ************************************************************************* */

/**
 * STATFS request.
 */
struct fus_m_statfs
{
	uint32_t  fus_q_statfs_magic;       // FUS_MAGIC_STATFS
	uint16_t  fus_q_statfs_pathlenz;    // path length plus nil char
	char      fus_q_statfs_path[];      // path string

} GCC_PACKED;

/**
 * STATFS response (see statvfs() system call)
 */
struct fus_m_statfs_r
{
	uint32_t  fus_r_statfs_magic;       // FUS_MAGIC_STATFS
	uint16_t  fus_r_statfs_err;         // system error code
        uint16_t  fus_r_statfs_namemax;     // maximum filename length
        uint32_t  fus_r_statfs_bsize;       // file system block size
        uint32_t  fus_r_statfs_blocks;      // size of fs in blocks
        uint32_t  fus_r_statfs_bfree;       // # free blocks
        uint32_t  fus_r_statfs_bavail;      // # free blocks (unprivileged)
	uint32_t  fus_r_statfs_files;       // # inodes
        uint32_t  fus_r_statfs_ffree;       // # free inodes

} GCC_PACKED;

/* ************************************************************************* */
/*                                   TRUNC                                   */
/* ************************************************************************* */

/**
 * TRUNC request.
 */
struct fus_m_trunc
{
	uint32_t  fus_q_trunc_magic;        // FUS_MAGIC_TRUNC
	uint32_t  fus_q_trunc_offs;         // offset
	uint32_t  fus_q_trunc_fd;           // file descriptor

} GCC_PACKED;

/**
 * TRUNC response.
 */
struct fus_m_trunc_r
{
	uint32_t  fus_r_trunc_magic;        // FUS_MAGIC_TRUNC
	uint16_t  fus_r_trunc_err;          // system error code

} GCC_PACKED;

/* ************************************************************************* */
/*                                   UNLINK                                    */
/* ************************************************************************* */

/**
 * UNLINK request.
 */
struct fus_m_unlink
{
	uint32_t  fus_q_unlink_magic;       // FUS_MAGIC_UNLINK
	uint16_t  fus_q_unlink_pathlenz;    // path length plus nil char
	char      fus_q_unlink_path[];      // path string

} GCC_PACKED;

/**
 * UNLINK response.
 */
struct fus_m_unlink_r
{
	uint32_t  fus_r_unlink_magic;       // FUS_MAGIC_UNLINK
	uint16_t  fus_r_unlink_err;         // system error code

} GCC_PACKED;

/* ************************************************************************* */
/*                                  WRITE                                    */
/* ************************************************************************* */

/**
 * WRITE request.
 */
struct fus_m_write
{
	uint32_t  fus_q_write_magic;        // FUS_MAGIC_WRITE
	uint32_t  fus_q_write_fd;           // file descriptor
	uint64_t  fus_q_write_offset;       // start offset
	uint32_t  fus_q_write_count;        // number of bytes to write
	uint8_t   fus_q_write_buf[];        // data to be written

} GCC_PACKED;

/**
 * WRITE response.
 */
struct fus_m_write_r
{
	uint32_t  fus_r_write_magic;        // FUS_MAGIC_WRITE
	uint16_t  fus_r_write_err;          // system error code
	uint16_t  fus_r_write_;             // (reserved)
	uint32_t  fus_r_write_count;        // number of bytes writen

} GCC_PACKED;

/* ************************************************************************* */

/**
 * Every supported FUSE request.
 */
union fus_u
{
	uint32_t              fus_u_magic;    // request identifier
	struct fus_m_close    fus_u_close;    // CLOSE
	struct fus_m_login    fus_u_login;    // LOGIN
	struct fus_m_mkdir    fus_u_mkdir;    // MKDIR
	struct fus_m_open     fus_u_open;     // OPEN
	struct fus_m_read     fus_u_read;     // READ
	struct fus_m_readdir  fus_u_readdir;  // READDIR
	struct fus_m_rename   fus_u_rename;   // RENAME
	struct fus_m_rmdir    fus_u_rmdir;    // RMDIR
	struct fus_m_stat     fus_u_stat;     // STAT
	struct fus_m_statfs   fus_u_statfs;   // STATFS
	struct fus_m_trunc    fus_u_trunc;    // TRUNC
	struct fus_m_unlink   fus_u_unlink;   // UNLINK
	struct fus_m_write    fus_u_write;    // WRITE

} GCC_PACKED;

#endif
