/**
 * fus-stat.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fus-internal.h"

int
fus_stat(
	solo_conn_t  * const conn,
	char const   * const path,
	struct stat  * const stbuf
	)
{
	log_t        * const log  = solo_conn_get_log(conn);
	int            err;

	/* check arguments */
	if (path == NULL || stbuf == NULL) {
		log_unexpected(log);
		return EFAULT;
	}

	/* Submit STAT request.
	 */
	size_t const lenz = strlen(path) + 1;
	struct fus_m_stat const req = {
		.fus_q_stat_magic    = FUS_MAGIC_STAT,
		.fus_q_stat_pathlenz = lenz,
	};
	struct iovec const iov[] = {
		{ (void *) &req, sizeof(req), },
		{ (void *) path, lenz, },
	};
	if ((err = solo_sendv(conn, iov, GCC_DIM(iov)))) {
		log_perror(log, LOG_TRACE, "solo_sendv()", err);
		return err;
	}

	/* Fetch STAT response.
	 */
	struct fus_m_stat_r rsp;
	if ((err = solo_recvn(conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "solo_recvn()", err);
		return err;
	}
	if (rsp.fus_r_stat_magic != FUS_MAGIC_STAT) {
		log_unexpected(log);
		return EPROTO;
	}

	/* return information back to the caller */
	err = fus_errno_fus2os(rsp.fus_r_stat_err);
	*stbuf = FUS_MAKE_STBUF_FROM_STRSP(&rsp);
	return err;
}
