/*
 * tcp.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* System Headers */
#include <assert.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <poll.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef __linux__
#include <sys/sendfile.h>
#endif

/* Project Headers */
#include "pthread.h"

/* Public API */
#include "tcp.h"

#define TCP_MAGIC             0x8a499fb1u   // message magic value
#define TCP_MAX_PAYLOAD      (16ull << 20)  // max payload size

struct tcp_server                           // listening server handle
{
	int                   sv_lisfd;     // listening socket
	int                   sv_shutfd[2]; // used to wake poll() (see below)
	log_t               * sv_log;       // log stream

	/* NOTE
	 *	The `sv_shutfd' array contains two file descriptors allocated
	 *	with pipe(). It is used to break out of poll() when the server
	 *	needs to go down.
	 */
};

struct tcp_msghdr                           // message header
{
	uint32_t              hd_magic;     // TCP_MAGIC
	uint32_t              hd_size;      // size of payload
};

struct tcp_conn                             // peer connection
{
	int                   cn_fd;
	log_t               * cn_log;
	bool                  cn_recving;   // recv operation in progress
	struct tcp_msghdr     cn_recvbuf;   // header buffer for partial recv
};

/**
 * Disable SIGPIPE signals (return EPIPE instead).
 */
#ifdef __APPLE__
static inline void
_setnosigpipe(int const fd)
{
	int const opt = 1;
	setsockopt(fd, SOL_SOCKET, SO_NOSIGPIPE, &opt, sizeof(opt));
}
#else
static inline void
_setnosigpipe(int const GCC_ATTRIBUTE_UNUSED(fd))
{
	/* Linux does not support SO_NOSIGPIPE */
	signal(SIGPIPE, SIG_IGN);
}
#endif

/**
 * Cork/uncork connection.
 *
 * TODO
 *	Use writev() instead of TCP_CORK, as writev() is a single system call.
 */
#ifdef __linux__
static inline void
_tcp_cork(int const fd)
{
	int const v = 1;
	int const ret = setsockopt(fd, IPPROTO_TCP, TCP_CORK, &v, sizeof(v));
	assert(ret == 0);
}
static inline void
_tcp_uncork(int const fd)
{
	int const v = 0;
	int const ret = setsockopt(fd, IPPROTO_TCP, TCP_CORK, &v, sizeof(v));
	assert(ret == 0);
}
#else
static inline void
_tcp_cork(int const GCC_ATTRIBUTE_UNUSED(fd))
{
	// TODO
	//	Use TCP_NOPUSH? While this option can certainly be used in
	//	place of TCP_CORK for _tcp_cork(), it is not clear to me that
	//	unsetting it will cause the buffer to be flashed, like in
	//	_tcp_uncork(). See `http://dotat.at/writing/nopush.html'.
	//
}
static inline void
_tcp_uncork(int const GCC_ATTRIBUTE_UNUSED(fd))
{
}
#endif

/**
 * Disable Nagle algorithm on socket.
 */
static inline void
_setnodelay(int const fd)
{
	int const opt = 1;
	setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &opt, sizeof(opt));
}

/**
 * Send data through a socket. Block until all data has been sent.
 */
static inline int
_send(int const sock, void const * buf, size_t size)
{
	while (size) {
		ssize_t const n = send(sock, buf, size, 0);
		if (GCC_UNLIKELY(n < 0))
			return errno;
		if (GCC_UNLIKELY(n == 0))
			return EPIPE;
		buf  += n;
		size -= n;
	}

	return 0;
}

/**
 * Receive data from a socket. Block until all data has been received.
 */
static inline int
_recv(int sock, void * buf, size_t size)
{
	while (size) {
		ssize_t const n = recv(sock, buf, size, 0);
		if (GCC_UNLIKELY(n < 0))
			return errno;
		if (GCC_UNLIKELY(n == 0))
			return EPIPE;
		buf  += n;
		size -= n;
	}
	return 0;
}

int
tcp_listen(
	log_t               * const log,
	short unsigned        const port,
	int                   const backlog,
	struct tcp_server  ** const new_server
	)
{
	struct tcp_server   * srv;
	struct sockaddr_in    in;
	int                   reuse;
	int                   fd;
	int                   err;

	if ((fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "socket()", err);
		goto fail_0;
	}

	reuse = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse))) {
		err = errno;
		log_perror(log, LOG_ERR, "setsockopt()", err);
		goto fail_1;
	}

	in = (struct sockaddr_in) {
		.sin_family = AF_INET,
		.sin_port   = htons(port),
		.sin_addr   = { htonl(INADDR_ANY) },
	};
	if (bind(fd, (struct sockaddr *) &in, sizeof(in)) < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "bind()", err);
		goto fail_1;
	}

	if (listen(fd, backlog) < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "listen()", err);
		goto fail_1;
	}

	/* allocate server handle */
	if ((srv = malloc(sizeof(struct tcp_server))) == NULL) {
		err = errno;
		log_perror(log, LOG_ERR, "malloc()", errno);
		goto fail_1;
	}
	*srv = (struct tcp_server) {
		.sv_lisfd = fd,
		.sv_log   = log,
	};

	/* a file descriptor pair used to wake up poll() upon shutdown */
	if (pipe(srv->sv_shutfd)) {
		err = errno;
		log_perror(log, LOG_ERR, "pipe()", err);
		goto fail_2;
	}

	*new_server = srv;
	return 0;

fail_2:	free(srv);
fail_1:	close(fd);
fail_0:	*new_server = NULL;
	log_assert(log, err);
	return err;
}

void
tcp_shutdown(struct tcp_server * const srv)
{
	/* wake up poll() */
	ssize_t GCC_ATTRIBUTE_UNUSED(ret) = write(srv->sv_shutfd[1], "X", 1);
}

void
tcp_release(struct tcp_server * const srv)
{
	close(srv->sv_shutfd[1]);
	close(srv->sv_shutfd[0]);
	close(srv->sv_lisfd);
	free(srv);
}

int
tcp_accept(
	struct tcp_server   * const srv,
	struct tcp_conn    ** const new_conn
	)
{
	log_t               * const log    = srv->sv_log;
	int                   const lisfd  = srv->sv_lisfd;
	int                   const shutfd = srv->sv_shutfd[0];
	struct tcp_conn     * conn;
	struct sockaddr_in    in;
	socklen_t             insize;
	int                   fd;
	int                   err;

	struct pollfd fdv[2] = {
		{ shutfd, POLLIN, 0 },  // for shutting down
		{ lisfd,  POLLIN, 0 },  // for new connections
	};

	/* wait for a new connection, or a shutdown request */
	if (poll(fdv, 2, -1) < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "poll()", err);
		goto fail_poll_or_accept;
	}
	if (fdv[0].revents) {
		/* NOTE
		 *	Because we never read the data from the pipe, further
		 *	attempts to call tcp_accept() will keep failing with
		 *	ESHUTDOWN. This is the desired behavior.
		 */
		return ESHUTDOWN;
	}

	/* accept the new client connection */
	insize = sizeof(in);
	memset(&in, 0, sizeof(in));
	fd = accept(lisfd, (struct sockaddr *) &in, &insize);
	if (fd < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "accept()", err);
		goto fail_poll_or_accept;
	}

	/* disable SIGPIPE signals (return EPIPE instead) */
	_setnosigpipe(fd);

	/* disable Nagle algorithm on connection */
	_setnodelay(fd);

	/* allocate client descriptor */
	if ((conn = malloc(sizeof(struct tcp_conn))) == NULL) {
		err = errno;
		log_perror(log, LOG_ERR, "malloc()", err);
		goto fail;
	}
	*conn = (struct tcp_conn) {
		.cn_log = log,
		.cn_fd  = fd,
	};

	*new_conn = conn;
	return 0;

fail:
	close(fd);
fail_poll_or_accept:
	*new_conn = NULL;
	log_assert(log, err);
	return err;
}

// TODO Needs timeout
static inline int
_connect(
	struct addrinfo const * const ai,
	int                   * const out_fd
	)
{
	int fd;

	fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
	if (fd < 0) {
		*out_fd = -1;
		return errno;
	}

	if (connect(fd, ai->ai_addr, ai->ai_addrlen) != 0) {
		int const err = errno;
		close(fd);
		*out_fd = -1;
		return err;
	}

	*out_fd = fd;
	return 0;
}

int
tcp_connect(
	log_t            * const log,
	char const       * const host,
	int unsigned       const port,
	struct tcp_conn ** const new_conn
	)
{
	struct tcp_conn  * conn;
	struct addrinfo    hints;
	struct addrinfo  * ai;
	struct addrinfo  * ailist;
	char               service[16];
	int                fd;
	int                err;

	/* resolve host name */
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	snprintf(service, sizeof(service), "%u", port);
	if (getaddrinfo(host, service, &hints, &ailist)) {
		err = EHOSTUNREACH;
		log_perror(log, LOG_ERR, "getaddrinfo()", err);
		goto fail_getaddrinfo;
	}

	log_assert(log, ailist != NULL);

	/* connect */
	ai = ailist;
	while (ai && (err = _connect(ai, &fd)))
		ai = ai->ai_next;
	if (err) {
		log_perror(log, LOG_ERR, "_connect()", err);
		goto fail_connect;
	}

	/* disable SIGPIPE signals (return EPIPE instead) */
	_setnosigpipe(fd);

	/* disable Nagle algorithm on connection */
	_setnodelay(fd);

	/* allocate handle */
	if ((conn = malloc(sizeof(struct tcp_conn))) == NULL) {
		err = errno;
		log_perror(log, LOG_ERR, "malloc()", err);
		goto fail_malloc;
	}
	*conn = (struct tcp_conn) {
		.cn_fd  = fd,
		.cn_log = log,
	};

	freeaddrinfo(ailist);

	*new_conn = conn;
	return 0;

fail_malloc:
	close(fd);
fail_connect:
	freeaddrinfo(ailist);
fail_getaddrinfo:
	log_assert(log, err);
	*new_conn = NULL;
	return err;
}

void
tcp_drop(struct tcp_conn * const conn)
{
	(void) shutdown(conn->cn_fd, SHUT_RDWR);
}

void
tcp_close(struct tcp_conn * const conn)
{
	close(conn->cn_fd);
	free(conn);
}

static inline int
_inithdr(struct tcp_msghdr * const hdr, size_t const payload_size)
{
	if (payload_size > TCP_MAX_PAYLOAD)
		return EFBIG;

	*hdr = (struct tcp_msghdr) {
		.hd_magic = TCP_MAGIC,
		.hd_size  = payload_size,
	};

	return 0;
}

int
tcp_send(
	struct tcp_conn  * const conn,
	void const       * const payload,
	size_t             const payload_size
	)
{
	log_t            * const log  = conn->cn_log;
	int                const sock = conn->cn_fd;
	int                err;

	struct tcp_msghdr hdr;
	if ((err = _inithdr(&hdr, payload_size))) {
		log_perror(log, LOG_ERR, "_inithdr()", err);
		return err;
	}

	_tcp_cork(sock);

	/* header */
	if ((err = _send(sock, &hdr, sizeof(hdr)))) {
		log_perror(log, LOG_ERR, "_send()", err);
		_tcp_uncork(sock);
		return err;
	}

	/* payload */
	if ((err = _send(sock, payload, payload_size))) {
		log_perror(log, LOG_ERR, "_send()", err);
		_tcp_uncork(sock);
		return err;
	}

	_tcp_uncork(sock);

	return 0;
}

int
tcp_sendv(
	struct tcp_conn    * const conn,
	struct iovec const * const iov,
	int unsigned         const iocnt
	)
{
	log_t * const log = conn->cn_log;
	int const sock = conn->cn_fd;
	int unsigned i;
	size_t size;
	int err;

	// FIXME
	//	This whole function should be optimized to use writev().

	size = 0;
	for (i = 0; i < iocnt; i++)
		size += iov[i].iov_len;

	struct tcp_msghdr hdr;
	if ((err = _inithdr(&hdr, size))) {
		log_perror(log, LOG_ERR, "_inithdr()", err);
		return err;
	}

	_tcp_cork(sock);

	/* header */
	if ((err = _send(sock, &hdr, sizeof(hdr)))) {
		log_perror(log, LOG_ERR, "_send()", err);
		_tcp_uncork(sock);
		return err;
	}

	/* payload */
	for (i = 0; i < iocnt; i++)
		if ((err = _send(sock, iov[i].iov_base, iov[i].iov_len))) {
			log_perror(log, LOG_ERR, "_send()", err);
			_tcp_uncork(sock);
			return err;
		}

	_tcp_uncork(sock);

	return 0;
}

#ifdef __linux__

int
tcp_sendfile(
	tcp_conn_t          * const conn,
	struct iovec const  * const iov,
	size_t                const iocnt,
	int                   const fd,
	off_t                 const offs,
	size_t                const count
	)
{
	log_t * const log = conn->cn_log;
	int const sock = conn->cn_fd;
	int unsigned i;
	size_t size;
	int err;

	// FIXME
	//	This whole function should be optimized with TCP_CORK.

	size = 0;

	/* calculate total size of message */
	for (i = 0; i < iocnt; i++)
		size += iov[i].iov_len;
	size += count;

	/* generate message header */
	struct tcp_msghdr hdr;
	if ((err = _inithdr(&hdr, size))) {
		log_perror(log, LOG_ERR, "_inithdr()", err);
		return err;
	}

	_tcp_cork(sock);

	/* send header */
	if ((err = _send(sock, &hdr, sizeof(hdr)))) {
		log_perror(log, LOG_ERR, "_send()", err);
		_tcp_uncork(sock);
		return err;
	}

	/* send payload */
	for (i = 0; i < iocnt; i++)
		if ((err = _send(sock, iov[i].iov_base, iov[i].iov_len))) {
			log_perror(log, LOG_ERR, "_send()", err);
			_tcp_uncork(sock);
			return err;
		}

	/* send file contents */
	off_t offs1 = offs;
	ssize_t const n = sendfile(sock, fd, &offs1, count);
	if (n < 0 || (size_t) n != count) {
		err = n < 0 ? errno : EIO;
		log_perror(log, LOG_ERR, "sendfile()", err);
		_tcp_uncork(sock);
		return err;
	}

	_tcp_uncork(sock);

	return 0;
}

#else  // ifdef __linux__

int
tcp_sendfile(
	tcp_conn_t          * const GCC_ATTRIBUTE_UNUSED(conn),
	struct iovec const  * const GCC_ATTRIBUTE_UNUSED(iov),
	size_t                const GCC_ATTRIBUTE_UNUSED(iocnt),
	int                   const GCC_ATTRIBUTE_UNUSED(fd),
	off_t                 const GCC_ATTRIBUTE_UNUSED(offs),
	size_t                const GCC_ATTRIBUTE_UNUSED(count)
	)
{
	// TODO
	//	Mac OS also has a sendfile() system call. We should support it
	//	at some point in the future.
	//

	return ENOSYS;
}

#endif // ifdef __linux__

int
tcp_recv(
	struct tcp_conn   * const conn,
	void              * const buf,
	size_t              const buf_size,
	size_t            * const payload_size
	)
{
	log_t             * const log  =  conn->cn_log;
	struct tcp_msghdr * const phdr = &conn->cn_recvbuf;
	int                 const fd   =  conn->cn_fd;
	int                 err;

	log_assert(log, payload_size != NULL);

	/* are we finishing a previous receive operation? */
	if (conn->cn_recving) {
		conn->cn_recving = false;
		goto skip_header;
	}

	// NOTE
	//	Notice that if the message header is somehow corrupted or the
	//	payload exceeds the maximum allowed size, then the socket is
	//	shut down and no further communication is possible.

	/* receive and verify message header */
	if ((err = _recv(fd, phdr, sizeof(struct tcp_msghdr)))) {
		if (err != EPIPE)
			log_perror(log, LOG_ERR, "_recv()", err);
		*payload_size = 0;
		return err;
	}
	if (phdr->hd_magic != TCP_MAGIC) {
		log_printf(log, LOG_ERR, "invalid magic number");
		shutdown(fd, SHUT_RDWR);
		*payload_size = 0;
		return EPROTO;
	}
	if (phdr->hd_size == 0 || phdr->hd_size > TCP_MAX_PAYLOAD) {
		log_printf(log, LOG_ERR, "invalid payload size");
		shutdown(fd, SHUT_RDWR);
		*payload_size = 0;
		return EPROTO;
	}

skip_header:
	if (phdr->hd_size > buf_size) {  // is the buffer big enough?
		conn->cn_recving = true;
		*payload_size = phdr->hd_size;
		return ENOSPC;
	}

	log_assert(log, buf != NULL);
	if ((err = _recv(fd, buf, phdr->hd_size))) {
		log_perror(log, LOG_ERR, "_recv()", err);
		*payload_size = 0;
		return err;
	}

	*payload_size = phdr->hd_size;
	return 0;
}

int
tcp_recvn(
	struct tcp_conn  * const conn,
	void             * const payload,
	size_t             const payload_size
	)
{
	log_t * const log = conn->cn_log;
	size_t got_size;

	int const err = tcp_recv(conn, payload, payload_size, &got_size);
	if (err == ENOSPC || (!err && got_size != payload_size)) {
		log_printf(log, LOG_ERR,
		    "unexpected message size (got %zuB, expected %zuB)",
		    got_size, payload_size);
		return EPROTO;
	}

	return err;
}

int
tcp_recva(
	struct tcp_conn  * conn,
	void            ** payload,      // [out]
	size_t           * payload_size  // [out]
	)
{
	log_t            * const log = conn->cn_log;
	void             * buf;
	size_t             size;
	int                err;

	log_assert(log, payload != NULL);
	log_assert(log, payload_size != NULL);

	/* read size of message */
	err = tcp_recv(conn, NULL, 0, &size);
	log_assert(log, err != 0);
	if (err == EPIPE)
		goto fail_0;
	else if (err != ENOSPC) {
		log_perror(log, LOG_TRACE, "tcp_recv()", err);
		goto fail_0;
	}

	/* allocate buffer for message payload */
	if ((buf = malloc(size)) == NULL) {
		err = errno;
		log_perror(log, LOG_ERR, "malloc()", err);
		goto fail_0;
	}

	/* receive payload */
	if ((err = tcp_recv(conn, buf, size, &size))) {
		log_perror(log, LOG_TRACE, "tcp_recv()", err);
		goto fail_1;
	}

	*payload = buf;
	*payload_size = size;
	return 0;

fail_1:	free(buf);
fail_0:	*payload = NULL;
	*payload_size = 0;
	log_assert(log, err);
	return err;
}

int
tcp_fileno(struct tcp_conn * const conn)
{
	return conn->cn_fd;
}

int
tcp_fileno_server(struct tcp_server * const srv)
{
	return srv->sv_lisfd;
}

int
tcp_serverinfo(
	struct tcp_server  * const srv,
	uint32_t           * const addr,
	uint16_t           * const port )
{
	log_t              * const log = srv->sv_log;
	struct sockaddr_in   in;
	socklen_t            inlen;
	int                  err;

	inlen = sizeof(in);
	if (getsockname(srv->sv_lisfd, (struct sockaddr *) &in, &inlen) < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "getsockname()", err);
		*addr = 0;
		*port = 0;
		return err;
	}

	*addr = ntohl(in.sin_addr.s_addr);
	*port = ntohs(in.sin_port);
	return 0;
}

#ifdef __APPLE__

void
tcp_keepalive(
	tcp_conn_t * GCC_ATTRIBUTE_UNUSED(conn),
	unsigned     GCC_ATTRIBUTE_UNUSED(start),
	unsigned     GCC_ATTRIBUTE_UNUSED(intvl),
	unsigned     GCC_ATTRIBUTE_UNUSED(count)
	)
{
	// FIXME
	//	OS X does have a SO_KEEPALIVE option, but I'm not aware of any
	//	other option concerning the tuning of the keepalive probes.
	//	We need to investigate how to make this work on the Mac.
}

#else // #ifdef __APPLE__

void
tcp_keepalive(
	tcp_conn_t * conn,
	unsigned     start,
	unsigned     intvl,
	unsigned     count
	)
{
	log_t      * const log = conn->cn_log;
	int          const fd  = conn->cn_fd;
	int          const val = 1;
	socklen_t    const len = sizeof(val);

	if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &val, len)) {
		int const err = errno;
		log_perror(log, LOG_WARN, "setsockopt(SO_KEEPALIVE)", err);
		return;
	}

	if (setsockopt(fd, SOL_TCP, TCP_KEEPIDLE, &start, sizeof(start))) {
		int const err = errno;
		log_perror(log, LOG_WARN, "setsockopt(TCP_KEEPIDLE)", err);
	}

	if (setsockopt(fd, SOL_TCP, TCP_KEEPINTVL, &intvl, sizeof(intvl))) {
		int const err = errno;
		log_perror(log, LOG_WARN, "setsockopt(TCP_KEEPINTVL)", err);
	}

	if (setsockopt(fd, SOL_TCP, TCP_KEEPCNT, &count, sizeof(count))) {
		int const err = errno;
		log_perror(log, LOG_WARN, "setsockopt(TCP_KEEPCNT)", err);
	}
}
#endif
