/*
 * switchd-serve-client.c
 *
 *	Handle client connection.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <fcntl.h>
#include <poll.h>
#include <sys/epoll.h>

#include "switchd.h" // private header

/* Shortcut to log debug messages showing the session number */
#define _debug(ses, fmt, ...) \
	log_printf(ses->ses_log, LOG_DEBUG, "SES%u: " fmt, \
	           ses->ses_no, ## __VA_ARGS__)

/**
 * Send REGISTER reply message to client.
 */
static inline int
_client_reply(struct sosw_ses * const ses, enum solo_err const e)
{
	int err;

	struct solo_m_register_r const rsp = {
		.solo_r_reg_magic = SOLO_MAGIC_REGISTER,
		.solo_r_reg_err   = e,
	};

	if ((err = tcp_send(ses->ses_conn, &rsp, sizeof(rsp)))) {
		log_perror(ses->ses_log, LOG_ERR, "tcp_send()", err);
		return err; // TCP error overrides given Solo error code
	}

	/* return an error code appropriate for the given status code */
	switch (e) {
	case SOLO_OK:
		return 0;
	case SOLO_EBUSY:
		return EBUSY;
	case SOLO_ENOENT:
		return ENOENT;
	default:
		return EPERM;
	}
}

static int
_wait_for_events(
	log_t           * const log,
	tcp_server_t    * const srv,   // listening socket
	tcp_conn_t      * const cli    // connected client socket
	)
{
	short const events = POLLERR | POLLHUP | POLLIN;

	struct pollfd fds[2] = {
		{ tcp_fileno_server(srv), events, 0 },
		{ tcp_fileno(cli), events, 0 },
	};

	switch (poll(fds, 2, SWITCHD_PROXY_TIMEOUT)) {

	case -1:
		return errno;       // poll() error

	case 0:
		return ETIMEDOUT;   // poll() timeout

	case 1:	/* This is the expected return. The server should have made a
		 * connection to the proxy port number, so the listening
		 * socket should have pending events.
		 */
		return fds[0].revents && !fds[1].revents ? 0 : EPIPE;

	case 2:	/* Too many events, something went wrong, probably the client
		 * got disconnected.
		 */
		return EPIPE;

	default:
		log_bug(log);
	}
}

static inline int
_splice(log_t * const GCC_ATTRIBUTE_UNUSED(log),
	int     const pipefd[],
	int     const infd,
	int     const outfd)
{
	// XXX
	//
	//	Here we have a socket-to-socket data transfer using splice(),
	//	a Linux system call designed to keep the data to be
	//	transferred in kernel space as well as minimize movement of
	//	the data between buffers. Notice that the function is
	//	logically split into two phases, since splice() cannot operate
	//	from a socket directly to another socket. Instead, a pipe
	//	needs to be present as intermediary.
	//
	//	The read phase of the function should not block, since we are
	//	guaranteed (by the poll() in _proxy()) that data is present in
	//	the input file descriptor. So far tests have confirmed this,
	//	even if the data to be read is less than `max'. splice()
	//	simply returns the number of bytes that were read, which is
	//	typically less than `max'. However, the splice() manual is not
	//	terribly clear about the blocking behavior of the system call.
	//	In particular, the meaning of the SPLICE_F_NONBLOCK flag still
	//	remains a bit of a mystery to me, especially because the
	//	manual does not list EWOULDBLOCK as one of the possible return
	//	codes.
	//
	//	In the write phase, we are not concerned about blocking at
	//	all. In fact, blocking is desirable as all of the data
	//	obtained during the read phase now needs to be written to the
	//	output socket.
	//

	/* read from input socket */
	size_t const max = 16 << 10; // 16 KB
	int const rflags = SPLICE_F_NONBLOCK | SPLICE_F_MOVE;
	ssize_t const r = splice(infd, NULL, pipefd[1], NULL, max, rflags);
	if (r == 0)
		return EPIPE;
	else if (r < 0)
		return errno;

//-	log_printf(log, LOG_DEBUG, "IN: splice(%d) = %zd", infd, r); // FIXME

	/* write to output socket */
	int const wflags = SPLICE_F_MOVE;
	ssize_t const w = splice(pipefd[0], NULL, outfd, NULL, r, wflags);
	if (w == 0)
		return EPIPE;
	else if (w < 0)
		return errno;

//-	log_printf(log, LOG_DEBUG, "OUT: splice(%d) = %zd", outfd, w); // FIXME

	return 0;
}

static int
_proxy(
	log_t      * const log,
	tcp_conn_t * const srv,
	tcp_conn_t * const cli
	)
{
	int          const srvfd = tcp_fileno(srv);
	int          const clifd = tcp_fileno(cli);
	int          pipefd[2];
	int          err;

	/* poll() vector */
	short const events = POLLERR | POLLHUP | POLLIN;
	struct pollfd pollfd[2] = {
		{ srvfd, events, 0 },
		{ clifd, events, 0 },
	};

	/* allocate pipe to be used by the splice() system call */
	if (pipe(pipefd) < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "pipe()", err);
		return err;
	}

	/* proxy data between the two connections */
	err = 0;
	while (err == 0)
	{
		/* listen for events on the file descriptors */
		int const n = poll(pollfd, 2, -1);
		if (n < 0) {
			err = errno;
			log_perror(log, LOG_ERR, "poll()", err);
			continue;
		}
		log_assert(log, n > 0);

		/* errors */
		if (   pollfd[0].revents & ~POLLIN
		    || pollfd[1].revents & ~POLLIN)
			err = EPIPE;
		/* from server to client */
		else if (pollfd[0].revents & POLLIN)
			err = _splice(log, pipefd, srvfd, clifd);
		/* from client to server */
		else if (pollfd[1].revents & POLLIN)
			err = _splice(log, pipefd, clifd, srvfd);
		/* anything else... */
		else
			log_bug(log);

		/* report _splice() error if relevant */
		if (err && err != EPIPE)
			log_perror(log, LOG_ERR, "_splice()", err);
	}

	close(pipefd[0]);
	close(pipefd[1]);

	return err == EPIPE ? 0 : err;
}

int
switchd_serve_client(struct sosw_ses * const ses)
{
	struct sosw     * const sw    = ses->ses_sw;
	log_t           * const log   = ses->ses_log;
	char const      * const srvid = ses->ses_svid;
	enum solo_proto   const proto = ses->ses_proto;
	struct sosw_ses * srvses;
	tcp_conn_t      * pxyconn;
	list_t          * it;
	int               err;

	log_entering(log);

	_debug(ses, "looking for %s:%u", srvid, proto);

	Pthread_mutex_lock(&sw->sw_mapmux);

	/* Look for the requested server ID and protocol number.
	 */
	srvses = NULL;
	list_for_each(it, &sw->sw_maplist) {
		struct sosw_map const * const
			map = list_entry(it, struct sosw_map, map_node);
		if (!strcmp(map->map_id, srvid) && map->map_proto == proto) {
			srvses = map->map_ses;
			break;
		}
	}
	if (srvses == NULL) {
		_debug(ses, "cannot find %s:%u", srvid, proto);
		Pthread_mutex_unlock(&sw->sw_mapmux);
		/* failed to locate server */
		err = _client_reply(ses, SOLO_ENOENT);
		goto fail_0;
	}

	_debug(ses, "waiting for %s:%u", srvid, proto);

	/* lock the server session */
	Pthread_mutex_lock(&srvses->ses_climux);

	/* it is now safe to unlock the map */
	Pthread_mutex_unlock(&sw->sw_mapmux);

	/* We found the server, now wake up the server session by adding our
	 * file descriptor to the server session's epoll() vector.
	 */
	struct epoll_event epev = {
		.data.ptr = ses,      // our client session
		.events   = EPOLLIN   // for (future) keepalive messages
		          | EPOLLOUT  // to wake up the server session
			  | EPOLLERR | EPOLLHUP | EPOLLRDHUP,
	};
	int const epfd = srvses->ses_epfd; // server epoll() file descriptor
	int const fd = tcp_fileno(ses->ses_conn); // our file descriptor
	if (epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &epev)) {
		err = errno;
		log_perror(log, LOG_ERR, "epoll_ctl()", err);
		Pthread_mutex_unlock(&srvses->ses_climux);
		/* an internal error has occurred, maybe out of resource? */
		(void) _client_reply(ses, SOLO_EBUSY);
		goto fail_0;
	}

	/* Add ourselves to the list of pending clients in the server session.
	 * The list keeps track of the clients that wish to connect to the
	 * server but for which an ACCEPT message has not yet been received
	 * from the server.
	 */
	list_add_tail(&ses->ses_clinode, &srvses->ses_cliwait);
	srvses->ses_clinum++;

	/* Sleep until we receive a go-ahead signal from the server session.
	 * The signal is sent upon receipt of an ACCEPT message from the
	 * server, but also in the event that the client socket hangs up or
	 * produces an error (see EPOLLERR and EPOLLHUP conditions above).
	 * Basically, at this point we temporarily delegate control of our
	 * socket to the server session.
	 */
	while (ses->ses_pxysrv == NULL && !ses->ses_pxybail)
		Pthread_cond_wait(&ses->ses_pxycond, &srvses->ses_climux);

	/* notify server session if this is the last pending client, so that
	 * the server session can safely destroy its mutex */
	log_assert(log, srvses->ses_clinum > 0);
	srvses->ses_clinum--;
	if (srvses->ses_clinum == 0 && srvses->ses_down)
		Pthread_cond_signal(&srvses->ses_cliend);

	Pthread_mutex_unlock(&srvses->ses_climux);

	/* bail if the server or client socket was dropped */
	if (ses->ses_pxybail) {
		_debug(ses, "connection aborted");
		err = _client_reply(ses, SOLO_EBUSY);
		goto fail_0;
	}

	/* we now own a listening socket used for the proxy connection */
	log_assert(log, ses->ses_pxysrv != NULL);

	_debug(ses, "connecting to %s:%u", srvid, proto);

	/* Observe both the listening socket (for a connection from the
	 * server) and the client socket (in case it gets dropped). As we are
	 * only dealing with 2 descriptors, a simple poll() should suffice.
	 */
	if ((err = _wait_for_events(log, ses->ses_pxysrv, ses->ses_conn))) {
		log_perror(log, LOG_ERR, "_wait_for_events()", err);
		_debug(ses, "cannot establish proxy connection");
		goto fail_1;
	}

	/* accept the server connection on the listening socket */
	if ((err = tcp_accept(ses->ses_pxysrv, &pxyconn))) {
		log_perror(log, LOG_TRACE, "tcp_accept()", err);
		(void) _client_reply(ses, SOLO_EBUSY);
		goto fail_1;
	}

	/* tell the client everything is ready to go */
	if ((err = _client_reply(ses, SOLO_OK))) {
		log_perror(log, LOG_TRACE, "_client_reply()", err);
		goto fail_2;
	}

	/* no need to keep the listening socket around */
	tcp_release(ses->ses_pxysrv);
	ses->ses_pxysrv = NULL;

	/* begin proxy function */
	if ((err = _proxy(log, pxyconn, ses->ses_conn))) {
		log_perror(log, LOG_TRACE, "_proxy()", err);
		goto fail_2;
	}

	/* close proxy connection */
	_debug(ses, "disconnecting from %s:%u", srvid, proto);
	_debug(ses, "end of session");
	tcp_close(pxyconn);
	log_leaving(log);
	return 0;

fail_2:
	tcp_close(pxyconn);
fail_1:
	if (ses->ses_pxysrv) {
		tcp_release(ses->ses_pxysrv);
		ses->ses_pxysrv = NULL;
	}
fail_0:
	_debug(ses, "end of session (with errors)");
	log_assert(log, err);
	log_leaving(log);
	return err;
}
