/*
 * switchd-main.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <signal.h>
#include <string.h>
#include <sysexits.h>

#include "switchd.h"  // private header

/**
 * Parse command line arguments, configuration file if any.
 */
static int
switchd_config(struct sosw_cf * const cf)
{
	*cf = (struct sosw_cf) {
		.cf_portno = SWITCHD_PORTNO,
	};

	return 0;
}

/**
 * Configure server runtime environment.
 */
static int
switchd_init(
	struct sosw_cf const * const cf,
	struct sosw          * const sw )
{
	log_t                * log;
	tcp_server_t         * srv;
	int                    err;

	memset(sw, 0, sizeof(*sw));

	/* global configuration */
	sw->sw_cf = *cf;

	/* open log file */
	if ((err = log_open_f(stderr, &log))) {
		switchd_fatal("cannot open log file");
		goto fail_log;
	}
	sw->sw_log = log;

	/* open TCP port for switchboard services */
	short unsigned const portno = cf->cf_portno;
	if ((err = tcp_listen(log, portno, cf->cf_backlog, &srv))) {
		char const * const reason = strerror(err);
		switchd_fatal("cannot listen on port %u: %s", portno, reason);
		goto fail_listen;
	}
	sw->sw_srv = srv;

	/* initialize session list */
	Pthread_mutex_init(&sw->sw_sesmux, NULL);
	Pthread_cond_init(&sw->sw_sescond, NULL);
	list_init(&sw->sw_seslist);

	/* initialize ID map */
	Pthread_mutex_init(&sw->sw_mapmux, NULL);
	list_init(&sw->sw_maplist);

	return 0;

fail_listen:
	log_assert(log, err);
	log_close(log);
fail_log:
	memset(sw, 0, sizeof(*sw));
	return err;
}

/**
 * Destroy server runtime environment.
 */
static void
switchd_uninit(struct sosw * const sw)
{
	/* destroy ID map */
	list_uninit(&sw->sw_maplist);
	Pthread_mutex_destroy(&sw->sw_mapmux);
	/* destroy session list */
	list_uninit(&sw->sw_seslist);
	Pthread_cond_destroy(&sw->sw_sescond);
	Pthread_mutex_destroy(&sw->sw_sesmux);
	/* final cleanup */
	tcp_release(sw->sw_srv);
	log_close(sw->sw_log);
	memset(sw, 0, sizeof(*sw));
}

/**
 * Main server body.
 */
static int
switchd_main(struct sosw * const sw)
{
	log_t     * const log = sw->sw_log;
	pthread_t   sigid;
	sigset_t    sigset;
	int         err;

	log_printf(log, LOG_INFO, "starting up");

	/* block all signals */
	sigfillset(&sigset);
	pthread_sigmask(SIG_BLOCK, &sigset, NULL);

	/* fork signal handling thread */
	void * (* const thread)(void *) = switchd_sigwait__thread;
	if ((err = pthread_create(&sigid, NULL, thread, sw))) {
		log_perror(log, LOG_ERR, "pthread_create()", err);
		goto fail_sigwait;
	}

	/* handle connections */
	if ((err = switchd_listen(sw))) {
		log_perror(log, LOG_TRACE, "siwtchd_listen()", err);
		goto fail_listen;
	}

	/* wait for the signal handling thread (if still running) */
	pthread_kill(sigid, SIGINT);
	pthread_join(sigid, NULL);

	log_printf(log, LOG_INFO, "exiting now");
	return 0;

fail_listen:
	pthread_kill(sigid, SIGINT);
	pthread_join(sigid, NULL);
fail_sigwait:
	log_printf(log, LOG_INFO, "exiting now (with errors)");
	log_assert(log, err);
	return err;
}

int
main(void)
{
	struct sosw_cf cf;
	struct sosw    sw;
	int            err;

	/* parse command line arguments, configuration file */
	if ((err = switchd_config(&cf)))
		goto fail;

	/* prepare server runtime environment */
	if ((err = switchd_init(&cf, &sw)))
		goto fail;

	/* run server */
	err = switchd_main(&sw);

	/* destroy server runtime environment */
	switchd_uninit(&sw);

fail:	return err ? EX_SOFTWARE : EX_OK;
}
