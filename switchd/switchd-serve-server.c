/*
 * switchd-serve-server.c
 *
 *	Handle server connection.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <sys/epoll.h>

#include "switchd.h"  // private header

/* Shortcut to log debug messages showing the server identifiers */
#define _debug(ses, fmt, ...) \
	log_printf(ses->ses_log, LOG_DEBUG, "%s:%u: " fmt, \
	           ses->ses_svid, ses->ses_no, ## __VA_ARGS__)

static inline int
_server_reply(struct sosw_ses * const ses, enum solo_err const e)
{
	int err;

	/* submit status message to peer */
	struct solo_m_register_r const rsp = {
		.solo_r_reg_magic = SOLO_MAGIC_REGISTER,
		.solo_r_reg_err   = e,
	};
	if ((err = tcp_send(ses->ses_conn, &rsp, sizeof(rsp)))) {
		log_perror(ses->ses_log, LOG_ERR, "tcp_send()", err);
		return err;  // TCP error overrides given Solo error code
	}

	/* return an error code appropriate for the given status code */
	switch (e) {
	case SOLO_OK:
		return 0;
	case SOLO_EEXIST:
		return EEXIST;
	default:
		return EPERM;
	}
}

static int
_publish_session(struct sosw_ses * const ses)
{
	struct sosw     * const sw    = ses->ses_sw;
	char const      * const id    = ses->ses_svid;
	enum solo_proto   const proto = ses->ses_proto;
	list_t          * it;

	/* initialize pending client list */
	Pthread_cond_init(&ses->ses_cliend, NULL);
	Pthread_mutex_init(&ses->ses_climux, NULL);
	list_init(&ses->ses_cliwait);

	Pthread_mutex_lock(&sw->sw_mapmux);

	// FIXME
	//	If another server session is using the same ID, we should
	//	probably abort that session granted we make sure that the
	//	session is stale.

	/* abort if a session with the same ID is already present */
	list_for_each(it, &sw->sw_maplist) {
		struct sosw_map const * const
			map = list_entry(it, struct sosw_map, map_node);
		if (!strcmp(map->map_id, id) && map->map_proto == proto) {
			Pthread_mutex_unlock(&sw->sw_mapmux);
			return EEXIST;
		}
	}

	/* map server ID to this session, so clients can find it */
	ses->ses_map.map_ses   = ses;
	ses->ses_map.map_id    = id;
	ses->ses_map.map_proto = proto;
	list_add_tail(&ses->ses_map.map_node, &sw->sw_maplist);

	Pthread_mutex_unlock(&sw->sw_mapmux);

	return 0;
}

static void
_revoke_session(struct sosw_ses * const ses)
{
	struct sosw * const sw  = ses->ses_sw;

	/* Unmap session. This will prevent new clients from findind us.
	 */
	Pthread_mutex_lock(&sw->sw_mapmux);
	list_del(&ses->ses_map.map_node);
	Pthread_mutex_unlock(&sw->sw_mapmux);

	/* Abort all pending clients. Notice that there is no need to remove
	 * the clients from the epoll() vector as this will be soon be
	 * deallocated. Also, strictly speaking we don't need to remove the
	 * clients from the pending list, but we do it anyway to catch
	 * potential bugs in the client session.
	 */
	Pthread_mutex_lock(&ses->ses_climux);
	list_t * it, * _it;
	list_for_each_safe(it, _it, &ses->ses_cliwait) {
		struct sosw_ses * const clises =
			list_entry(it, struct sosw_ses, ses_clinode);
		/* remove client from pending list (just in case) */
		list_del(&clises->ses_clinode);
		/* notify client session that connection was dropped */
		clises->ses_pxybail = true;
		Pthread_cond_signal(&clises->ses_pxycond);
	}
	/* wait for all pending clients to finish (we need to wait as these
	 * client sessions still need to access our mutex, and we are about to
	 * destroy it) */
	ses->ses_down = true;
	while (ses->ses_clinum)
		Pthread_cond_wait(&ses->ses_cliend, &ses->ses_climux);
	Pthread_mutex_unlock(&ses->ses_climux);

	/* other clean up */
	Pthread_cond_destroy(&ses->ses_cliend);
	Pthread_mutex_destroy(&ses->ses_climux);
	list_uninit(&ses->ses_cliwait);
}

/**
 * If a client is pending for the given server session, this function attempts
 * to establish a proxy connection beteween the server and the first pending
 * client. The function wakes up the client session.
 */
static void
_pick_a_client(struct sosw_ses * const ses)
{
	log_t        * const log = ses->ses_log;
	tcp_server_t * srv;
	uint32_t       ipaddr;
	uint16_t       portno;
	int            err;

	/* server must be ready to accept a connection */
	log_assert(log, ses->ses_accept);

	/*
	 * Perform the following operations:
	 *
	 * - Pick the first client from the list of pending clients.
	 * - Remove client from the list of pending clients.
	 * - Remove client from epoll() vector.
	 * - Open dynamic TCP/IP listening socket.
	 * - Send ACCEPT reply to sever with chosen port.
	 * - Pass listening socket to client session.
	 * - Wake up client session.
	 */

	Pthread_mutex_lock(&ses->ses_climux);

	/* pick the first client in the pending client list */
	if (list_empty(&ses->ses_cliwait)) {
		Pthread_mutex_unlock(&ses->ses_climux);
		return; // list is empty, nothing to do
	}
	struct sosw_ses * const clises =
		list_first_entry(&ses->ses_cliwait,
				 struct sosw_ses,
				 ses_clinode);

	_debug(ses, "selected SES%u", clises->ses_no);

	/* remove client from the list of pending clients */
	list_del(&clises->ses_clinode);

	/* remove client from epoll() vector */
	int const clifd = tcp_fileno(clises->ses_conn);
	if (epoll_ctl(ses->ses_epfd, EPOLL_CTL_DEL, clifd, NULL))
		log_panic(log);

	/* open TCP/IP listening socket on a dynamic port to be used for the
	 * Proxy connection */
	if ((err = tcp_listen(log, 0, 0, &srv))) {
		log_perror(log, LOG_TRACE, "tcp_listen()", err);
		goto fail_0;
	}
	if ((err = tcp_serverinfo(srv, &ipaddr, &portno))) {
		log_perror(log, LOG_TRACE, "tcp_serverinfo()", err);
		goto fail_1;
	}

	/* submit ACCEPT reply to server; the reply contains the port number
	 * to be used for the proxy connection */
	struct solo_m_accept_r const rsp = {
		.solo_r_acc_magic = SOLO_MAGIC_ACCEPT,
		.solo_r_acc_err   = SOLO_OK,
		.solo_r_acc_port  = portno,
	};
	if ((err = tcp_send(ses->ses_conn, &rsp, sizeof(rsp)))) {
		log_perror(log, LOG_TRACE, "tcp_send()", err);
		goto fail_2;
	}

	/* pass listening socket to client session and wake up the session */
	clises->ses_pxysrv  = srv;
	clises->ses_pxybail = false;
	Pthread_cond_signal(&clises->ses_pxycond);

	Pthread_mutex_unlock(&ses->ses_climux);

	/* server is now busy */
	ses->ses_accept = false;
	return;

fail_2:	/* something went wrong with the server socket... */
	tcp_drop(ses->ses_conn);
	ses->ses_accept = false;
fail_1:	/* close listening proxy socket */
	tcp_release(srv);
fail_0:	/* abort client session */
	clises->ses_pxysrv  = NULL;
	clises->ses_pxybail = true;
	Pthread_cond_signal(&clises->ses_pxycond);
	Pthread_mutex_unlock(&ses->ses_climux);
	log_assert(log, err);
	return;
}

/**
 * Handle epoll() event received on a client socket.
 */
static int
_handle_event_from_client(
	struct sosw_ses          * const ses,
	struct epoll_event const * const epev
	)
{
	log_t    * const log    = ses->ses_log;
	uint32_t   const events = epev->events;

	/* the client session */
	struct sosw_ses * const clises = epev->data.ptr;

	/* the client socket */
	int const clifd = tcp_fileno(clises->ses_conn);

	/* Client dropped its connection. Notice that this event will also
	 * result in an EPOLLIN event (I guess because one could consider
	 * reading 0 bytes from a socket an EOF event), so it is important to
	 * handle this event before EPOLLIN to avoid confusion.
	 */
	if (events & (EPOLLERR | EPOLLHUP | EPOLLRDHUP))
	{
		_debug(ses, "disconnected from SES%u", clises->ses_no);

		/* remove client from epoll() vector */
		if (epoll_ctl(ses->ses_epfd, EPOLL_CTL_DEL, clifd, NULL))
			log_panic(log);

		Pthread_mutex_lock(&ses->ses_climux);
		/* remove client from pending list */
		list_del(&clises->ses_clinode);
		/* notify client session that connection was dropped */
		clises->ses_pxybail = true;
		Pthread_cond_signal(&clises->ses_pxycond);
		Pthread_mutex_unlock(&ses->ses_climux);

		return 0;
	}

	/* Data available. Possibly a keepalive message.
	 */
	if (events & EPOLLIN)
	{
		log_bug(log); // FIXME Not yet implemented.
	}

	/* A new client wants to connect to our server (the EPOLLOUT event
	 * doesn't mean that we need to write to the socket, we simply use
	 * this event to notify epoll() that a new socket has been added to
	 * the vector).
	 */
	if (events & EPOLLOUT)
	{
		_debug(ses, "acknowledged SES%u", clises->ses_no);

		/* If we have already received an ACCEPT message from the
		 * server, we can connect the server with this client without
		 * delay ...
		 */
		if (ses->ses_accept) {
			_pick_a_client(ses);
			return 0;
		}

		/* ... Otherwise the server is not ready to accept the
		 * connection. Acknowledge the epoll event and keep the client
		 * session on the pending list.
		 */
		struct epoll_event epev1 = {
			.data.ptr = clises,
			.events   = EPOLLIN | EPOLLERR | EPOLLHUP | EPOLLRDHUP,
		};
		if (epoll_ctl(ses->ses_epfd, EPOLL_CTL_MOD, clifd, &epev1) < 0)
			log_panic(log);

		return 0;
	}

	/* we should not get here */
	log_bug(log);
}

/**
 * Handle epoll() event received on a server socket.
 */
static int
_handle_event_from_server(
	struct sosw_ses           * const ses,
	struct epoll_event const  * const epev
	)
{
	log_t      * const log    = ses->ses_log;
	tcp_conn_t * const conn   = ses->ses_conn;
	uint32_t     const events = epev->events;
	int          err;

	/* Server has been disconnected. We are done with this session and all
	 * the clients waiting to be connected.
	 */
	if (events & (EPOLLHUP | EPOLLRDHUP | EPOLLERR)) {
		_debug(ses, "server hang up");
		return EPIPE;
	}

	/* Data is available from the server. Right now this means that we
	 * have received an ACCEPT message from the server.
	 */
	if (events & EPOLLIN)
	{
		_debug(ses, "received ACCEPT");

		/* we should not be waiting on ACCEPT already */
		if (ses->ses_accept) {
			log_unexpected(log);
			return EPROTO;
		}

		/* fetch and verify the ACCEPT message */
		struct solo_m_accept req;
		if ((err = tcp_recvn(conn, &req, sizeof(req)))) {
			log_perror(log, LOG_TRACE, "tcp_recvn()", err);
			return err;
		}
		if (req.solo_q_acc_magic != SOLO_MAGIC_ACCEPT) {
			log_unexpected(log);
			return EPROTO;
		}

		// FIXME
		//	Only the proxy mode is supported at this time. But
		//	it's not even clear whether it is the client that
		//	should request this mode.

		if (req.solo_q_acc_flags != SOLO_FLAG_PROXY) {
			log_unexpected(log);
			return EPROTO;
		}

		/* flag server session as ready to accept a client */
		ses->ses_accept = true;

		/* pick a client from the list of pending clients, if any */
		_pick_a_client(ses);
		return 0;
	}

	/* we should not get here */
	log_unexpected(log);
	return EPROTO;
}

static int
_handle_event(
	struct sosw_ses          * const ses,
	struct epoll_event const * const epev
	)
{
	struct sosw_ses * const clises = epev->data.ptr;
	return clises ? _handle_event_from_client(ses, epev)
	              : _handle_event_from_server(ses, epev);
}

static int
_accept(struct sosw_ses * const ses)
{
	log_t              * const log  = ses->ses_log;
	tcp_conn_t         * const conn = ses->ses_conn;
	struct epoll_event   epev;
	int                  epfd;
	int                  err;

	_debug(ses, "check-in");

	/* we use epoll() to listen to the server socket as well as any other
	 * client socket that is trying to establish a connection with the
	 * server */
	if ((epfd = epoll_create1(0)) < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "epoll_create1()", err);
		(void) _server_reply(ses, SOLO_EBUSY);
		goto fail_epoll_create;
	}

	ses->ses_epfd = epfd;

	/* the epoll() descriptor with a NULL pointer data field refers to the
	 * current session, the server session */
	epev = (struct epoll_event) {
		.events = EPOLLIN | EPOLLERR | EPOLLHUP | EPOLLRDHUP,
	};
	if (epoll_ctl(epfd, EPOLL_CTL_ADD, tcp_fileno(conn), &epev) < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "epoll_ctl()", err);
		(void) _server_reply(ses, SOLO_EBUSY);
		goto fail_epoll_add_server;
	}

	/* publish the current session; from now on clients can find us */
	if ((err = _publish_session(ses))) {
		log_perror(log, LOG_TRACE, "_publish_session()", err);
		(void) _server_reply(ses, SOLO_EEXIST);
		goto fail_publish_session;
	}

	/* notify server we are ready,  */
	if ((err = _server_reply(ses, SOLO_OK))) {
		log_perror(log, LOG_TRACE, "_server_reply()", err);
		goto fail;
	}

	_debug(ses, "ready");

	/* wait for events */
	while (true)
	{
		// TODO Possibly fetch more than one event from epoll_wait().

		memset(&epev, 0, sizeof(epev));
		if (epoll_wait(epfd, &epev, 1, -1) < 0) {
			err = errno;
			log_perror(log, LOG_ERR, "epoll_wait()", err);
			goto fail;
		}

		switch ((err = _handle_event(ses, &epev))) {
		case 0:
			break;
		case EPIPE:
			goto done;
		default:
			log_perror(log, LOG_ERR, "_handle_event()", err);
			goto fail;
		}
	}
done:
	_debug(ses, "check-out");
	_revoke_session(ses);
	close(epfd);
	ses->ses_epfd = -1;
	return 0;

fail:
	_debug(ses, "check-out (with errors)");
	_revoke_session(ses);
fail_publish_session:
fail_epoll_add_server:
	close(epfd);
	ses->ses_epfd = -1;
fail_epoll_create:
	log_assert(log, err);
	return err;
}

int
switchd_serve_server(struct sosw_ses * const ses)
{
	log_t * const log = ses->ses_log;
	int     err;

	log_entering(log);

	if ((err = _accept(ses))) {
		log_perror(log, LOG_TRACE, "_accept()", err);
		/* (fall through) */
	}

	log_leaving(log);

	return err;
}
