/*
 * switchd-serve.c
 *
 *	Handle peer connection.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <ctype.h>

#include "switchd.h"     // private header

static int
switchd_handshake(struct sosw_ses * const ses)
{
	log_t      * const log  = ses->ses_log;
	tcp_conn_t * const conn = ses->ses_conn;
	void       * buf;
	size_t       size;
	char         c;
	char const * s;
	char       * t;
	char       * dupid;
	int          err;

	// TODO
	//	Revisit TCP keepalive parameters.

	/* TCP keepalive probes. Notice that we start the keepalive timer a
	 * few seconds after the client should have sent its first probe, so
	 * that if everything goes well we will have no need to probe the
	 * connection.
	 */
	tcp_keepalive(conn, 15,  /* start delay (sec)      */
		            30,  /* probes interval (sec)  */
			    1);  /* number of probes       */

	/* receive registration messages from SoloBox */
	if ((err = tcp_recva(conn, &buf, &size))) {
		log_perror(log, LOG_TRACE, "tcp_recva()", err);
		goto fail_0;
	}

	/* validate size of registration message */
	if (size <= sizeof(struct solo_m_register))
		goto invalid;
	struct solo_m_register const * const req = buf;
	size_t const regsize = sizeof(*req) + req->solo_q_reg_idlenz;
	if (size != regsize)
		goto invalid;
	/* validate magic value and version number */
	if (req->solo_q_reg_magic != SOLO_MAGIC_REGISTER)
		goto invalid;
	/* validate ASCII identifier */
	char const * const id = req->solo_q_reg_id;
	size_t const idlenz = req->solo_q_reg_idlenz;
	if (idlenz <= 1)
		goto invalid;   // empty identifier
	if (id[idlenz - 1])
		goto invalid;   // not a valid C string
	for (s = id; (c = *s) && isascii(c) && isprint(c) && !isspace(c); s++)
		continue;
	if (*s)
		goto invalid;   // not an ASCII string
	/* validate peer connection type and protocol type */
	if (   req->solo_q_reg_peer != SOLO_PEER_SERVER
	    && req->solo_q_reg_peer != SOLO_PEER_CLIENT)
		goto invalid;
	/* validate flags */
	if (req->solo_q_reg_flags)
		goto invalid;

	/* duplicate and normalize server identifier string */
	if ((err = mm_dup(log, id, &dupid, idlenz)))
		goto fail;
	for (t = dupid; (c = *t); t++)
		if (isalpha(c)) *t = tolower(c);

	/* update session descriptor */
	ses->ses_svid  = dupid;
	ses->ses_peer  = req->solo_q_reg_peer;
	ses->ses_proto = req->solo_q_reg_proto;

	/* cleanup */
	free(buf);
	return 0;

invalid:
	log_printf(log, LOG_ERR, "invalid handshake");
	err = EPROTO;
	goto fail;

fail:	free(buf);
fail_0:	log_assert(log, err);
	return err;
}

static int
switchd_serve(struct sosw_ses * const ses)
{
	log_t * const log  = ses->ses_log;
	int     err;

	/* perform handshake with peer to determine whether the peer is a
	 * server or client */
	if ((err = switchd_handshake(ses))) {
		log_perror(log, LOG_TRACE, "switchd_handshake()", err);
		return err;
	}

	switch (ses->ses_peer) {
	case SOLO_PEER_SERVER:
		err = switchd_serve_server(ses);
		break;
	case SOLO_PEER_CLIENT:
		err = switchd_serve_client(ses);
		break;
	default:
		log_unexpected(log);
		err = EPROTO;
		break;
	}

	return err;
}

static void
switchd_serve__atexit(struct sosw_ses * const ses)
{
	struct sosw * const sw = ses->ses_sw;
	log_t * const log = ses->ses_log;
	Pthread_mutex_lock(&sw->sw_sesmux);
	log_assert(log, ses->ses_pxysrv == NULL);
	log_assert(log, ses->ses_epfd == -1);
	/* remove from session list */
	list_del(&ses->ses_node);
	/* notify main thread if requested */
	if (list_empty(&sw->sw_seslist) && sw->sw_seswait)
		Pthread_cond_signal(&sw->sw_sescond);
	/* clean up */
	if (ses->ses_svid)
		mm_free(log, ses->ses_svid);
	tcp_close(ses->ses_conn);
	mm_free(log, ses);
	Pthread_mutex_unlock(&sw->sw_sesmux);
}

void *
switchd_serve__thread(void * const arg)
{
	struct sosw_ses * const ses = arg;
	Pthread_detach(pthread_self());
	switchd_serve(ses);
	switchd_serve__atexit(ses);
	pthread_exit(NULL);
}
