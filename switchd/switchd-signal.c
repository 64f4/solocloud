/*
 * switchd-signal.c
 *
 *	Signal handling.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <signal.h>

#include "switchd.h"  // private header

/**
 * Signal handling.
 */
static void
switchd_sigwait(struct sosw * const sw)
{
	log_t    * const log = sw->sw_log;
	sigset_t   sigset;
	int        signo;

	sigfillset(&sigset);
	while (!sigwait(&sigset, &signo))
	{
		char const * s = NULL;
		switch(signo) {
		case SIGINT:
			s = s ? : "SIGINT";
		case SIGQUIT:
			s = s ? : "SIGQUIT";
		case SIGTERM:
			s = s ? : "SIGTERM";
			log_printf(log, LOG_DEBUG, "received %s", s);
			tcp_shutdown(sw->sw_srv);
			return;
		case SIGWINCH:  /* window-size change (ignore) */
			break;
		default:
			log_printf(log, LOG_DEBUG, "ignoring SIG%d", signo);
		}
	}

	int const err = errno;
	log_perror(log, LOG_CRIT, "sigwait()", err);
	log_bug(log);
}

/**
 * Signal handling thread.
 */
void *
switchd_sigwait__thread(void * const arg)
{
	struct sosw * const sw = arg;
	switchd_sigwait(sw);
	pthread_exit(NULL);
}
