/*
 * switchd.h
 *
 *	Private header for the Solo Switchboard daemon.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_SWITCHD_H
#define SOLOCLOUD_SWITCHD_H

/* System headers */
#include <errno.h>
#include <stdbool.h>
#include <string.h>

/* Project headers */
#include "list.h"         // linked list
#include "log.h"          // logging facilities
#include "malloc.h"       // malloc() wrappers
#include "safepthread.h"  // safe pthread wrappers
#include "tcp.h"          // TCP communication library
#include "solo-proto.h"   // SoloCloud communication protocol

/* Listening TCP/IP port number (cf_portno) */
#define SWITCHD_PORTNO 29909

/* Timeout (in milliseconds) for a proxy connection to be established */
#define SWITCHD_PROXY_TIMEOUT 10000

/* Daemon configuration.
 */
struct sosw_cf
{
	short unsigned    cf_portno;   // TCP port number
	int               cf_backlog;  // TCP connection backlog
};

/* Maps a server ID to a session.
 */
struct sosw_map
{
	list_t            map_node;
	char const      * map_id;
	enum solo_proto   map_proto;
	struct sosw_ses * map_ses;
};

/* Server or client session.
 */
struct sosw_ses
{
	list_t            ses_node;    // node in the session list
	unsigned          ses_no;      // unique session number
	struct sosw     * ses_sw;      // daemon instance
	log_t           * ses_log;     // session log stream
	tcp_conn_t      * ses_conn;    // peer connection
	char            * ses_svid;    // server identifier
	enum solo_peer    ses_peer;    // connection type
	enum solo_proto   ses_proto;   // connection protocol

	/* Server session fields.
	 */
	struct sosw_map   ses_map;     // entry in the ID map
	bool              ses_accept;  // server is ready to accept a client
	bool volatile     ses_down;    // server session is shutting down
	int               ses_epfd;    // epoll() vector of pending clients
	pthread_mutex_t   ses_climux;  // protects the server fields below
	list_t            ses_cliwait; // list of pending clients
	unsigned volatile ses_clinum;  // number of pending clients in list
	pthread_cond_t    ses_cliend;  // signals the end of pending clients

	/* Client session fields. These fields are governed by the server
	 * session mutex (`ses_climux') to which the client is attempting a
	 * connection.
	 */
	list_t            ses_clinode; // node in server's pending client list
	pthread_cond_t    ses_pxycond; // signals proxy connection ready
	tcp_server_t    * ses_pxysrv;  // proxy server socket
	bool              ses_pxybail; // proxy request was aborted
};

/* Daemon instance.
 */
struct sosw
{
	struct sosw_cf    sw_cf;       // confguration
	log_t           * sw_log;      // log stream
	tcp_server_t    * sw_srv;      // listening server socket

	/* Sessions
	 */
	list_t            sw_seslist;  // session list (struct sosw_ses)
	pthread_mutex_t   sw_sesmux;   // session list mutex
	bool              sw_seswait;  // set when waiting for condition below
	pthread_cond_t    sw_sescond;  // signals that all sessions have quit

	/* Server ID to session map.
	 *
	 * FIXME !
	 *	The "map" is currently implemented as a linked list.
	 *	Obviously, the map should be a hash table.
	 */
	pthread_mutex_t   sw_mapmux;
	list_t            sw_maplist;

};

/* Print messages to standard error during initialization.
 */
#define SWITCHD_PREFIX "switchd"
#define switchd_fatal(fmt, ...) \
   fprintf(stderr, SWITCHD_PREFIX ": fatal: " fmt "\n", ## __VA_ARGS__)
#define switchd_warning(fmt, ...) \
   fprintf(stderr, SWITCHD_PREFIX ": warning: " fmt "\n", ## __VA_ARGS__)
#define switchd_info(fmt, ...) \
   fprintf(stderr, SWITCHD_PREFIX ": " fmt "\n", ## __VA_ARGS__)

/*
 * Private functions.
 */

/* switchd-listen.c */

extern int    switchd_listen(struct sosw *);

/* switchd-serve.c */

extern void * switchd_serve__thread(void *);

/* switchd-serve-server.c */

extern int    switchd_serve_server(struct sosw_ses *);

/* switchd-serve-client.c */

extern int    switchd_serve_client(struct sosw_ses *);

/* switchd-signal.c */

extern void * switchd_sigwait__thread(void *);

#endif
