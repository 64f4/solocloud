/*
 * switchd-listen.c
 *
 *	Handles incoming connections.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include "switchd.h"  // private header

int switchd_listen(struct sosw * const sw)
{
	log_t        * const log = sw->sw_log;
	tcp_server_t * const srv = sw->sw_srv;
	tcp_conn_t   * conn;
	int            err;

	/* session numbers */
	static unsigned sesno = 0;

	log_entering(log);

	/* accept connection */
	while ( !(err = tcp_accept(srv, &conn)) )
	{
		struct sosw_ses * ses;
		pthread_t         id;

		/* allocate session for the new connection */
		if ((err = mm_malloc_t(log, &ses, struct sosw_ses)))
			continue;
		*ses = (struct sosw_ses) {
			.ses_no   = __sync_add_and_fetch(&sesno, 1),
			.ses_sw   = sw,
			.ses_log  = log,
			.ses_conn = conn,
			.ses_epfd = -1,
		};

		/* add to session list */
		Pthread_mutex_lock(&sw->sw_sesmux);
		list_add_tail(&ses->ses_node, &sw->sw_seslist);
		Pthread_mutex_unlock(&sw->sw_sesmux);

		/* fork a thread to deal with this connection */
		void * (* const thread)(void *) = switchd_serve__thread;
		if ((err = pthread_create(&id, NULL, thread, ses))) {
			log_perror(log, LOG_ERR, "pthread_create()", err);
			/* remove from session list */
			Pthread_mutex_lock(&sw->sw_sesmux);
			list_del(&ses->ses_node);
			Pthread_mutex_unlock(&sw->sw_sesmux);
			tcp_close(conn);
			mm_free(log, ses);
			continue;
		}
	}

	/* report tcp_accept() error if applicable */
	if (err == ESHUTDOWN)  // proper shutdown
		err = 0;
	else if (err)          // abnormal shutdown
		log_perror(log, LOG_TRACE, "tcp_accept()", err);

	/* drop all sessions */
	log_printf(log, LOG_INFO, "dropping sessions");
	Pthread_mutex_lock(&sw->sw_sesmux);
	list_t * it, * next;
	list_for_each_safe(it, next, &sw->sw_seslist) {
		struct sosw_ses * const ses =
			list_entry(it, struct sosw_ses, ses_node);
		log_printf(log, LOG_DEBUG, "dropping SES%u", ses->ses_no);
		tcp_drop(ses->ses_conn);
	}
	/* wait for the current sessions to quit */
	log_printf(log, LOG_DEBUG, "waiting for sessions");
	sw->sw_seswait = true;
	while (!list_empty(&sw->sw_seslist))
		Pthread_cond_wait(&sw->sw_sescond, &sw->sw_sesmux);
	sw->sw_seswait = false;
	Pthread_mutex_unlock(&sw->sw_sesmux);

	log_leaving(log);

	return err;
}
