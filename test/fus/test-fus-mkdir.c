/*
 * test-fus-mkdir.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "test-fus.h"

void test_fus_mkdir(struct test_fus_ctx * const ctx)
{
	solo_conn_t * const conn = ctx->tfus_conn;
	struct stat   st;
	int           fd;

	TEST_ENTER(ctx);

	/* Invalid arguments (checked on the client)
	 */
	TEST_ERRNO   (ctx, EFAULT,  fus_mkdir(conn, NULL, 0));

	/* Invalid arguments (checked on the server)
	 */
	TEST_ERRNO   (ctx, EINVAL,  fus_mkdir(conn, "..", 0));
	TEST_ERRNO   (ctx, EINVAL,  fus_mkdir(conn, "../..", 0));
	TEST_ERRNO   (ctx, EINVAL,  fus_mkdir(conn, "../foo", 0));
	TEST_ERRNO   (ctx, EINVAL,  fus_mkdir(conn, "./../foo", 0));

	/* Somehow questionable mkdir() operations
	 */
	TEST_ERRNO   (ctx, EEXIST,  fus_mkdir(conn, "/", 0));
	TEST_ERRNO   (ctx, EEXIST,  fus_mkdir(conn, ".", 0));

	/* Creating too many directories at once */
	TEST_ERRNO   (ctx, ENOENT,  fus_mkdir(conn, "foo/bar", 0));

	/* Again, but one at the time */
	TEST_ERRNO   (ctx, 0,       fus_mkdir(conn, "foo", 0));
	TEST_ERRNO   (ctx, 0,       fus_mkdir(conn, "foo/bar", 0));
	/* Directory already exists */
	TEST_ERRNO   (ctx, EEXIST,  fus_mkdir(conn, "foo/bar", 0));

	/* Check stat() mode of new directory.
	 */
	TEST_ERRNO_J (fail, ctx, 0, fus_stat(conn, "foo", &st));
	TEST_COND_J  (fail, ctx, S_ISDIR(st.st_mode));

	/* create a temporary file */
	int const flags = O_CREAT | O_EXCL;
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, "foo.tmp", flags, 0, &fd));
	/* attempt to create a directory with the same name */
	TEST_ERRNO   (ctx, EEXIST,  fus_mkdir(conn, "foo.tmp", 0));
	/* attempt to treat file as a directory */
	TEST_ERRNO   (ctx, ENOTDIR, fus_mkdir(conn, "foo.tmp/bar", 0));
	/* close temporary file */
	TEST_ERRNO   (ctx, 0,       fus_close(conn, fd));

	/* cleanup */
fail:	TEST_ERRNO   (ctx, 0, fus_unlink(conn, "foo.tmp"));
	TEST_ERRNO   (ctx, 0, fus_rmdir(conn, "foo/bar"));
	TEST_ERRNO   (ctx, 0, fus_rmdir(conn, "foo"));

	TEST_LEAVE(ctx);
}
