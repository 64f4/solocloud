/*
 * test-fus-rmdir.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "test-fus.h"

void
test_fus_rmdir(struct test_fus_ctx * const ctx)
{
	solo_conn_t * const conn = ctx->tfus_conn;
	int           fd;

	TEST_ENTER(ctx);

	TEST_ERRNO (ctx, EFAULT, fus_rmdir(conn, NULL));
	TEST_ERRNO (ctx, EINVAL, fus_rmdir(conn, ""));
	TEST_ERRNO (ctx, EPERM,  fus_rmdir(conn, "."));
	TEST_ERRNO (ctx, EPERM,  fus_rmdir(conn, "/"));

	/* Attempt to remove a regular file.
	 */
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, "a", O_CREAT, 0, &fd));
	TEST_ERRNO_J (fail, ctx, 0, fus_close(conn, fd));
	TEST_ERRNO_J (fail, ctx, ENOTDIR, fus_rmdir(conn, "a"));

	/* Attempt to remove a non-empty directory.
	 */
	TEST_ERRNO_J (fail, ctx, 0, fus_mkdir(conn, "d", 0));
	TEST_ERRNO_J (fail, ctx, 0, fus_rename(conn, "a", "d/a"));
	TEST_ERRNO_J (fail, ctx, ENOTEMPTY, fus_rmdir(conn, "d"));

	/* Delete directory.
	 */
	TEST_ERRNO_J (fail, ctx, 0, fus_unlink(conn, "d/a"));
	TEST_ERRNO_J (fail, ctx, 0, fus_rmdir(conn, "d"));

fail:	fus_unlink(conn, "d/a");
	fus_rmdir (conn, "d");
	fus_unlink(conn, "a");
	TEST_LEAVE(ctx);
}
