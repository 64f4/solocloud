/*
 * test-fus-open.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "test-fus.h"

void test_fus_open(struct test_fus_ctx * const ctx)
{
	solo_conn_t * const conn  = ctx->tfus_conn;
	char          const str[] = "Hello world!\n";
	size_t        const len   = sizeof(str) - 1;
	char          buf[64];
	struct stat   st;
	size_t        nw;
	size_t        nr;
	int           fl;
	int           fd;

	TEST_ENTER(ctx);

	/* Invalid arguments (checked on the client)
	 */
	TEST_ERRNO   (ctx, EFAULT,  fus_open(conn, NULL, 0, 0, &fd));
	TEST_ERRNO   (ctx, EFAULT,  fus_open(conn, "x", 0, 0, NULL));

	/* Invalid path
	 */
	TEST_ERRNO   (ctx, EINVAL,  fus_open(conn, "../foo", 0, 0, &fd));
	TEST_ERRNO   (ctx, EINVAL,  fus_open(conn, "foo/../bar", 0, 0, &fd));
	TEST_ERRNO   (ctx, EINVAL,  fus_open(conn, "..", 0, 0, &fd));
	TEST_ERRNO   (ctx, EINVAL,  fus_open(conn, "/..", 0, 0, &fd));

	// XXX
	//	fus_open() can open directories in read-only mode, just like
	//	the open() system call on Linux. There does not appear to be
	//	any open() flag to prevent this behavior, so for the time
	//	being I will leave it as is. The only problem I see is that by
	//	reading a directory entry directly, a user may become aware of
	//	files that are hidden in READDIR requests.
	//

	/* We can't open a directory for writing.
	 . */
	TEST_ERRNO (ctx, EISDIR, fus_open(conn, ".", O_WRONLY, 0, &fd));
	TEST_ERRNO (ctx, EISDIR, fus_open(conn, "/", O_WRONLY, 0, &fd));

	/* Create file, open for writing only.
	 */
	fl = O_CREAT | O_EXCL | O_WRONLY;
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, "open.tmp", fl, 0, &fd));
	TEST_ERRNO_J (fail, ctx, 0, fus_stat(conn, "open.tmp", &st));
	/* check file mode and permissions */
	TEST_COND_J  (fail, ctx, S_ISREG(st.st_mode));
	TEST_COND_J  (fail, ctx, st.st_mode & (S_IRUSR | S_IWUSR));
	/* write something to the file */
	nw = len;
	TEST_ERRNO_J (fail, ctx, 0, fus_pwrite(conn, fd, str, &nw, 0));
	TEST_COND_J  (fail, ctx, nw == len);
	/* read something from the file, it should fail (O_WRONLY) */
	nr = sizeof(buf);
	TEST_ERRNO_J (fail, ctx, EBADF, fus_pread(conn, fd, buf, &nr, 0));
	/* close file */
	TEST_ERRNO_J (fail, ctx, 0, fus_close(conn, fd));

	/* Create same file again, but O_EXCL should make open() fail.
	 */
	fl = O_CREAT | O_EXCL | O_WRONLY;
	TEST_ERRNO_J (fail, ctx, EEXIST, fus_open(conn, "open.tmp", fl, 0, &fd));

	/* Open same file for reading only.
	 */
	fl = O_RDONLY;
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, "open.tmp", fl, 0, &fd));
	/* read from file */
	nr = sizeof(buf);
	memset(buf, 0, sizeof(buf));
	TEST_ERRNO_J (fail, ctx, 0, fus_pread(conn, fd, buf, &nr, 0));
	TEST_COND_J  (fail, ctx, nr == len);
	TEST_COND_J  (fail, ctx, !strncmp(str, buf, len));
	/* write to file (should fail, O_RDONLY) */
	nw = len;
	TEST_ERRNO_J (fail, ctx, EBADF, fus_pwrite(conn, fd, str, &nw, 0));
	/* close file */
	TEST_ERRNO_J (fail, ctx, 0, fus_close(conn, fd));

	/* Open same file for read/write, and truncate.
	 */
	fl = O_TRUNC | O_RDWR;
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, "open.tmp", fl, 0, &fd));
	/* check file size (should be zero) */
	TEST_ERRNO_J (fail, ctx, 0, fus_stat(conn, "open.tmp", &st));
	TEST_COND_J  (fail, ctx, st.st_size == 0);
	/* write to file */
	nw = len;
	TEST_ERRNO_J (fail, ctx, 0, fus_pwrite(conn, fd, str, &nw, 0));
	TEST_COND_J  (fail, ctx, nw == len);
	/* read from file */
	nr = sizeof(buf);
	TEST_ERRNO_J (fail, ctx, 0, fus_pread(conn, fd, buf, &nr, 0));
	TEST_COND_J  (fail, ctx, nr == len);
	TEST_COND_J  (fail, ctx, !strncmp(str, buf, len));
	/* close file */
	TEST_ERRNO_J (fail, ctx, 0, fus_close(conn, fd));

	// XXX
	//	O_APPEND is not tested, because it can't be tested with only
	//	the pread() and pwrite() system calls.

fail:	TEST_ERRNO   (ctx, 0, fus_unlink(conn, "open.tmp"));
	TEST_LEAVE(ctx);
}
