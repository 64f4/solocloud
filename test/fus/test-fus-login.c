/*
 * test-fus-login.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "test-fus.h"

void test_fus_login(struct test_fus_ctx * const ctx)
{
	solo_conn_t * const conn = ctx->tfus_conn;
	char        * pass;
	char          buf[64];

	TEST_ENTER(ctx);

	/* Bad passwords.
	 */
	TEST_ERRNO(ctx, EPERM, fus_login(conn, "x"));
	TEST_ERRNO(ctx, EPERM, fus_login(conn, "foo"));

	/* Correct password. The default password (for testing) is an empty
	 * string. If this doesn't work, try asking for a different password.
	 */
	pass = getenv("SOLO_PASS") ? : "";
	if (fus_login(conn, pass)) {
		pass = getpass("Password: ");
		TEST_ERRNO_J(fail, ctx, 0, fus_login(conn, pass));
	}

	/* The correct password, with additional newline characters which
	 * should be ignored.
	 */
	snprintf(buf, sizeof(buf), "%s\n", pass);
	TEST_ERRNO(ctx, 0, fus_login(conn, buf));
	snprintf(buf, sizeof(buf), "%s\r\n", pass);
	TEST_ERRNO(ctx, 0, fus_login(conn, buf));
	snprintf(buf, sizeof(buf), "%s\n\r", pass);
	TEST_ERRNO(ctx, 0, fus_login(conn, buf));
	snprintf(buf, sizeof(buf), "%s\n\r\n\n", pass);
	TEST_ERRNO(ctx, 0, fus_login(conn, buf));

fail:	TEST_LEAVE(ctx);
}
