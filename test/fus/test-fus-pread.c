/*
 * test-fus-pread.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "test-fus.h"

void test_fus_pread(struct test_fus_ctx * const ctx)
{
	solo_conn_t * const conn  = ctx->tfus_conn;
	char          const str0[] = "Hello world!\n";
	size_t        const len0   = sizeof(str0) - 1;
	char          const str1[] = "This is a test.\n";
	size_t        const len1   = sizeof(str1) - 1;
	char          buf[64];
	size_t        nw;
	size_t        nr;
	struct stat   st;
	int           fl;
	int           fd;

	TEST_ENTER(ctx);

	/* Read/write from bad file descriptors.
	 */

	memset(buf, 0, sizeof(buf));

	nr = sizeof(buf);
	TEST_ERRNO (ctx, EBADF,  fus_pread(conn, 0, buf, &nr, 0));
	TEST_ERRNO (ctx, EBADF,  fus_pread(conn, 1, buf, &nr, 0));
	TEST_ERRNO (ctx, EBADF,  fus_pread(conn, 2, buf, &nr, 0));
	TEST_ERRNO (ctx, EBADF,  fus_pread(conn, INT32_MAX, buf, &nr, 0));
	TEST_ERRNO (ctx, EINVAL, fus_pread(conn, -1, buf, &nr, 0));
	TEST_ERRNO (ctx, EINVAL, fus_pread(conn, 0, buf, &nr, -1));

	nw = sizeof(buf);
	TEST_ERRNO (ctx, EBADF,  fus_pwrite(conn, 0, buf, &nw, 0));
	TEST_ERRNO (ctx, EBADF,  fus_pwrite(conn, 1, buf, &nw, 0));
	TEST_ERRNO (ctx, EBADF,  fus_pwrite(conn, 2, buf, &nw, 0));
	TEST_ERRNO (ctx, EBADF,  fus_pwrite(conn, INT32_MAX, buf, &nw, 0));
	TEST_ERRNO (ctx, EINVAL, fus_pwrite(conn, -1, buf, &nw, 0));
	TEST_ERRNO (ctx, EINVAL, fus_pwrite(conn, 0, buf, &nw, -1));

	/* Create file, open for reading and writing.
	 */

	fl = O_CREAT | O_EXCL | O_RDWR;
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, "rw.tmp", fl, 0, &fd));

	/* write to file at offset 0 */
	nw = len0;
	TEST_ERRNO_J (fail, ctx, 0, fus_pwrite(conn, fd, str0, &nw, 0));
	TEST_COND_J  (fail, ctx, nw == len0);
	TEST_ERRNO_J (fail, ctx, 0, fus_stat(conn, "rw.tmp", &st));
	TEST_COND_J  (fail, ctx, (size_t) st.st_size == len0);

	/* write to file at offset 64 */
	nw = len1;
	TEST_ERRNO_J (fail, ctx, 0, fus_pwrite(conn, fd, str1, &nw, 64));
	TEST_COND_J  (fail, ctx, nw == len1);
	TEST_ERRNO_J (fail, ctx, 0, fus_stat(conn, "rw.tmp", &st));
	TEST_COND_J  (fail, ctx, st.st_size == 64 + len1);

	/* read from file at offest 0 */
	nr = len0;
	memset(buf, 0, sizeof(buf));
	TEST_ERRNO_J (fail, ctx, 0, fus_pread(conn, fd, buf, &nr, 0));
	TEST_COND_J  (fail, ctx, nr == len0);
	TEST_COND_J  (fail, ctx, !strncmp(buf, str0, len0));

	/* read from file at offest 64 */
	nr = len1;
	memset(buf, 0, sizeof(buf));
	TEST_ERRNO_J (fail, ctx, 0, fus_pread(conn, fd, buf, &nr, 64));
	TEST_COND_J  (fail, ctx, nr == len1);
	TEST_COND_J  (fail, ctx, !strncmp(buf, str1, len1));

	/* read from file at offest 64, again, but using a bigger count */
	nr = sizeof(buf);
	memset(buf, 0, sizeof(buf));
	TEST_ERRNO_J (fail, ctx, 0, fus_pread(conn, fd, buf, &nr, 64));
	TEST_COND_J  (fail, ctx, nr == len1);
	TEST_COND_J  (fail, ctx, !strncmp(buf, str1, len1));

	/* read from file at offest 128, there should be nothing there */
	nr = sizeof(buf);
	memset(buf, 0, sizeof(buf));
	TEST_ERRNO_J (fail, ctx, 0, fus_pread(conn, fd, buf, &nr, 128));
	TEST_COND_J  (fail, ctx, nr == 0);

	/* check the file size once more */
	TEST_ERRNO_J (fail, ctx, 0, fus_stat(conn, "rw.tmp", &st));
	TEST_COND_J  (fail, ctx, st.st_size == 64 + len1);

	/* close file and delete */
	TEST_ERRNO_J (fail, ctx, 0, fus_close(conn, fd));
fail:	TEST_ERRNO   (ctx, 0, fus_unlink(conn, "rw.tmp"));
	TEST_LEAVE(ctx);
}
