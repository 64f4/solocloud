/*
 * test-fus-close.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "test-fus.h"

void test_fus_close(struct test_fus_ctx * const ctx)
{
	solo_conn_t * const conn = ctx->tfus_conn;

	TEST_ENTER(ctx);

	/* Issue CLOSE with a file descriptor that may exist on the server,
	 * but which does not belong to our session.
	 */
	TEST_ERRNO(ctx, EBADF,  fus_close(conn, STDIN_FILENO));
	TEST_ERRNO(ctx, EBADF,  fus_close(conn, STDOUT_FILENO));
	TEST_ERRNO(ctx, EBADF,  fus_close(conn, STDERR_FILENO));

	/* Issue CLOSE with invalid file descriptors.
	 */
	TEST_ERRNO(ctx, EINVAL, fus_close(conn, -1));
	TEST_ERRNO(ctx, EINVAL, fus_close(conn, INT16_MIN));
	TEST_ERRNO(ctx, EBADF,  fus_close(conn, INT16_MAX));
	TEST_ERRNO(ctx, EINVAL, fus_close(conn, INT32_MIN));
	TEST_ERRNO(ctx, EBADF,  fus_close(conn, INT32_MAX));
	TEST_ERRNO(ctx, EBADF,  fus_close(conn, UINT16_MAX));
	TEST_ERRNO(ctx, EINVAL, fus_close(conn, UINT32_MAX));

	TEST_LEAVE(ctx);
}
