/*
 * test-fus-rename.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "test-fus.h"

void
test_fus_rename(struct test_fus_ctx * const ctx)
{
	solo_conn_t * const conn = ctx->tfus_conn;
	int           fd;

	TEST_ENTER(ctx);

	TEST_ERRNO (ctx, EFAULT, fus_rename(conn, NULL, NULL));
	TEST_ERRNO (ctx, EFAULT, fus_rename(conn, "a", NULL));
	TEST_ERRNO (ctx, EFAULT, fus_rename(conn, NULL, "a"));

	/* Invalid renames.
	 */
	TEST_ERRNO (ctx, EINVAL, fus_rename(conn, ".", "."));
	TEST_ERRNO (ctx, EINVAL, fus_rename(conn, ".", "x"));
	TEST_ERRNO (ctx, EINVAL, fus_rename(conn, ".", "/"));
	TEST_ERRNO (ctx, EINVAL, fus_rename(conn, "/", "x"));
	TEST_ERRNO (ctx, EINVAL, fus_rename(conn, ".", ".."));
	TEST_ERRNO (ctx, EINVAL, fus_rename(conn, "a", ""));
	TEST_ERRNO (ctx, EINVAL, fus_rename(conn, "", "a"));

	/* Rename file.
	 */
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, "a", O_CREAT, 0, &fd));
	TEST_ERRNO_J (fail, ctx, 0, fus_close(conn, fd));
	TEST_ERRNO_J (fail, ctx, 0, fus_rename(conn, "a", "b"));

	/* Rename to invalid path.
	 */
	TEST_ERRNO_J (fail, ctx, ENOENT, fus_rename(conn, "b", "r/b"));

	/* Rename file to file.
	 */
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, "a", O_CREAT, 0, &fd));
	TEST_ERRNO_J (fail, ctx, 0, fus_close(conn, fd));
	TEST_ERRNO_J (fail, ctx, 0, fus_rename(conn, "b", "a"));

	/* Rename file to directory (INVALID)
	 */
	TEST_ERRNO_J (fail, ctx, 0, fus_mkdir(conn, "r", 0));
	TEST_ERRNO_J (fail, ctx, EISDIR, fus_rename(conn, "a", "r"));

	/* Move file to a directory */
	TEST_ERRNO_J (fail, ctx, 0, fus_rename(conn, "a", "r/a"));
	
	/* Rename directory to directory */
	TEST_ERRNO_J (fail, ctx, 0, fus_rename(conn, "r", "d"));
	TEST_ERRNO_J (fail, ctx, EINVAL, fus_rename(conn, "d", "d/x"));

fail:	fus_unlink(conn, "d/a");
	fus_rmdir (conn, "d"  );
	fus_unlink(conn, "r/a");
	fus_rmdir (conn, "r"  );
	fus_unlink(conn, "a"  );
	fus_unlink(conn, "b"  );

	TEST_LEAVE(ctx);
}
