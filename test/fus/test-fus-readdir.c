/*
 * test-fus-readdir.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "test-fus.h"

/* Size of the buffer where the directory listing will be stored */
#define BUFSIZE 4096

struct fillctx
{
	char * s;   // start of buffer
	char * e;   // end of buffer
};

static int
dumb_fill()
{
	return 0;
}

static int
bad_fill()
{
	return ENOMEM;
}

static int
fill(
	void              * const arg,   // arbitrary data
	char const        * const name,  // entry name
	char const        * const path,  // entry full path
	struct stat const * const st     // entry stat() result
	)
{
	struct fillctx    * const ctx = arg;
	int                 const m   = st->st_mode;
	void const        * const e   = ctx->e;
	void              *       s   = ctx->s;

	if (s < e) {
		s += snprintf(s, e - s, "%s %s %zu %c%c%c%c\n",
			      name,
			      path,
			      (size_t) st->st_size,
			      m & S_IRUSR ? 'r' : '-',
			      m & S_IWUSR ? 'w' : '-',
			      m & S_IXUSR ? 'x' : '-',
			      S_ISDIR(m)  ? 'd' : '-');
		ctx->s = s;
	}

	return 0;
}

static int
cmp(void const * a, void const * b)
{
	char const * const * const l = a;
	char const * const * const r = b;
	return strcmp(*l, *r);
}

static void
sort(char * src, char * dst, size_t size)
{
	char * v[64];
	char * s;
	char * e;
	int    n;
	int    i;

	for (s = src, n = 0; *s; s++, n++) {
		v[n] = s;
		s = strchr(s, '\n');
		*s = '\0';
	}

	qsort(v, n, sizeof(char *), cmp);

	for (i = 0, s = dst, e = dst + size; i < n; i++)
		if (s < e)
			s += snprintf(s, e - s, "%s\n", v[i]);
}

void
test_fus_readdir(struct test_fus_ctx * const ctx)
{
	solo_conn_t    * const conn = ctx->tfus_conn;
	char           * dirbuf;
	char           * lst;
	int              fl;
	int              fd;
	struct fillctx   fillctx;

	TEST_ENTER(ctx);

	dirbuf = calloc(1, BUFSIZE);
	lst = calloc(1, BUFSIZE);
	log_assert(ctx->tfus_log, dirbuf != NULL);
	log_assert(ctx->tfus_log, lst != NULL);

	/* Invalid arguments.
	 */

	TEST_ERRNO (ctx, EFAULT, fus_readdir(conn, NULL, NULL, NULL));
	TEST_ERRNO (ctx, EFAULT, fus_readdir(conn, "/",  NULL, NULL));
	TEST_ERRNO (ctx, EFAULT, fus_readdir(conn, NULL, NULL, dumb_fill));

	/* Invalid path.
	 */

	TEST_ERRNO (ctx, EINVAL, fus_readdir(conn, "..",  NULL, dumb_fill));
	TEST_ERRNO (ctx, EINVAL, fus_readdir(conn, "../..",  NULL, dumb_fill));

	/* Read root directory with a dumb filler.
	 */
	TEST_ERRNO (ctx, 0, fus_readdir(conn, ".",  NULL, dumb_fill));
	TEST_ERRNO (ctx, 0, fus_readdir(conn, "/",  NULL, dumb_fill));

	/* Create regular file and attemp readdir() on it.
	 */
	fl = O_CREAT | O_EXCL;
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, "dir.tmp", fl, 0, &fd));
	TEST_ERRNO_J (fail, ctx, 0, fus_close(conn, fd));
	TEST_ERRNO_J (fail, ctx, ENOTDIR,
		      fus_readdir(conn, "dir.tmp",  NULL, dumb_fill));

	/* Create and read empty directory.
	 */
	TEST_ERRNO_J (fail, ctx, 0, fus_mkdir(conn, "dir", 0));
	memset(dirbuf, 0, BUFSIZE);
	fillctx = (struct fillctx) { dirbuf, dirbuf + BUFSIZE };
	TEST_ERRNO_J (fail, ctx, 0, fus_readdir(conn, "dir", &fillctx, fill));
	TEST_COND    (ctx, strlen(dirbuf) == 0);

	/* Add empty regular file and read directory.
	 */
	fl = O_CREAT | O_EXCL;
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, "dir/a", fl, 0, &fd));
	TEST_ERRNO_J (fail, ctx, 0, fus_close(conn, fd));
	memset(dirbuf, 0, BUFSIZE);
	fillctx = (struct fillctx) { dirbuf, dirbuf + BUFSIZE };
	TEST_ERRNO_J (fail, ctx, 0, fus_readdir(conn, "dir", &fillctx, fill));
	TEST_COND_J  (fail, ctx, !strcmp(dirbuf, "a dir/a 0 rw--\n"));

	/* Add non-empty regular file and read directory.
	 */
	fl = O_CREAT | O_EXCL;
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, "dir/bb", fl, 0, &fd));
	TEST_ERRNO_J (fail, ctx, 0, fus_ftruncate(conn, fd, 16));
	TEST_ERRNO_J (fail, ctx, 0, fus_close(conn, fd));
	memset(dirbuf, 0, BUFSIZE);
	fillctx = (struct fillctx) { dirbuf, dirbuf + BUFSIZE };
	TEST_ERRNO_J (fail, ctx, 0, fus_readdir(conn, "dir", &fillctx, fill));
	sort(dirbuf, lst, BUFSIZE);
	TEST_COND_J  (fail, ctx, !strcmp(lst, "a dir/a 0 rw--\n"
					      "bb dir/bb 16 rw--\n"));

	/* Add subdirectory and read directory.
	 */
	TEST_ERRNO_J (fail, ctx, 0, fus_mkdir(conn, "dir/ccc", 0));
	memset(dirbuf, 0, BUFSIZE);
	fillctx = (struct fillctx) { dirbuf, dirbuf + BUFSIZE };
	TEST_ERRNO_J (fail, ctx, 0, fus_readdir(conn, "dir", &fillctx, fill));
	sort(dirbuf, lst, BUFSIZE);
	TEST_COND_J  (fail, ctx, !strcmp(lst, "a dir/a 0 rw--\n"
					      "bb dir/bb 16 rw--\n"
					      "ccc dir/ccc 4096 rwxd\n"));

	/* Check that filler errors are propagated.
	 */
	TEST_ERRNO_J (fail, ctx, ENOMEM,
		      fus_readdir(conn, "dir", NULL, bad_fill));

	/* Clean up.
	 */
fail:	TEST_ERRNO   (ctx, 0, fus_rmdir (conn, "dir/ccc"));
	TEST_ERRNO   (ctx, 0, fus_unlink(conn, "dir/bb"));
	TEST_ERRNO   (ctx, 0, fus_unlink(conn, "dir/a"));
	TEST_ERRNO   (ctx, 0, fus_rmdir (conn, "dir"));
	TEST_ERRNO   (ctx, 0, fus_unlink(conn, "dir.tmp"));
	free(dirbuf);
	free(lst);

	TEST_LEAVE(ctx);
}
