/*
 * test-fus-main.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* System headers */
#include <sysexits.h>

/* Private header */
#include "test-fus.h"

int main()
{
	log_t        * log;
	solo_t       * solo;
	solo_conn_t  * conn;
	int            err;
	int            ret;

	// FIXME
	//	We are currentl using the following environment variables:
	//
	//	SOLO_SERVER  Switchboard server name (default: unspecified)
	//
	//	SOLO_BOX     Box name (default: "test-box")
	//

	srand(time(NULL));

	/* open log stream */
	if ((err = log_open_f(stderr, &log)))
		goto fail_log;

	/* Connect to SoloCloud Box
	 */
	struct solo_cf const solocf = {
		.socf_server = getenv("SOLO_SERVER"),
	};
	if ((err = solo_init(log, &solocf, &solo))) {
		log_perror(log, LOG_TRACE, "solo_init()", err);
		goto fail_solo;
	}
	char const * const boxid = getenv("SOLO_BOX") ? : "test-box";
	if ((err = solo_connect(solo, boxid, SOLO_PROTO_FUS, &conn))) {
		log_perror(log, LOG_TRACE, "solo_connect()", err);
		goto fail_connect;
	}

	/* runtime context */
	struct test_fus_ctx ctx = { log, conn, 0 };

	/* Test LOGIN operation first, otherwise there is no point continuing.
	 */
	test_fus_login(&ctx);
	if (ctx.tfus_numerr)
		goto fail_login;

	/* Run unit tests.
	 */
	test_fus_close(&ctx);
	test_fus_ftruncate(&ctx);
	test_fus_mkdir(&ctx);
	test_fus_open(&ctx);
	test_fus_pread(&ctx);
	test_fus_readdir(&ctx);
	test_fus_rename(&ctx);
	test_fus_rmdir(&ctx);
	test_fus_unlink(&ctx);

	/* Report number of errors.
	 */
	unsigned const numerr = ctx.tfus_numerr;
	if (numerr == 0) {
		printf("test-fus: success\n");
		ret = EX_OK;
	}
	else {
		printf("test-fus: %s%u error%s%s\n",
			RED, ctx.tfus_numerr, numerr == 1 ? "" : "s", RST);
		ret = EX_SOFTWARE;
	}

	/* cleanup */
	solo_close(conn);
	solo_uninit(solo);
	log_close(log);
	return ret;

fail_login:
	log_printf(log, LOG_ERR, "cannot login");
	solo_close(conn);
fail_connect:
	solo_uninit(solo);
fail_solo:
	log_printf(log, LOG_ERR, "exiting (with errors)");
	log_close(log);
fail_log:
	printf("test-fus: %scannot run tests%s\n", RED, RST);
	return EX_OSERR;
}
