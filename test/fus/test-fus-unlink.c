/*
 * test-fus-unlink.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "test-fus.h"

void
test_fus_unlink(struct test_fus_ctx * const ctx)
{
	solo_conn_t * const conn = ctx->tfus_conn;

	TEST_ENTER(ctx);

	TEST_ERRNO (ctx, EFAULT, fus_unlink(conn, NULL));
	TEST_ERRNO (ctx, EINVAL, fus_unlink(conn, ""));
	TEST_ERRNO (ctx, EISDIR, fus_unlink(conn, "."));
	TEST_ERRNO (ctx, EISDIR, fus_unlink(conn, "/"));
	TEST_ERRNO (ctx, EINVAL, fus_unlink(conn, ".."));
	TEST_ERRNO (ctx, EINVAL, fus_unlink(conn, "../.."));

	/* Attempt to unlink a directory.
	 */
	TEST_ERRNO_J (fail, ctx, 0, fus_mkdir(conn, "d", 0));
	TEST_ERRNO_J (fail, ctx, EISDIR, fus_unlink(conn, "d"));

fail:	fus_rmdir (conn, "d");
	TEST_LEAVE(ctx);
}
