/*
 * test-fus.h
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_TEST_FUS_H
#define SOLOCLOUD_TEST_FUS_H

/* System headers */
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

/* Filesystem in Userspace */
#include "fus.h"

/* Test runtime context.
 */
struct test_fus_ctx
{
	log_t       * tfus_log;
	solo_conn_t * tfus_conn;
	unsigned      tfus_numerr;    // number of errors
};

/* Tests.
 */
extern void test_fus_close     (struct test_fus_ctx *);
extern void test_fus_login     (struct test_fus_ctx *);
extern void test_fus_ftruncate (struct test_fus_ctx *);
extern void test_fus_mkdir     (struct test_fus_ctx *);
extern void test_fus_open      (struct test_fus_ctx *);
extern void test_fus_pread     (struct test_fus_ctx *);
extern void test_fus_readdir   (struct test_fus_ctx *);
extern void test_fus_rename    (struct test_fus_ctx *);
extern void test_fus_rmdir     (struct test_fus_ctx *);
extern void test_fus_unlink    (struct test_fus_ctx *);

/*
 * Accessory functions.
 */

#define RED "\033[31m"
#define GRE "\033[32m"
#define RST "\033[00m"

#define TEST_ENTER(ctx) \
	log_entering(ctx->tfus_log);

#define TEST_LEAVE(ctx) \
	log_leaving(ctx->tfus_log);

/* Test return value of expression */
#define TEST_ERRNO(ctx, err, exp) \
do {									\
	char const * const expstr = #exp;				\
	int const ret = (exp);						\
	if (err != ret) {						\
		ctx->tfus_numerr++;					\
		printf("test-fus: %sFAIL%s: %s:%u:\n",			\
		       RED, RST, __FILE__, __LINE__);			\
		printf("    >>> %s <<<\n", expstr);			\
		printf("    Returned %d (%s)\n",			\
		       ret, ret ? strerror(ret) : "Success");		\
		printf("    Expected %d (%s)\n",			\
		       err, err ? strerror(err) : "Success");		\
	}								\
} while(0)

/* Test return value of expression and jump to label on error */
#define TEST_ERRNO_J(label, ctx, err, exp) \
do {									\
	unsigned const n0 = ctx->tfus_numerr;				\
	TEST_ERRNO(ctx, err, exp);					\
	unsigned const n1 = ctx->tfus_numerr;				\
	if (n1 != n0)							\
		goto label;						\
} while(0)

/* Test condition */
#define TEST_COND(ctx, cond) \
do {									\
	char const * const condstr = #cond;				\
	if (!(cond)) {							\
		ctx->tfus_numerr++;					\
		printf("test-fus: %sFAIL%s: %s:%u:\n",			\
		       RED, RST, __FILE__, __LINE__);			\
		printf("    >>> %s <<<\n", condstr);			\
		printf("    Condition not satisfied\n");		\
	}								\
} while(0)

/* Test condition and jump to label on failure */
#define TEST_COND_J(label, ctx, cond) \
do {									\
	unsigned const n0 = ctx->tfus_numerr;				\
	TEST_COND(ctx, cond);						\
	unsigned const n1 = ctx->tfus_numerr;				\
	if (n1 != n0)							\
		goto label;						\
} while(0)

#endif
