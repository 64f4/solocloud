/*
 * test-fus-ftruncate.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "test-fus.h"

void test_fus_ftruncate(struct test_fus_ctx * const ctx)
{
	solo_conn_t * const conn = ctx->tfus_conn;
	char          tmpfile[] = "trunc.tmp";
	char          buf[64];
	struct stat   st;
	int           fd;

	TEST_ENTER(ctx);

	/* Issue TRUNC with a file descriptor that may exist on the server,
	 * but which does not belong to our session.
	 */
	TEST_ERRNO   (ctx, EBADF,  fus_ftruncate(conn, STDIN_FILENO, 0));
	TEST_ERRNO   (ctx, EBADF,  fus_ftruncate(conn, STDOUT_FILENO, 0));
	TEST_ERRNO   (ctx, EBADF,  fus_ftruncate(conn, STDERR_FILENO, 0));

	/* Issue TRUNC with invalid file descriptors and offset values.
	 */
	TEST_ERRNO   (ctx, EINVAL, fus_ftruncate(conn, -1, 0));
	TEST_ERRNO   (ctx, EINVAL, fus_ftruncate(conn, 0, -1));

	/* Issue TRUNC on an existing file. First we increase the size of the
	 * file, then we truncate the file size to zero.
	 */

	/* create a temporary file */
	int const flags = O_CREAT | O_RDWR | O_EXCL;
	TEST_ERRNO_J (fail, ctx, 0, fus_open(conn, tmpfile, flags, 0, &fd));

	/* check new file size (should be zero) */
	TEST_ERRNO_J (fail, ctx, 0, fus_stat(conn, tmpfile, &st));
	TEST_COND_J  (fail, ctx, st.st_size == 0);

	/* write something to the file */
	char const str[] = "Hello world!\n";
	size_t const len = sizeof(str) - 1;
	size_t nw = len;
	TEST_ERRNO_J (fail, ctx, 0, fus_pwrite(conn, fd, str, &nw, 0));
	TEST_COND_J  (fail, ctx, nw == len);

	/* check file size after writing */
	TEST_ERRNO_J (fail, ctx, 0, fus_stat(conn, tmpfile, &st));
	TEST_COND_J  (fail, ctx, (size_t) st.st_size == len);

	/* increase file size with ftruncate() */
	TEST_ERRNO_J (fail, ctx, 0, fus_ftruncate(conn, fd, 64));
	TEST_ERRNO_J (fail, ctx, 0, fus_stat(conn, tmpfile, &st));
	TEST_COND_J  (fail, ctx, st.st_size == 64);

	/* read the original data back, make sure it's still there */
	size_t nr = len;
	memset(buf, 0, sizeof(buf));
	TEST_ERRNO_J (fail, ctx, 0, fus_pread(conn, fd, buf, &nr, 0));
	TEST_COND_J  (fail, ctx, nr == len);
	TEST_COND_J  (fail, ctx, !strncmp(str, buf, len));

	/* shrink file size to zero with ftruncate() */
	TEST_ERRNO_J (fail, ctx, 0, fus_ftruncate(conn, fd, 0));
	TEST_ERRNO_J (fail, ctx, 0, fus_stat(conn, tmpfile, &st));
	TEST_COND_J  (fail, ctx, st.st_size == 0);

	/* close and delete file */
	TEST_ERRNO   (ctx, 0, fus_close(conn, fd));
	TEST_ERRNO   (ctx, 0, fus_unlink(conn, tmpfile));

fail:	TEST_LEAVE(ctx);
}
