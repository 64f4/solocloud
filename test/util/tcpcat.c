/*
 * tcpcat.c
 *
 *	Tool for testing connections to servers that use `libtcp'.
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>

#include "tcp.h"  // TCP communication library

int main(int const argc, char * const argv[])
{
	log_t      * log;
	char const * host;
	char       * e;
	int          port;
	tcp_conn_t * conn;
	char         buf[256];
	int          err;

	if (argc != 2) {
		fprintf(stderr, "Syntax: tcpcat host:port\n");
		return EX_USAGE;
	}

	/* parse host name and port number */
	host = argv[1];
	if ((e = strchr(host, ':')) == NULL) {
		fprintf(stderr, "tcpcat: expected hostname:port\n");
		goto fail_0;
	}
	*e++ = '\0';
	port = atoi(e);
	if (port <= 0 || port >= UINT16_MAX) {
		fprintf(stderr, "tcpcat: invalid port number\n");
		goto fail_0;
	}

	fprintf(stderr, "tcpcat: connecting to %s on port %d\n", host, port);

	/* open log stream */
	if ((err = log_open_f(stderr, &log))) {
		fprintf(stderr, "tcpcat: %s\n", strerror(err));
		goto fail_0;
	}

	/* connect to server */
	if ((err = tcp_connect(log, host, port, &conn))) {
		log_perror(log, LOG_TRACE, "tcp_connect()", err);
		goto fail_1;
	}

	/* read from stdin */
	while ((fgets(buf, sizeof(buf), stdin)))
	{
		size_t len = strlen(buf);
		/* strip newline char if present */
		if (len && buf[len - 1] == '\n')
			len--;
		/* send data */
		if ((err = tcp_send(conn, buf, len))) {
			log_perror(log, LOG_TRACE, "tcp_send()", err);
			goto fail;
		}
	}

	tcp_close(conn);
	log_close(log);
	return 0;

fail:	tcp_close(conn);
fail_1:	log_assert(log, err);
	log_close(log);
fail_0:	return EX_SOFTWARE;
}
