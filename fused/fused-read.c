/*
 * fused-read.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <limits.h>
#include <sys/time.h>

/* Project headers */
#include "safepthread.h"

/* Private header */
#include "fused.h"

/* Asynchronous reader: minimum file size */
#define FUSED_READER_MINSIZE (256u << 10)  // 256 KiB

/* Asynchronous reader: block size */
#define FUSED_READER_BLKSZ (512u << 10)    // 512 KiB

/* Convert `timeval' structure to a double time value */
#define DOUBLETIME(tv) (tv.tv_sec + tv.tv_usec / 1000000.0)

/**
 * Asynchronous reader.
 *
 * This thread is launched when reading a large file. It reads the contents of
 * the entire file into a local, temporary file using large blocks, so
 * fused_read() does not have to issue many small requests to the server.
 */
static void *
_reader(void * const arg)
{
	struct fused_openfile * const of    = arg;
	struct fused          * const fused = of->fso_fused;
	log_t                 * const log   = fused->fsd_log;
	solo_t                * const solo  = fused->fsd_solo;
	char const            * const boxid = fused->fsd_boxid;
	char const            * const pass  = fused->fsd_pass;
	solo_conn_t           * conn        = NULL;
	bool                    eof         = false;
	char                  * buf;
	size_t                  blksize;
	fus_fd_t                svfd;
	size_t                  nr;
	ssize_t                 nw;
	struct timeval          t0;
	struct timeval          t1;
	int                     err;

	// FIXME
	//	The password should not be kept in memory in plain text. We
	//	need FUS facilities to either clone a connection, or login
	//	through a hash.
	//

	log_entering(log);

	/* connect to box, login, and open the desired file for reading */
	if ((err = solo_connect(solo, boxid, SOLO_PROTO_FUS, &conn))) {
		log_perror(log, LOG_TRACE, "solo_connect()", err);
		goto fail_0;
	}
	if ((err = fus_login(conn, pass))) {
		log_perror(log, LOG_TRACE, "fus_login()", err);
		goto fail_1;
	}
	if ((err = fus_open(conn, of->fso_path, O_RDONLY, 0, &svfd))) {
		log_perror(log, LOG_TRACE, "fus_open()", err);
		goto fail_1;
	}

	/* Allocate read buffer. We are issuing relatively large requests to
	 * the server as to minimize communication overhead.
	 */
	if ((buf = malloc(FUSED_READER_BLKSZ)) == NULL) {
		err = errno;
		log_perror(log, LOG_ERR, "malloc()", err);
		goto fail_1;
	}

	log_printf(log, LOG_DEBUG, "reading...");

	/* initial request size */
	blksize = GCC_MIN(128u << 10, FUSED_READER_BLKSZ); // 128 KiB

	/* Store connection in file descriptor so it can be dropped when the
	 * file is closed by fused_release().
	 */
	Pthread_mutex_lock(&of->fso_rmux);
	of->fso_rconn = conn;
	while (   !of->fso_rintr    // interrupted by fused_release()
	       && !of->fso_rerr     // I/O error
	       && !of->fso_reof )   // EOF reached
	{
		Pthread_mutex_unlock(&of->fso_rmux);

		/* issue READ request to the server */
		nr = blksize;
		gettimeofday(&t0, NULL);
		if ((err = fus_pread(conn, svfd, buf, &nr, of->fso_rsize)))
			goto done;
		gettimeofday(&t1, NULL);
		if (nr == 0) {
			eof = true;
			goto done;
		}

		/* write received data to temporary file */
		nw = pwrite(of->fso_rfd, buf, nr, of->fso_rsize);
		if (nw < 0 || (size_t) nw != nr) {
			err = EIO;
			goto done;
		}

		/* log a few useful stats */
		double const td = DOUBLETIME(t1) - DOUBLETIME(t0);
		log_printf(log, LOG_INFO,
			   "[%s | %zu KiB | @%llu | %.1lf KiB/s]",
			   of->fso_path, nr >> 10,
			   (long long unsigned) of->fso_rsize,
			   (nr / td / 1024.0));

		/* double the request size */
		blksize = GCC_MIN(FUSED_READER_BLKSZ, blksize << 1);

		/* notify fused_read() that data is available */
done:		Pthread_mutex_lock(&of->fso_rmux);
		of->fso_reof = eof;
		of->fso_rerr = err;
		if (!eof && !err)
			of->fso_rsize += nr;
		if (of->fso_rwait)
			Pthread_cond_signal(&of->fso_rcond);
	}
	of->fso_rconn = NULL;
	Pthread_mutex_unlock(&of->fso_rmux);
	free(buf);
	solo_close(conn);
	log_leaving(log);
	pthread_exit(NULL);

	/* Reader bootstrap failures.
	 */
fail_1:	solo_close(conn);
fail_0:	log_assert(log, err);
	Pthread_mutex_lock(&of->fso_rmux);
	of->fso_rerr = err;
	if (of->fso_rwait)
		Pthread_cond_signal(&of->fso_rcond);
	Pthread_mutex_unlock(&of->fso_rmux);
	log_leaving(log);
	pthread_exit(NULL);
}

static void
_launch_reader(struct fused_openfile * const of)
{
	log_t * const log = of->fso_fused->fsd_log;
	int err;

	/* create temporary file to receive data asynchronously */
	strcpy(of->fso_rpath, "/tmp/solo-fs.XXXXXX");
	if ((of->fso_rfd = mkstemp(of->fso_rpath)) < 0) {
		err = errno;
		log_perror(log, LOG_ERR, "mkstemp()", err);
		goto fail_0;
	}

	/* start reading asynchronously */
	Pthread_mutex_init(&of->fso_rmux, NULL);
	Pthread_cond_init(&of->fso_rcond, NULL);
	if ((err = pthread_create(&of->fso_rthread, NULL, _reader, of))) {
		log_perror(log, LOG_ERR, "pthread_create()", err);
		goto fail_1;
	}

	of->fso_ralive = true;
	of->fso_rhealthy = true;
	return;

fail_1:
	Pthread_mutex_destroy(&of->fso_rmux);
	Pthread_cond_destroy(&of->fso_rcond);
	unlink(of->fso_rpath);
	close(of->fso_rfd);
	of->fso_rfd = -1;
fail_0:
	of->fso_ralive = false;
	return;
}

static bool
_buffered_read(
	struct fused_openfile * const of,
	void                  * const buf,
	size_t                  const count,
	off_t                   const offs,
	size_t                * const n,
	double                * const p
	)
{
	log_t * const log = of->fso_fused->fsd_log;

	/* the new offset after reading the data */
	off_t const endoffs = offs + count;

	/* If we are downloading data asynchronously, attempt to read data
	 * from the buffered stream instead of contacting the server directly.
	 * If the data we are interested in is not yet available, then wait
	 * for it if all the following conditions apply:
	 *
	 * (a) The end of the file has not been reached. If the EOF has been
	 *     read, then there is not point waiting as we should already have
	 *     all the data in question.
	 *
	 * (b) There are no errors. If the asynchronous reader reported an
	 *     error, then we cannot trust what has been already read.
	 *
	 * (c) Buffering has started. It may take some time before buffering
	 *     begins, so in the meanwhile we want to go directly to the
	 *     server.
	 *
	 * (d) The offset we are trying to read is beyond the currently
	 *     buffered data size. This means that the data in question has
	 *     not yet been buffered.
	 *
	 * (e) The offset we are trying to read is not too far from the end of
	 *     the currently buffered data. Otherwise the process may be doing
	 *     random access, where buffering doesn't help. Sometimes a
	 *     process will do both scattered and sequential access.
	 */

	Pthread_mutex_lock(&of->fso_rmux);
	while (   !of->fso_reof                                    // (a)
	       && !of->fso_rerr                                    // (b)
	       &&  of->fso_rsize                                   // (c)
	       &&  endoffs > of->fso_rsize                         // (d)
	       &&  endoffs - of->fso_rsize <= FUSED_READER_BLKSZ)  // (e)
	{
		of->fso_rwait = true;
		Pthread_cond_wait(&of->fso_rcond, &of->fso_rmux);
		of->fso_rwait = false;
	}
	int const err = of->fso_rerr;
	bool const eof = of->fso_reof;
	off_t const roffs = of->fso_rsize;
	Pthread_mutex_unlock(&of->fso_rmux);

	/* reader I/O error */
	if (err) {
		of->fso_rhealthy = false;
		return false;
	}
	/* insufficient buffered data */
	if (endoffs > roffs)
		return false;

	/* read buffered data */
	int const ret = pread(of->fso_rfd, buf, count, offs);
	if (ret < 0) {
		int const err = errno;
		log_perror(log, LOG_ERR, "pread()", err);
		of->fso_rhealthy = false;
		return false;
	}

	*n = ret;
	*p = eof ? 0 : (double) (offs + ret) / roffs * 100.0;
	return true;
}

int
fused_read(
	char const            * const path,
	char                  * const buf,
	size_t                  const count,
	off_t                   const offs,
	struct fuse_file_info * const fi
	)
{
	struct fused          * const fused = _fused();
	log_t                 * const log   = fused->fsd_log;
	solo_conn_t           * const conn  = fused->fsd_conn;
	size_t                  n;
	double                  p;
	int                     err;

	log_entering_args(log, "\"%s\", %lluB, @%llu", path,
			  (long long) count, (long long) offs);

	/* file descriptor */
	struct fused_openfile * const of = (void *) fi->fh;
	log_assert(log, of != NULL);
	fus_fd_t fd = of->fso_fd;

#ifdef __APPLE__
	/* If the open() operations was delayed (see note in fused_open()),
	 * then open the file now. Notice that the file is opened in read-only
	 * mode, as any other access mode would have not delayed the open()
	 * operation (again, see fused_open()).
	 */
	if (fd == INT_MIN) {
		log_printf(log, LOG_DEBUG, "(opening now)");
		if ((err = fus_open(conn, path, O_RDONLY, 0, &fd))) {
			log_perror(log, LOG_TRACE, "fus_open()", err);
			log_leaving_err(log, -err);
			return -err;
		}
		/* use real file descriptor from now on */
		of->fso_fd = fd;
	}
#endif

	// XXX
	//	Ideally we would launch an asynchronous reader during an
	//	open() operation, but as we have seen the Finder issues a lot
	//	of open() operations immediately followed by close(), without
	//	any read() in between. This is also the reason why on OSXFUSE
	//	we delay the open() operation until the first read() (see
	//	above).
	//

	/* Determine whether an asynchronous reader makes sense for this file.
	 *
	 * Don't use an asynchronous reader if the file was opened in R/W
	 * mode, or the file is too small, or the read operation reaches
	 * beyond the end of the file.
	 */
	bool  const canwrite = FUSED_O_WRITE(of->fso_flags);
	off_t const filesize = of->fso_st.st_size;
	bool  const toosmall = filesize < FUSED_READER_MINSIZE;
	off_t const endoffs  = offs + count;
	if (canwrite || toosmall || endoffs >= filesize)
		goto SLOW;

	/* Launch an asynchronous reader.
	 */
	if (!of->fso_rready) {
		of->fso_rready = true;
		_launch_reader(of);
	}

	/* Attempt to read from the buffered stream.
	 */
	if (of->fso_rhealthy && _buffered_read(of, buf, count, offs, &n, &p)) {
		if (p == 0)
			log_leaving_ret(log, "%zuB [buffered]", n);
		else
			log_leaving_ret(log, "%zuB [buffered %%%.0lf]", n, p);
		return n;
	}

	/* Read directly from the server.
	 */
SLOW:	n = count;
	if ((err = fus_pread(conn, fd, buf, &n, offs))) {
		log_leaving_err(log, -err);
		return -err;
	}
	log_leaving_ret(log, "%zuB", n);
	return n;
}
