/*
 * fused-truncate.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include "fused.h"  // private header

int
fused_truncate(char const * const path, off_t const offs)
{
	struct fused          * const fused = _fused();
	log_t                 * const log   = fused->fsd_log;
	struct fuse_file_info   fi;
	int                     ret;

	log_entering_args(log, "\"%s\"", path);

	memset(&fi, 0, sizeof(fi));
	fi.flags |= O_WRONLY;
	if ((ret = fused_open(path, &fi)) == 0) {
		ret = fused_ftruncate(path, offs, &fi);
		(void) fused_release(path, &fi);
	}

	log_leaving_err(log, ret);
	return ret;
}
