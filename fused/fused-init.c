/*
 * fused-init.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <signal.h>
#include <stdio.h>

/* Private header */
#include "fused.h"

int
fused_init(
	char const    * const boxid,
	char const    * const server,   // (may be NULL)
	struct fused ** const newfused  // [out] new daemon context
	)
{
	struct fused  * fused;
	log_t         * log;
	solo_t        * solo;
	solo_conn_t   * conn;
	int             err;

	// TODO
	//	If process is not expected to become a daemon, enable logging
	//	to stderr (requires better parsing of command-line options).

	/* If stderr is a terminal, then redirect the log stream to /dev/null
	 * because, as a daemon, stderr is not available.
	 */
	if (isatty(STDERR_FILENO))
	{
		FILE * const f = fopen("/dev/null", "w");
		if (f == NULL) {
			err = errno;
			goto fail_log;
		}
		if ((err = log_open_f(f, &log))) {
			fclose(f);
			goto fail_log;
		}
	}
	/* If stderr is not a terminal, then enable logging but dup() the file
	 * descriptor first, as it will be closed when the process becomes a
	 * daemon.
	 */
	else {
		int const fd = dup(STDERR_FILENO);
		if (fd < 0) {
			err = errno;
			goto fail_log;
		}
		FILE * const f = fdopen(fd, "w");
		if (f == NULL) {
			err = errno;
			close(fd);
			goto fail_log;
		}
		/* open log stream */
		if ((err = log_open_f(f, &log))) {
			fclose(f);
			goto fail_log;
		}
	}

	/* Ininitialize SoloCloud library.
	 */
	struct solo_cf const solocf = {
		.socf_server = server,  // (NULL selects the default)
	};
	if ((err = solo_init(log, &solocf, &solo))) {
		log_perror(log, LOG_TRACE, "solo_init()", err);
		goto fail_solo;
	}

	// FIXME !
	//	Besides being unsafe, this code should also duplicate the
	//	password string.

	/* ask user for password */
	char const * const pass = getpass("Password: ");

	/* connected to box */
	if ((err = solo_connect(solo, boxid, SOLO_PROTO_FUS, &conn))) {
		log_perror(log, LOG_TRACE, "solo_connect()", err);
		goto fail_connect;
	}

	/* login */
	if ((err = fus_login(conn, pass))) {
		log_perror(log, LOG_ERR, "fus_login()", err);
		goto fail_login;
	}

	/* allocate daemon context */
	size_t const boxidlenz = strlen(boxid) + 1;
	if ((fused = malloc(sizeof(struct fused) + boxidlenz)) == NULL) {
		err = errno;
		log_perror(log, LOG_ERR, "malloc()", err);
		goto fail;
	}
	*fused = (struct fused) {
		.fsd_log   = log,
		.fsd_solo  = solo,
		.fsd_conn  = conn,
		.fsd_pass  = pass,
		.fsd_boxid = (char const *) (fused + 1),
	};
	memcpy(fused + 1, boxid, boxidlenz);

	*newfused = fused;
	return 0;

fail:
fail_login:
	solo_close(conn);
fail_connect:
	solo_uninit(solo);
fail_solo:
	log_assert(log, err);
	log_printf(log, LOG_ERR, "exiting (with errors)");
	log_close(log);
fail_log:
	*newfused = NULL;
	return err;
}

void
fused_destroy(struct fused * const fused)
{
	log_t * const log = fused->fsd_log;
	log_printf(log, LOG_DEBUG, "fsd_cache_coll: %u", fused->fsd_cache_coll);
	log_printf(log, LOG_INFO, "exiting");
	solo_close(fused->fsd_conn);
	solo_uninit(fused->fsd_solo);
	mm_free(log, fused);
	log_close(log);
}
