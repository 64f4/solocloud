/*
 * fused.h
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#ifndef SOLOCLOUD_FUSED_H
#define SOLOCLOUD_FUSED_H

/* System headers */
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

/* Project headers */
#include "fus.h"
#include "malloc.h"

/* Maximum buffer size for read/write operations */
#define FUSED_MAX_IOBUF 65536

/* Cached result of a getattr() operation.
 */
struct fused_attr
{
	uint64_t            fat_h[2];       // file name hash
	int32_t             fat_err;        // getattr() error
	uint16_t            fat_quantum;    // cache time quantum (in seconds)
	uint16_t            fat_st_nlink;   // stat(): number of hard links
	uint32_t            fat_st_mode;    // stat(): file mode
	uint32_t            fat_st_size;    // stat(): file size in bytes
	uint64_t            fat_st_atime;   // stat(): time of last access
	uint64_t            fat_st_mtime;   // stat(): time of last modification
	uint64_t            fat_st_ctime;   // stat(): time of last status change
	int64_t             fat_atime;      // cache last access time
	int64_t             fat_etime;      // cache expiration time

} GCC_PACKED;

/* Open file descriptor.
 */
struct fused_openfile
{
	struct fused      * fso_fused;

	char const        * fso_path;       // file path
	fus_fd_t            fso_fd;         // server file descriptor
	mode_t              fso_flags;      // open() flags
	struct stat         fso_st;         // stat() info

	/* Asynchronous reader. Runs in a separate thread and reads the file
	 * using large block sizes in order to maximize throughput. A reader
	 * is only launched when the file is large enough and is opened in
	 * read-only mode.
	 */
	bool                fso_rready;     // reader has been initialized
	bool                fso_ralive;     // reader is alive
	bool                fso_rhealthy;   // reader is healthy
	pthread_t           fso_rthread;    // reader thread ID
	char                fso_rpath[32];  // temporary file path
	int                 fso_rfd;        // temporary file descriptor
	pthread_mutex_t     fso_rmux;       // (protects the fields below)
	pthread_cond_t      fso_rcond;      // signals reader data is available
	bool volatile       fso_rwait;      // fused_read() is waiting for data
	off_t               fso_rsize;      // size of data file
	bool volatile       fso_reof;       // end of file reached
	int volatile        fso_rerr;       // file I/O error
	bool volatile       fso_rintr;      // fused_release() has been called
	solo_conn_t       * fso_rconn;      // reader connection to the server
};

/* Daemon runtime context.
 */
struct fused
{
	log_t             * fsd_log;        // log stream
	solo_t            * fsd_solo;       // SoloCloud library instance
	solo_conn_t       * fsd_conn;       // SoloCloud switchboard connection
	char const        * fsd_pass;       // password
	char const        * fsd_boxid;      // box identifier

	/* attribute cache */
	unsigned            fsd_cache_coll; // # of collisions
	struct fused_attr   fsd_cache[1021];
};

/* Private functions.
 */
extern int    fused_init      (char const *, char const *, struct fused **);
extern void   fused_destroy   (struct fused *);
extern void   fused_cache_put (struct fused *, char const *, int, struct stat const *, time_t);
extern int    fused_cache_get (struct fused *, char const *, int *, struct stat *, time_t);
extern void   fused_cache_del (struct fused *, char const *);

/* System calls.
 */
extern int    fused_access    (char const *, int);
extern int    fused_create    (char const *, mode_t, struct fuse_file_info *);
extern int    fused_ftruncate (char const *, off_t, struct fuse_file_info *);
extern int    fused_getattr   (char const *, struct stat *);
extern int    fused_mkdir     (char const *, mode_t);
extern int    fused_open      (char const *, struct fuse_file_info *);
extern int    fused_read      (const char *, char *, size_t, off_t, struct fuse_file_info *);
extern int    fused_readdir   (char const *, void *, fuse_fill_dir_t, off_t, struct fuse_file_info *);
extern int    fused_release   (char const *, struct fuse_file_info *);
extern int    fused_rename    (char const *, char const *);
extern int    fused_rmdir     (char const *);
extern int    fused_statfs    (char const *, struct statvfs *);
extern int    fused_truncate  (char const *, off_t);
extern int    fused_unlink    (char const *);
extern int    fused_write     (const char *, char const *, size_t, off_t, struct fuse_file_info *);

/* Shortcut to retrieve the FUSE daemon runtime context.
 */
static inline struct fused * _fused(void)
{
	struct fuse_context const * const ctx = fuse_get_context();
	return ctx->private_data;
}

/* Print messages to standard error during initialization.
 */
#define FUSED_PREFIX "solo-fs"
#define fused_fatal(fmt, ...) \
   fprintf(stderr, FUSED_PREFIX ": fatal: " fmt "\n", ## __VA_ARGS__)
#define fused_warning(fmt, ...) \
   fprintf(stderr, FUSED_PREFIX ": warning: " fmt "\n", ## __VA_ARGS__)
#define fused_info(fmt, ...) \
   fprintf(stderr, FUSED_PREFIX ": " fmt "\n", ## __VA_ARGS__)

/* Test whether open() flags indicate write access.
 */
#define FUSED_O_WRITE(f) \
	(f) & (O_RDWR | O_WRONLY | O_CREAT | O_APPEND | O_TRUNC);

/* Check for frivolous Apple files.
 *
 * In particular, paths containing "/." are ignored, as we really don't care
 * about hidden files and especially extended attribtes (which we cannot
 * officialy disable using mount options due to weird Finder behavior).
 *
 * The Finder application on the Mac touches a lot of hidden files, especially
 * attribute files (e.g. "._foo.txt"). By ignoring operations on these files,
 * we eliminate a great number of frivolous requests to the server and
 * significantly speed up access to our remote filesystem.
 */
static inline bool fused_is_frivolous(char const * const path)
{
	return strstr(path, "/.")
	    || strstr(path, "/Backups.backupdb")
	    || strstr(path, "/Contents")
	    || strstr(path, "/Resources")
	    || strstr(path, "/Support Files")
	    || strstr(path, "/mach_kernel")
	    || strstr(path, "/Contents");
}

#endif
