/*
 * fused-main.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <assert.h>
#include <sysexits.h>

#include "fused.h"  // private header

/* A FUSE operation that does nothing but is nevertheless required for the
 * proper functioning of the FUSE driver.
 */
static int
fused_dummy()
{
	return 0;
}

/* FUSE initialization.
 */
static void *
fused_dummy_init(struct fuse_conn_info * const fci)
{
	fci->max_write = FUSED_MAX_IOBUF;      // buffer size for write()
	fci->max_readahead = FUSED_MAX_IOBUF;  // buffer size for read()
	return _fused();
}

/* FUSE file operations.
 */
static const struct fuse_operations fused_ops =
{
	/* System calls */
	.ftruncate = fused_ftruncate,
	.getattr   = fused_getattr,
	.mkdir     = fused_mkdir,
	.open      = fused_open,
	.read      = fused_read,
	.readdir   = fused_readdir,
	.release   = fused_release,
	.rename    = fused_rename,
	.rmdir     = fused_rmdir,
	.statfs    = fused_statfs,
	.unlink    = fused_unlink,
	.write     = fused_write,

	/* Pseudo system calls (implemented via other system calls) */
	.access    = fused_access,
	.create    = fused_create,
	.truncate  = fused_truncate,

	/* Dummy system calls (they do nothing, always succeed) */
	.chmod     = (void *) fused_dummy,
	.chown     = (void *) fused_dummy,
	.flush     = (void *) fused_dummy,
	.fsync     = (void *) fused_dummy,
	.init      =          fused_dummy_init,
	.utimens   = (void *) fused_dummy,
};

/* Mandatory FUSE options */
static char const * const fused_argv[] = {
#if 0
	"-f",  // stay in the foreground
#endif
	"-s",  // single-threaded
	NULL,
};

static void
help_and_exit(void)
{
	fprintf(stderr, "\
Usage: " FUSED_PREFIX " [fuse-opts] MOUNTPATH BOXID [SWITCHBOARD]\n\
\n\
This is the SoloCloud remote filesystem driver. It connects to the SoloBox\n\
given by BOXID, and mounts the corresponding remote file system at the path\n\
MOUNTPATH. The remote file system can be accessed at that location.\n\
\n\
Mandatory arguments:\n\
  MOUNTPATH      local mount point for the remote filesystem\n\
  BOXID          box we will be connecting to\n\
\n\
Optional arguments:\n\
  fuse-opts      miscellaneous FUSE options (developers only)\n\
  SWITCHBOARD    switchboard server (developers only)\n\
\n\
Example: " FUSED_PREFIX " ~/box mrspock-box\n\
");
	exit(EX_USAGE);
}

int main(int uargc, char const * uargv[])
{
	char const * boxid;
	char const * server;
	int          i;

	// TODO We need better option parsing.

	// TODO Duplicate uargv[].

	/* Very crude option parsing...
	 */

	/* too few arguments? */
	if (uargc < 2)
		help_and_exit();
	/* override -h, -help, and --help options */
	for (i = 1; i < uargc; i++) {
		char const * const o = uargv[i];
		if (   !strcmp(o, "-h")
		    || !strcmp(o, "-help")
		    || !strcmp(o, "--help"))
			help_and_exit();
	}
	/* find mount path, box id, and (optional) server address */
	for (i = 1; i < uargc && uargv[i][0] == '-'; i++)
		continue;
	int const nleft = uargc - i;
	if (nleft < 2 || nleft > 3)
		help_and_exit();
	boxid  = uargv[i + 1];
	server = uargv[i + 2];  // (may be NULL)
	if (*boxid == '-' || (server && *server == '-'))
		help_and_exit();
	/* remove box id and (optional) server address from command line */
	uargv[i + 1] = NULL;
	uargv[i + 2] = NULL;    // (may already be NULL)
	uargc = i + 1;

	/* Add operating-system specific options to the command line.
	 */

#ifdef __APPLE__
	size_t const argc = GCC_DIM(fused_argv) + uargc + 2;
#else
	size_t const argc = GCC_DIM(fused_argv) + uargc;
#endif
	char const * * const argv = malloc((argc + 1) * sizeof(char *));
	if (argv == NULL)
		return EX_OSERR;
	char const * * wp = argv;
	char const * const * rp;
	/* set process name */
	*wp++ = uargv[0];
	/* process UID and GID */
	char idbuf[64];
	snprintf(idbuf, sizeof(idbuf), "-ouid=%u,gid=%u", getuid(), getgid());
	*wp++ = idbuf;
	/* set OS specific arguments */
	for (rp = fused_argv; *rp; rp++, wp++)
		*wp = *rp;
#ifdef __APPLE__
	/* volume and filesystem name */
	char volbuf[64];
	snprintf(volbuf, sizeof(volbuf), "-ovolname=%s", boxid);
	*wp++ = volbuf;
	*wp++ = "-ofsname=" FUSED_PREFIX;
#endif
	/* set user arguments */
	for (rp = uargv + 1; *rp; rp++, wp++)
		*wp = *rp;
	*wp = NULL;

	assert((size_t) (wp - argv) == argc);
#if 0
	unsigned ix;
	for (ix = 0; ix < argc; ix++)
		fprintf(stderr, "[%2u] `%s'\n", ix, argv[ix]);
#endif
	/*
	 * Ready to mount the file system.
	 */

	/* connect to box and login */
	int err;
	struct fused * fused;
	if ((err = fused_init(boxid, server, &fused))) {
		int ex = EX_UNAVAILABLE;
		/* most common errors */
		switch (err) {
		case EPERM:
			fused_fatal("invalid password");
			ex = EX_NOPERM;
			break;
		case ESRCH:
			fused_fatal("failed to locate box `%s'", boxid);
			ex = EX_NOHOST;
			break;
		case EHOSTUNREACH:
		case ECONNREFUSED:
			fused_fatal("switchboard server is unavailable, please retry later");
			break;
		default:
			fused_fatal("unknown error (%s)", strerror(err));
		}
		free(argv);
		return ex;
	}

	fused_info("connected to `%s'", boxid);

	/* FUSE main event loop */
	int const ret = fuse_main(argc, (char **) argv, &fused_ops, fused);
	fused_destroy(fused);
	free(argv);
	return ret;
}
