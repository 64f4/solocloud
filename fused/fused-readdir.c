/*
 * fused-readdir.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fused.h"

struct fillctx
{
	struct fused      * fused;
	fuse_fill_dir_t     filler;
	void              * dirbuf;
	time_t              now;
};

static int
fillfunc(
	void              * const arg,
	char const        * const name,
	char const        * const path,
	struct stat const * const stbuf
	)
{
	struct fillctx    * const ctx    = arg;
	struct fused      * const fused  = ctx->fused;
	fuse_fill_dir_t     const filler = ctx->filler;
	void              * const dirbuf = ctx->dirbuf;
	time_t              const now    = ctx->now;

	/* invoke FUSE filler function */
	if (filler(dirbuf, name, NULL, 0) == 1)
		return ENOMEM;

	/* cache file attributes */
	fused_cache_put(fused, path, 0, stbuf, now);

	return 0;
}

int
fused_readdir(
	char const            * const path,
	void                  * const dirbuf,
	fuse_fill_dir_t         const filler,
	off_t                   const GCC_ATTRIBUTE_UNUSED(offset),
	struct fuse_file_info * const GCC_ATTRIBUTE_UNUSED(fi)
	)
{
	struct fused          * const fused = _fused();
	log_t                 * const log   = fused->fsd_log;
	solo_conn_t           * const conn  = fused->fsd_conn;
	int                     err;

	// XXX
	//	If an error occurs in the middle of fus_readdir(), our
	//	attribute cache may contain entries that could not be reported
	//	by fus_readdir() and therefore, from the point of view of the
	//	user, they shouldn't exist. I don't think this is problem, as
	//	these entries do in fact exist and can be still accessed
	//	directly by open(), stat(), and so on. However, we should
	//	revisit this case to make sure that we don't overlook any
	//	cache inconsistencies or other unexpected scenarios.
	//

	log_entering_args(log, "\"%s\"", path);

#ifdef __APPLE__
	/* avoid operations on frivolous files */
	if (fused_is_frivolous(path)) {
		log_printf(log, LOG_DEBUG, "(ignore)");
		log_leaving_err(log, -ENOENT);
		return -ENOENT;
	}
#endif
	struct fillctx ctx = { fused, filler, dirbuf, time(NULL) };
	if ((err = fus_readdir(conn, path, &ctx, fillfunc)))
		log_perror(log, LOG_TRACE, "fus_readdir()", err);

	log_leaving_err(log, -err);
	return -err;
}
