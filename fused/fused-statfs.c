/*
 * fused-statfs.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fused.h"

int
fused_statfs(char const * const path, struct statvfs * const stvfs)
{
	struct fused * const fused = _fused();
	log_t        * const log   = fused->fsd_log;
	solo_conn_t  * const conn  = fused->fsd_conn;
	time_t         const now   = time(NULL);
	int            err;

	log_entering_args(log, "\"%s\"", path);

	/* path must be root */
	if (strcmp(path, "/")) {
		log_leaving_err(log, -EINVAL);
		return -EINVAL;
	}

	/* Here we do a much more rudimental kind of caching compared to the
	 * attribute cache. We need to have some caching because the Finder
	 * issues a ridiculous number of statfs() system calls.
	 *
	 * Simply put, we use the cached result if it is less than 10 seconds
	 * from the previous call to statfs(). We ignore the path, as it is
	 * always "/".
	 */

	static time_t         last_time  = 0;
	static int            last_err   = 0;
	static struct statvfs last_stvfs;

	if (now - last_time < 10) {
		log_printf(log, LOG_DEBUG, "(cached)");
		log_leaving_err(log, -last_err);
		*stvfs = last_stvfs;
		return -last_err;
	}

	/* Execute statfs() system call.
	 */
	err = fus_statvfs(conn, path, stvfs);

	/* Cache result.
	 */
	last_time  = now;
	last_err   = err;
	last_stvfs = *stvfs;

	log_leaving_err(log, -err);
	return -err;
}
