/*
 * fused-open.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* System headers */
#include <limits.h>

/* Private header */
#include "fused.h"

int
fused_open(char const * const path, struct fuse_file_info * const fi)
{
	struct fused * const fused = _fused();
	log_t        * const log   = fused->fsd_log;
	solo_conn_t  * const conn  = fused->fsd_conn;
	int            const flags = fi->flags;
	struct stat    stbuf;
	int            fd;
	int            err;

	log_entering_args(log, "\"%s\", 0x%08x", path, flags);

	/* opening for writing? */
	bool const writing = FUSED_O_WRITE(flags);

	fd = -1;

	memset(&stbuf, 0, sizeof(stbuf));

#ifdef __APPLE__

	/* avoid operations on frivolous files */
	if (fused_is_frivolous(path)) {
		log_printf(log, LOG_DEBUG, "(ignore)");
		err = flags & O_CREAT ? EPERM : ENOENT;
		log_leaving_err(log, -err);
		return -err;
	}

	// XXX
	//	The Finder issues a lot of open() calls immediately followed
	//	by close(). I'm not sure why, but maybe it does so to check
	//	whether a file exists, or to verify access to the file.
	//	Whatever the reason, this behavior is a significant drag on
	//	performance. Therefore, in OSXFUSE we don'actually issue an
	//	open operation until we actually read something from the file
	//	(notice that when opening a file with write access, the OPEN
	//	operation is always carried out).
	//

	if (writing)
	{
		/* invalidate cache */
		fused_cache_del(fused, path);
		/* open() */
		err = fus_open(conn, path, flags, 0, &fd);
	}
	else
	{
		log_printf(log, LOG_DEBUG, "(delayed)");
		/* only check whether the file exists */
		err = fused_getattr(path, &stbuf);
		fd = INT_MIN;
	}

#else   // ifdef __APPLE__

	/* invalidate cache */
	if (writing)
		fused_cache_del(fused, path);
	else
		(void) fused_getattr(path, &stbuf);

	/* open() */
	err = fus_open(conn, path, flags, 0, &fd);

#endif
	/* stop here on errors */
	if (err) {
		fi->fh = (uintptr_t) NULL;
		log_leaving_err(log, -err);
		return -err;
	}

	// XXX
	//	Initially we were assigning `fd' directly to `fi->fh'.
	//	However, once set this field cannot be changed by any other
	//	system call. This is a problem for delayed open operations,
	//	because a later read() system call requires `fi->fh' to be
	//	modified.
	//

	/* allocate open file descriptor */
	struct fused_openfile * of;
	size_t const pathlenz = strlen(path) + 1;
	if ((of = malloc(sizeof(struct fused_openfile) + pathlenz)) == NULL) {
		err = errno;
		log_perror(log, LOG_ERR, "malloc()", err);
		/* close the file we just opened */
		if (fd >= 0) (void) fus_close(conn, fd);
		fi->fh = (uintptr_t) NULL;
		log_leaving_err(log, -err);
		return -err;
	}
	*of = (struct fused_openfile) {
		.fso_fused = fused,
		.fso_path  = (char const *) (of + 1),
		.fso_fd    = fd,     // (INT_MIN if delayed open)
		.fso_flags = flags,
		.fso_st    = stbuf,
		.fso_rfd   = -1,
	};
	memcpy(of + 1, path, pathlenz);

	fi->fh = (uintptr_t) of;
	log_leaving_err(log, 0);
	return 0;
}
