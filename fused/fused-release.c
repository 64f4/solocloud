/*
 * fused-release.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include <limits.h>

/* Library headers */
#include "safepthread.h"

/* Private header */
#include "fused.h"

int
fused_release(char const * const path, struct fuse_file_info * const fi)
{
	struct fused * const fused = _fused();
	log_t        * const log   = fused->fsd_log;
	solo_conn_t  * const conn  = fused->fsd_conn;
	int            err;

	log_entering_args(log, "\"%s\"", path);

	struct fused_openfile * const of = (void *) fi->fh;
	log_assert(log, of != NULL);
	int const fd = of->fso_fd;

	/* destroy asynchronous read thread, if present */
	if (of->fso_ralive) {
		Pthread_mutex_lock(&of->fso_rmux);
		of->fso_rintr = true;             // interrupt thread
		if (of->fso_rconn)
			solo_drop(of->fso_rconn);
		Pthread_mutex_unlock(&of->fso_rmux);
		pthread_join(of->fso_rthread, NULL);  // wait for thread
		Pthread_mutex_destroy(&of->fso_rmux);
		Pthread_cond_destroy(&of->fso_rcond);
		unlink(of->fso_rpath);
		close(of->fso_rfd);
	}

	/* destroy open file descriptor */
	fi->fh = (uintptr_t) NULL;
	free(of);

#ifdef __APPLE__
	/* Nothing to do if the open() was delayed and never took place (see
	 * note in fused_open()).
	 */
	if (fd != INT_MIN)
		err = fus_close(conn, fd);
	else {
		log_printf(log, LOG_DEBUG, "(noop)");
		err = 0;
	}
#else
	err = fus_close(conn, fd);
#endif

	log_leaving_err(log, -err);
	return -err;
}
