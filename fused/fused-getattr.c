/*
 * fused-getattr.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fused.h"

int
fused_getattr(char const * const path, struct stat * const stbuf)
{
	struct fused * const fused = _fused();
	log_t        * const log   = fused->fsd_log;
	solo_conn_t  * const conn  = fused->fsd_conn;
	time_t         const now   = time(NULL);
	int            err;

	log_entering_args(log, "\"%s\"", path);

#ifdef __APPLE__
	/* avoid operations on frivolous files */
	if (fused_is_frivolous(path)) {
		log_printf(log, LOG_DEBUG, "(ignore)");
		log_leaving_err(log, -ENOENT);
		return -ENOENT;
	}
#endif
	/* use cache if possible */
	if (fused_cache_get(fused, path, &err, stbuf, now) == 0) {
		log_printf(log, LOG_DEBUG, "(cached)");
		log_leaving_err(log, -err);
		return -err;
	}

	/* stat() */
	err = fus_stat(conn, path, stbuf);

	/* cache response */
	if (!err || err == ENOENT)
		fused_cache_put(fused, path, err, stbuf, now);

	log_leaving_err(log, -err);
	return -err;
}
