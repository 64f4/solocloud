/*
 * fused-truncate.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fused.h"

int
fused_ftruncate(
	char const            * const path,
	off_t                   const offs,
	struct fuse_file_info * const fi
	)
{
	struct fused          * const fused = _fused();
	log_t                 * const log   = fused->fsd_log;
	solo_conn_t           * const conn  = fused->fsd_conn;
	int                     err;

	log_entering_args(log, "\"%s\"", path);

	/* file descriptor */
	struct fused_openfile const * const of = (void *) fi->fh;
	log_assert(log, of != NULL);
	int const fd = of->fso_fd;

	/* invalidate cache entry */
	fused_cache_del(fused, path);

	/* ftruncate() */
	err = fus_ftruncate(conn, fd, offs);

	log_leaving_err(log, -err);
	return -err;
}
