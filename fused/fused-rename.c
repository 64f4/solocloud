/*
 * fused-rename.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fused.h"

int
fused_rename(char const * const src, char const * dst)
{
	struct fused * const fused   = _fused();
	log_t        * const log     = fused->fsd_log;
	solo_conn_t  * const conn    = fused->fsd_conn;
	int            err;

	log_entering_args(log, "\"%s\", \"%s\"", src, dst);

#ifdef __APPLE__
	/* avoid operations on frivolous files */
	if (fused_is_frivolous(src)) {
		log_printf(log, LOG_DEBUG, "(ignore)");
		log_leaving_err(log, -ENOENT);
		return -ENOENT;
	}
	if (fused_is_frivolous(dst)) {
		log_printf(log, LOG_DEBUG, "(ignore)");
		log_leaving_err(log, -EPERM);
		return -EPERM;
	}
#endif
	/* invalidate cache entries */
	fused_cache_del(fused, src);
	fused_cache_del(fused, dst);

	/* rename() */
	err = fus_rename(conn, src, dst);

	log_leaving_err(log, -err);
	return -err;
}
