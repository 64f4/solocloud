/*
 * fused-write.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

#include "fused.h"  // private header

int
fused_write(
	char const            * const path,
	char const            * const buf,
	size_t                  const count,
	off_t                   const offset,
	struct fuse_file_info * const fi
	)
{
	struct fused          * const fused = _fused();
	log_t                 * const log   = fused->fsd_log;
	solo_conn_t           * const conn  = fused->fsd_conn;
	int                     err;

	log_entering_args(log, "\"%s\", %lluB, @%llu", path,
			  (long long) count, (long long) offset);

	/* file descriptor */
	struct fused_openfile const * const of = (void *) fi->fh;
	log_assert(log, of != NULL);
	int const fd = of->fso_fd;

	// FIXME
	//	OSXFUSE stats the file while writing to it, so we cannot cache
	//	the file attributes otherwise we will end up with a corrupted
	//	file (notice that normally we flush the cache after releasing
	//	the file). This problem does not affect Linux (OSXFUSE never
	//	stops amazing me). Anyway, for now we are flushing the cache
	//	before every write, but we should probably do something in
	//	fused_getattr() instead. It took me a while to figure out this
	//	stupid bug.
	//
	fused_cache_del(fused, path);

	/* pwrite() */
	size_t n = count;
	if ((err = fus_pwrite(conn, fd, buf, &n, offset))) {
		log_leaving_err(log, -err);
		return -err;
	}

	log_leaving_ret(log, "%zuB", n);
	return n;
}
