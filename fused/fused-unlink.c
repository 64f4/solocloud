/*
 * fused-unlink.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fused.h"

int
fused_unlink(char const * const path)
{
	struct fused * const fused = _fused();
	log_t        * const log   = fused->fsd_log;
	solo_conn_t  * const conn  = fused->fsd_conn;
	int            err;

	log_entering_args(log, "\"%s\"", path);

	/* invalidate cache entry */
	fused_cache_del(fused, path);

	/* unlink() */
	err = fus_unlink(conn, path);

	log_leaving_err(log, -err);
	return -err;
}
