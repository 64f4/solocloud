/*
 * fused-cache.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Project headers */
#include "spooky.h"     // spooky_hash128()

/* Private header */
#include "fused.h"

/* Expiration time increment for every cache entry access */
#define FUSED_CACHE_QUANTUM 64 // sec

static inline void
_hash128(char const * const path, uint64_t h[2])
{
	h[0] = 0; h[1] = 0;
	spooky_hash128(path, strlen(path), &h[0], &h[1]);
}

void
fused_cache_put(
	struct fused      * const fused,
	char const        * const path,
	int                 const err,
	struct stat const * const st,
	time_t              const now
	)
{
	size_t              const dim   = GCC_DIM(fused->fsd_cache);
	struct fused_attr * const cache = fused->fsd_cache;
	struct fused_attr * slot;
	uint64_t            h[2];

	/* hash */
	_hash128(path, h);
	struct fused_attr * const a = &cache[h[0] % dim];
	struct fused_attr * const b = &cache[h[1] % dim];

	/* If the attribute is already cached, replace it with newer
	 * information.
	 */
	if (a->fat_h[0] == h[0] && a->fat_h[1] == h[1])
		slot = a;
	else if (b->fat_h[0] == h[0] && b->fat_h[1] == h[1])
		slot = b;
	else {
		/* Given two candidate cache slots, `a' and `b', pick the one
		 * that is expired (or unused), or the one with the oldest
		 * access time.
		 */
		if (   a->fat_etime <= now
		    || (   b->fat_etime  > now
		        && a->fat_atime <= b->fat_atime))
			slot = a;
		else
			slot = b;

		/* keep track of collisions */
		if (slot->fat_etime > now)
			fused->fsd_cache_coll++;
	}

	/* write entry to the cache */
	if (err)
		*slot = (struct fused_attr) {
			.fat_h        = { h[0], h[1] },
			.fat_err      = err,
			.fat_quantum  = FUSED_CACHE_QUANTUM,
			.fat_atime    = now,
			.fat_etime    = now + FUSED_CACHE_QUANTUM,
		};
	else {
		*slot = (struct fused_attr) {
			.fat_h        = { h[0], h[1] },
			.fat_err      = err,
			.fat_quantum  = FUSED_CACHE_QUANTUM,
			.fat_st_nlink = st->st_nlink,
			.fat_st_mode  = st->st_mode,
			.fat_st_size  = st->st_size,
			.fat_st_atime = st->st_atime,
			.fat_st_mtime = st->st_mtime,
			.fat_st_ctime = st->st_ctime,
			.fat_atime    = now,
			.fat_etime    = now + FUSED_CACHE_QUANTUM,
		};
	}
}

int
fused_cache_get(
	struct fused      * const fused,
	char const        * const path,
	int               * const err,    // [out]
	struct stat       * const st,     // [out]
	time_t              const now
	)
{
	size_t              const dim   = GCC_DIM(fused->fsd_cache);
	struct fused_attr * const cache = fused->fsd_cache;
	struct fused_attr * slot;
	uint64_t            h[2];

	/* hash */
	_hash128(path, h);
	struct fused_attr * const a = &cache[h[0] % dim];
	struct fused_attr * const b = &cache[h[1] % dim];

	/* in the cache? */
	if (a->fat_h[0] == h[0] && a->fat_h[1] == h[1])
		slot = a;
	else if (b->fat_h[0] == h[0] && b->fat_h[1] == h[1])
		slot = b;
	else
		return ENOENT;

	/* expired? */
	if (slot->fat_etime <= now)
		return ENOENT;

	/* update slot access times */
	uint16_t const q = slot->fat_quantum >> 1;
	slot->fat_quantum = q;
	slot->fat_atime   = now;
	slot->fat_etime  += q;

	/* return cached information back to the caller */
	if ((*err = slot->fat_err) == 0) {
		*st = (struct stat) {
			.st_mode  = slot->fat_st_mode,
			.st_nlink = slot->fat_st_nlink,
			.st_size  = slot->fat_st_size,
		};
		st->st_atime = slot->fat_st_atime;
		st->st_mtime = slot->fat_st_mtime;
		st->st_ctime = slot->fat_st_ctime;
	}

	return 0;
}

void
fused_cache_del(
	struct fused      * const fused,
	char const        * const path
	)
{
	size_t              const dim   = GCC_DIM(fused->fsd_cache);
	struct fused_attr * const cache = fused->fsd_cache;
	uint64_t            h[2];

	/* hash */
	_hash128(path, h);
	struct fused_attr * const a = &cache[h[0] % dim];
	struct fused_attr * const b = &cache[h[1] % dim];

	/* in the cache? */
	struct fused_attr * slot;
	if (a->fat_h[0] == h[0] && a->fat_h[1] == h[1])
		slot = a;
	else if (b->fat_h[0] == h[0] && b->fat_h[1] == h[1])
		slot = b;
	else
		return;

	/* invalidate entry */
	memset(slot, 0, sizeof(*slot));
}
