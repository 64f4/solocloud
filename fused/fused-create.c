/*
 * fused-create.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fused.h"

int
fused_create(
	char const            * const path,
	mode_t                  const GCC_ATTRIBUTE_UNUSED(mode),
	struct fuse_file_info * const fi
	)
{
	struct fused          * const fused = _fused();
	log_t                 * const log   = fused->fsd_log;
	int                     ret;

	log_entering_args(log, "\"%s\"", path);

	fi->flags |= O_CREAT;
	ret = fused_open(path, fi);

	log_leaving_err(log, ret);
	return ret;
}
