/*
 * fused-mkdir.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fused.h"

int
fused_mkdir(char const * const path, mode_t const mode)
{
	struct fused * const fused = _fused();
	log_t        * const log   = fused->fsd_log;
	solo_conn_t  * const conn  = fused->fsd_conn;
	int            err;

	log_entering_args(log, "\"%s\"", path);

#ifdef __APPLE__
	/* avoid operations on frivolous files */
	if (fused_is_frivolous(path)) {
		log_printf(log, LOG_DEBUG, "(ignore)");
		log_leaving_err(log, -EPERM);
		return -EPERM;
	}
#endif
	/* invalidate cache entry */
	fused_cache_del(fused, path);

	/* mkdir() */
	err = fus_mkdir(conn, path, mode);

	log_leaving_err(log, -err);
	return -err;
}
