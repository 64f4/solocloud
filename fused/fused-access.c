/*
 * fused-access.c
 *
 * (C) 2012 Emanuele Altieri <ea@paradoxity.com>
 */

/* Private header */
#include "fused.h"

int
fused_access(char const * const path, int GCC_ATTRIBUTE_UNUSED(mode))
{
	struct fused * const fused = _fused();
	log_t        * const log   = fused->fsd_log;
	struct stat    stbuf;

	log_entering_args(log, "\"%s\"", path);

	/* if the file exists then we have access  */
	int const ret = fused_getattr(path, &stbuf);

	log_leaving_err(log, ret);
	return ret;
}
