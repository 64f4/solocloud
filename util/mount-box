#!/bin/bash

# Process name
readonly ME=$(basename $0)

# Cloud Box identifier
readonly BOXID="$1"

# Directory where we will be mounting the file system (temporary)
readonly MOUNTDIR="/Volumes/$BOXID"

# Switchboard server
readonly SWITCHBOARD="ec2-107-20-78-222.compute-1.amazonaws.com"

# Log file
readonly LOGFILE="/tmp/$BOXID.log"

function fail()
{
	echo "$ME: $*" >&2
	exit 1
}

function info()
{
	echo "$ME: $*" >&2
}

function init()
{
	# test number of arguments
	if [ $# -ne 1 ]
	then
		fail "syntax error: expected box identifier"
	fi

	# chdir to parent directory of current process
	cd $(dirname "$0")/.. || fail "chdir() error"

	# create temporary directory where to mount the file system
	if [ ! -d $MOUNTDIR ]
	then
		mkdir -p $MOUNTDIR 2>/dev/null || \
			fail "cannot create $MOUNTDIR"
	fi
}

function main()
{
	# mount FUSE filesystem
	fused/solo-fs $MOUNTDIR $BOXID $SWITCHBOARD &>$LOGFILE
	case $? in
	0)
		;;
	# EX_NOHOST
	68)
		fail "cannot locate box \"$BOXID\"" ;;
	# EX_NOPERM
	77)
		fail "invalid password" ;;
	*)
		fail "service is currently unavailable" ;;
	esac

	# wait for file system to be mounted
	info "mounting file system"
	while [ 1 ];
	do
		sleep 0.1
		mount | /usr/bin/grep $MOUNTDIR &>/dev/null && break
	done

	# open mount directory with the Finder
	info "ready"
	open $MOUNTDIR
}

init "$@"
main
